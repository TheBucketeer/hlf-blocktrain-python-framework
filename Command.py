import sys

from config_generation.util import wide_print, print_useful_msg


class Command(object):
    """Helper class for storing commands that are part of this script.
    """
    # To be filled in before calling any of the following functions
    all_commands = []

    def __init__(self, cmd_str, function, help_str, in_pipeline=True, consumes_args=False, param_help_info=None):
        self.cmd_str = cmd_str
        self.function = function
        self.in_pipeline = in_pipeline
        self.help_str = help_str
        assert not (consumes_args and not param_help_info), 'Function consumes arguments, but no parameter info was ' \
                                                            'provided'
        assert not (param_help_info and not consumes_args), 'Function consumes no arguments, yet parameter info was' \
                                                            'passed'
        self.consumes_args = consumes_args
        self.param_help_info = param_help_info

    @staticmethod
    def execute_pipeline():
        for cmd in Command.all_commands:
            if cmd.in_pipeline:
                cmd.function()

    @staticmethod
    def get_command(cmd_str):
        for cmd in Command.all_commands:
            if cmd.cmd_str == cmd_str:
                return cmd
        print('Invalid command: ' + cmd_str)

    @staticmethod
    def execute_commands(cmd_strings):
        if len(cmd_strings) == 0:
            print_useful_msg()
        for i, cmd_str in enumerate(cmd_strings):
            cmd = Command.get_command(cmd_str)
            if cmd is None:
                Command.print_help()
            else:
                if cmd.consumes_args:
                    cmd.function(*cmd_strings[i + 1:])
                    break
                else:
                    cmd.function()

    @staticmethod
    def print_help():
        wide_print('All available commands:')
        for c in Command.all_commands:
            print('{:s}'.format(c.cmd_str))
            print('\t{:s}'.format(c.help_str))
            if c.consumes_args:
                print('\t(Consumes all remaining arguments)')
                output = '\tUsage: python3 {:s} {:s}'.format(sys.argv[0], c.cmd_str)
                for name, is_vararg, _ in c.param_help_info:
                    output += ' [{:s}]'.format(name)
                    if is_vararg:
                        output += '...'
                output += '\n'
                for name, _, help_str in c.param_help_info:
                    output += '\t\t- {:s}: {:s}\n'.format(name, help_str)
                print(output)
