import sys

from Command import Command
from config_generation.config import clients, CLIENT_COMPILE_FILE, CLIENT_CONNECTION_FILE, CLIENT_EVENTLOG_FILE, \
    CLIENT_LOG_FILE, CLIENT_RUN_FILE, \
    CLIENT_SETTINGS_FILE, LOCAL_CLIENTROOT_REL, LOCAL_PI_WDIR, LOCAL_CRYPTODIR_REL, CLIENT_LOG4J_PROPS_FILE, \
    LOCALHOST_IP
from config_generation.network_layout import make_network_layout, setup_hosts
from config_generation.util import debug_system, make_chmod_and_exec_cmd, send_parallel_ssh_cmds, broadcast_cmd_to_all, \
    send_files_to_hosts, send_dir_to_clients


def cleanup():
    """Cleans up all network data, both locally and remotely."""
    files_to_remove = [CLIENT_COMPILE_FILE, CLIENT_CONNECTION_FILE, CLIENT_EVENTLOG_FILE, CLIENT_LOG_FILE,
                       CLIENT_RUN_FILE, CLIENT_SETTINGS_FILE, CLIENT_LOG4J_PROPS_FILE]
    files_to_remove_param = ' '.join(['*' + filename for filename in files_to_remove])
    # Remove local data
    rm_cmd = 'rm {:s}'.format(files_to_remove_param)
    debug_system(rm_cmd, ignore_error=True, silence=True)
    # Remove data from Pis
    broadcast_cmd_to_all('cd {:s} && {:s}'.format(LOCAL_PI_WDIR, rm_cmd), ignore_error=True, silence=True)


def transfer_settings():
    """Send all client-specific files except the source code."""
    send_dir_to_clients(LOCAL_CRYPTODIR_REL, LOCAL_PI_WDIR, ignore_error=True)
    send_parallel_files_param = []
    # Send client-specific files
    for client in clients:
        if client.host.ip != LOCALHOST_IP:
            for filename in [client.run_filename, client.connection_json_filename, client.clientsettings_filename,
                             client.client_compile_filename, client.log4j_props_filename]:
                send_parallel_files_param.append((client.host, filename, '{:s}/{:s}'.format(LOCAL_PI_WDIR, filename)))
    send_files_to_hosts(send_parallel_files_param)


def transfer_code():
    """Send the client code to all hosts that need it."""
    send_dir_to_clients(LOCAL_CLIENTROOT_REL, LOCAL_PI_WDIR)


def compile_remote_clients():
    """Compiles the client code on all remote hosts."""
    send_parallel_ssh_cmds([(client.host, make_chmod_and_exec_cmd(LOCAL_PI_WDIR, client.client_compile_filename))
                            for client in clients if client.host.ip != LOCALHOST_IP])


def remote_clients_up(asset_ids=None):
    """Starts all (or a subset of) remote clients.

    Args:
        asset_ids (:obj:`list` of :obj:`str`): List of client asset IDs to start. If not provided, all clients will be
            started
    """
    if asset_ids is None:
        asset_ids = []

    send_parallel_ssh_cmds([(client.host, make_chmod_and_exec_cmd(LOCAL_PI_WDIR, client.run_filename))
                            for client in clients if
                            client.host.ip != LOCALHOST_IP and (len(asset_ids) == 0 or client.asset_id in asset_ids)])


if __name__ == '__main__':
    setup_hosts()
    make_network_layout()

    Command.all_commands = ([
        Command('all', Command.execute_pipeline, 'Execute the entire pipeline for starting the network',
                in_pipeline=False),
        Command('transfer_settings', transfer_settings, 'Transmit all client configuration files to Pis that need '
                                                        'them'),
        Command('transfer_code', transfer_code, 'Transmit the client code to Pis that need it'),
        Command('compile_remote', compile_remote_clients, 'Compile the client code on all Pis that host clients'),
        Command('remote_clients_up', remote_clients_up, 'Start all remote clients', consumes_args=True,
                param_help_info=[('CLIENT_IDS', True, 'The IDs of the clients that are to be started - if none are'
                                                      'provided, all remote clients will be started')]),
        Command('help', Command.print_help, 'Print help message', in_pipeline=False)
    ])
    Command.execute_commands(sys.argv[1:])
