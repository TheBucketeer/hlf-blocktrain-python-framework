import os
import sys

from Command import Command
from config_generation.config import cas, channel_host_orderers, get_cli, get_main_peer, \
    get_remote_hosts, peer_nodes, peer_orgs, \
    CHANNEL_NAME, DOCKER_COMPOSE_FILE, LOCAL_CA_SCRIPTS_REL, LOCAL_CCROOT_REL, LOCAL_CONFIGTXDIR_REL, \
    LOCAL_CRYPTODIR_REL, LOCAL_FS_DIR, \
    LOCAL_KEYSTORE_REL, ORDERER_SYSCHANNEL_NAME, PEER_PROFILE, LOCAL_PI_WDIR, SSH_NAMES_TO_IPS, SLEEP_DELAYS, \
    get_network_hosts, get_remote_network_hosts, DOTENV_FILE, CRYPTO_CONFIG_FILE, \
    LOCAL_DESKTOP_WDIR, clients, LOCALHOST_IP, get_localhost, IPS_TO_HOSTS, CLIENT_COMPILE_FILE, CLIENT_LOG4J_FILE, \
    CONFIGTX_FILE, CLIENT_LOG4J_PROPS_FILE, CLIENT_CONNECTION_FILE, CLIENT_RUN_FILE, CLIENT_EVENTLOG_FILE, \
    CLIENT_LOG_FILE, UPDATE_AFF_FILE, CLIENT_SETTINGS_FILE
from config_generation.generate_configs import generate_configs
from config_generation.network_layout import make_network_layout, setup_hosts
from config_generation.nodes.CA import CA
from config_generation.nodes.PeerNode import PeerNode
from config_generation.util import sort_services, sleep, debug_system, send_parallel_ssh_cmds, \
    broadcast_cmd_to_all, send_files_to_hosts, send_dir_to_network, wide_print


def cleanup():
    """Cleans up all network-related files, both locally and remotely."""
    dirs_to_remove = [LOCAL_KEYSTORE_REL, LOCAL_CONFIGTXDIR_REL, LOCAL_CRYPTODIR_REL, LOCAL_CA_SCRIPTS_REL]
    files_to_remove = [CLIENT_COMPILE_FILE, CLIENT_CONNECTION_FILE, CLIENT_EVENTLOG_FILE, CLIENT_LOG_FILE,
                       CLIENT_LOG4J_FILE, CLIENT_LOG4J_PROPS_FILE, CLIENT_RUN_FILE, CLIENT_SETTINGS_FILE,
                       CRYPTO_CONFIG_FILE, CONFIGTX_FILE, DOCKER_COMPOSE_FILE, DOTENV_FILE, UPDATE_AFF_FILE]
    dirs_to_remove_param = ' '.join(dirs_to_remove)
    # Filenames are partial, and are prefixed by client IDs, hence the asterisk
    files_to_remove_param = ' '.join(['*' + filename for filename in files_to_remove])
    wide_print("Cleaning up our mess")
    # Remove local data
    rm_cmd = 'rm {:s} -r {:s}'.format(files_to_remove_param, dirs_to_remove_param)
    debug_system('cd {:s} && {:s}'.format(LOCAL_DESKTOP_WDIR, rm_cmd), ignore_error=True)
    # Remove data from remote hosts
    broadcast_cmd_to_all('cd {:s} && {:s}'.format(LOCAL_PI_WDIR, rm_cmd), ignore_error=True)


def create_dirs():
    """Creates the required file structure for the remainder of the process."""
    dirs_to_remove = [LOCAL_KEYSTORE_REL, LOCAL_CONFIGTXDIR_REL, LOCAL_CRYPTODIR_REL, LOCAL_CA_SCRIPTS_REL]
    dirs_to_remove_param = ' '.join(dirs_to_remove)
    mkdir_cmd = 'mkdir {:s}'.format(dirs_to_remove_param)
    wide_print("(Re-)creating the required directories")
    # Make local dirs
    debug_system('cd {:s} && {:s}'.format(LOCAL_DESKTOP_WDIR, mkdir_cmd), ignore_error=True)
    # Make remote dirs
    broadcast_cmd_to_all('cd {:s} && {:s}'.format(LOCAL_PI_WDIR, mkdir_cmd), ignore_error=True)


def generate_crypto_materials(channel=CHANNEL_NAME, orderer_syschannel=ORDERER_SYSCHANNEL_NAME):
    """Generates all cryptographic materials for the network."""
    wide_print("Generating cryptographic materials")
    debug_system('cryptogen generate --config={:s}/{:s} --output={:s}/{:s}'.format(
        LOCAL_DESKTOP_WDIR, CRYPTO_CONFIG_FILE, LOCAL_DESKTOP_WDIR, LOCAL_CRYPTODIR_REL))
    assert channel in channel_host_orderers, '' + str(channel_host_orderers)
    orderer = channel_host_orderers[channel]

    debug_system(
        'configtxgen -configPath {:s} -profile {:s} -outputBlock {:s}/{:s}/genesis.block -channelID {:s}'.format(
            LOCAL_DESKTOP_WDIR, orderer.orderer_org.profile_name, LOCAL_DESKTOP_WDIR, LOCAL_CONFIGTXDIR_REL,
            orderer_syschannel))
    debug_system(('configtxgen -configPath {:s} -profile {:s} -outputCreateChannelTx {:s}/{:s}/channel.tx '
                  '-channelID {:s}').format(
        LOCAL_DESKTOP_WDIR, PEER_PROFILE, LOCAL_DESKTOP_WDIR, LOCAL_CONFIGTXDIR_REL, channel))

    cgen_fmt_cmd = 'configtxgen -configPath {:s} -profile {:s} -outputAnchorPeersUpdate {:s}/{:s}/{:s}anchors.tx ' \
                   '-channelID {:s} -asOrg {:s}'
    for peer_org in peer_orgs:
        debug_system(cgen_fmt_cmd.format(LOCAL_DESKTOP_WDIR, PEER_PROFILE, LOCAL_DESKTOP_WDIR, LOCAL_CONFIGTXDIR_REL,
                                         peer_org.msp_id, channel, peer_org.name))


def find_sks():
    """Finds and stores the names of all secret keys."""
    wide_print("Searching for private key files")
    # Find admin SK files for organisations
    for peer_org in peer_orgs:
        peer_org.set_admin_sk()
    # Find SK files for clients
    for client in clients:
        client.set_sk()
    # Find SK files for CAs
    for ca in cas:
        ca.set_sk()


def insert_sks():
    """Places the names of secret keys in the relevant configuration files."""

    # Helper function for editing files
    def replace_in_local_file(filename, fromstr, tostr):
        filename = '{:s}/{:s}'.format(LOCAL_DESKTOP_WDIR, filename)
        with open(filename) as f:
            text = f.read()
            text = text.replace(fromstr, tostr)
        with open(filename, 'w') as f:
            f.write(text)

    wide_print("Inserting the names of private key files into configuration files")
    for ca in cas:
        replace_in_local_file(ca.host.docker_compose_filename, ca.sk_filename_placeholder, ca.sk_filename)
    for client in clients:
        # Insert private key filenames into each client's connection.json
        replace_in_local_file(client.clientsettings_filename, client.sk_filename_placeholder, client.sk_filename)
        for peer_org in peer_orgs:
            replace_in_local_file(
                client.connection_json_filename, peer_org.admin_sk_filename_placeholder, peer_org.admin_sk_filename)


def update_affiliations(hosts=None):
    """Tells Certificate Authorities to update their affiliations.
    Args:
        hosts (:obj:`list` of `Host`, optional): The hosts on which CAs should be updated. If not provided,
            CAs on all hosts in the network will be included.
    """
    if hosts is None:
        hosts = get_network_hosts()
    wide_print('Updating CA affiliations')
    for host in hosts:
        for ca in host.docker_services[CA.__name__]:
            ca.update_affiliations()
    sleep(SLEEP_DELAYS['after_affiliation_update'])


def transfer_networkfiles():
    """Transmits all required files to all remote hosts that need them, with the exception of the Chaincode."""
    wide_print('Transmitting network-related files to Pis')
    for local_dir in [LOCAL_CRYPTODIR_REL, LOCAL_CONFIGTXDIR_REL, LOCAL_CA_SCRIPTS_REL]:
        send_dir_to_network(local_dir, LOCAL_PI_WDIR, ignore_error=True, from_local_wdir=True)

    send_files_to_hosts_params = []
    for host in get_remote_hosts():
        send_files_to_hosts_params.append((host, DOTENV_FILE, LOCAL_PI_WDIR))
        # Find files destined for remote hosts and transfer them
        if os.path.exists('{:s}/{:s}'.format(LOCAL_DESKTOP_WDIR, host.docker_compose_filename)):
            send_files_to_hosts_params.append(
                (host, host.docker_compose_filename, '{:s}/{:s}'.format(LOCAL_PI_WDIR, DOCKER_COMPOSE_FILE)))
    send_files_to_hosts(send_files_to_hosts_params, ignore_error=True, from_local_wdir=True)


def transfer_ccfiles():
    """Transmits the Chaincode to all nodes in the network."""
    wide_print('Transmitting Chaincode to Pis')
    send_dir_to_network(LOCAL_CCROOT_REL, LOCAL_PI_WDIR, ignore_error=True, from_local_wdir=True)


def make_mince_of_network():
    """Stops all Docker services.

    Executes 4 commands which consecutively:
     * Stop all containers defined in the docker-compose.yaml file (on that host)
     * Stop all remaining containers
     * Remove all containers
     * Remove all Chaincode images
    """
    cmds = ['if test -f {0}; then docker-compose -f {0} down --remove-orphans; fi'.format(DOCKER_COMPOSE_FILE),
            'containersToKill=$(docker ps -a | grep -v CONTAINER | cut -d \' \' -f 1); if [ $( expr length '
            '"$containersToKill") -ne 0 ]; then docker kill $containersToKill; fi',
            'containersToRemove=$(docker ps -a | grep -v CONTAINER | cut -d \' \' -f 1); if [ $(expr length '
            '"$containersToRemove") -ne 0 ]; then docker rm $containersToRemove; fi',
            'imagesToRemove=$(docker images | grep dev | tr -s \' \' | cut -d \' \' -f 3); if [ $(expr length '
            '"$imagesToRemove") -ne 0 ]; then docker rmi --force $imagesToRemove; fi']
    wide_print("Taking down the network")
    for cmd in cmds:
        # Silence the output, we don't care if Docker starts crying
        debug_system('cd {:s} && {:s}'.format(LOCAL_DESKTOP_WDIR, cmd), ignore_error=True, silence=True)
        broadcast_cmd_to_all('cd {:s} && {:s}'.format(LOCAL_PI_WDIR, cmd), ignore_error=True, silence=True)


def destruct_cc():
    """Takes down and remove all Chaincode containers."""
    wide_print("Destroying Chaincode containers")
    kill_containers_cmd = 'containersToKill=$(docker ps -a | grep dev | cut -d \' \' -f 1); if [ $( expr length ' \
                          '"$containersToKill") -ne 0 ]; then docker kill $containersToKill; fi'
    rm_cc_cmd = 'imagesToRemove=$(docker images | grep dev | tr -s \' \' | cut -d \' \' -f 3); if [ $(expr length ' \
                '"$imagesToRemove") -ne 0 ]; then docker rmi --force $imagesToRemove; fi'
    debug_system(kill_containers_cmd, ignore_error=True, silence=True)
    debug_system(rm_cc_cmd, ignore_error=True, silence=True)
    broadcast_cmd_to_all(kill_containers_cmd, ignore_error=True)
    broadcast_cmd_to_all(rm_cc_cmd, ignore_error=True)


def start_services(hosts=None, service_names=None):
    """Starts all Docker containers of a given list of types.

    Args:
        hosts (:obj:`list` of :obj:`Host`, optional): List of all hosts on which services should be started
        service_names (:obj:`list` of :obj:`str`, optional): List of Docker service types. If not provided, all types
            will be used.
    """
    if hosts is None:
        hosts = get_network_hosts()
    # Create commands
    remote_host_cmd_pairs = []
    local_cmd = None
    for host in hosts:
        cmd = host.start_services_cmd(service_names=service_names)
        cmd = 'MSYS_NO_PATHCONV=1 FABRIC_START_TIMEOUT=10 ' + cmd
        if cmd is not None:
            if host.ip == LOCALHOST_IP:
                local_cmd = cmd
            else:
                remote_host_cmd_pairs.append((host, cmd))
    # Execute commands
    if local_cmd is not None:
        debug_system(local_cmd)
    send_parallel_ssh_cmds(remote_host_cmd_pairs)


def start_nodes(hosts=None):
    """Starts all Docker containers required for the network.

    Args:
        hosts (:obj:`list` of :obj:`Host`, optional): The hosts on which services should be started. If not provided,
           all hosts in the network will be included.
    """
    if hosts is None:
        hosts = get_network_hosts()
    wide_print('Starting nodes')
    start_services(hosts=hosts)
    # Wait for Kafka cluster to boot
    sleep(SLEEP_DELAYS['after_network_up'])


def create_channel():
    """Creates the channel."""
    wide_print('Creating channel')
    main_peer = get_main_peer()
    main_peer.host.execute_cmd(main_peer.bash_cmd('channel_create'))
    sleep(SLEEP_DELAYS['after_channel_create'])


def fetch_config_blocks(hosts=None):
    """Has peers of a given list of hosts fetch the channel's config block.

        Args:
            hosts (:obj:`list` of `Host`, optional): The hosts on which peers should be joined. If not provided,
                peers from all hosts in the network will be included.
    """
    wide_print('Having peers fetch config block')
    if hosts is None:
        hosts = get_network_hosts()
    remote_hosts = [host for host in hosts if host.ip != LOCALHOST_IP]
    localhost = get_localhost()
    main_peer = get_main_peer()
    if localhost in hosts:
        # Local peers
        for peer_node in localhost.docker_services[PeerNode.__name__]:
            # No need to have the local main peer fetch this block; he created the channel and already has it
            if peer_node != main_peer:
                debug_system(peer_node.bash_cmd('fetch'))

    # Create SSH commands for remote peers
    remote_host_cmd_pairs = []
    for host in remote_hosts:
        for peer_node in host.docker_services[PeerNode.__name__]:
            if peer_node != main_peer:
                cmd = 'MSYS_NO_PATHCONV=1 FABRIC_START_TIMEOUT=10 ' + peer_node.bash_cmd('fetch')
                remote_host_cmd_pairs.append((host, cmd))
    send_parallel_ssh_cmds(remote_host_cmd_pairs)
    sleep(SLEEP_DELAYS['after_fetch_config'])


def join_peers_to_channel(hosts=None):
    """Joins peers of a given list of hosts to the channel.

    Args:
        hosts (:obj:`list` of `Host`, optional): The hosts on which peers should be joined. If not provided,
            peers from all hosts in the network will be included.
    """
    wide_print('Joining peers')
    if hosts is None:
        hosts = get_network_hosts()
    remote_hosts = [host for host in hosts if host.ip != LOCALHOST_IP]
    localhost = get_localhost()
    if localhost in hosts:
        # Local peers
        for peer_node in localhost.docker_services[PeerNode.__name__]:
            debug_system(peer_node.bash_cmd('join'))

    # Create SSH commands for remote peers
    remote_host_cmd_pairs = []
    for host in remote_hosts:
        for peer_node in host.docker_services[PeerNode.__name__]:
            cmd = 'MSYS_NO_PATHCONV=1 FABRIC_START_TIMEOUT=10 ' + peer_node.bash_cmd('join')
            remote_host_cmd_pairs.append((host, cmd))
    send_parallel_ssh_cmds(remote_host_cmd_pairs)
    sleep(SLEEP_DELAYS['after_join'])


def update_anchor_peers(hosts=None):
    """Updates the anchor peers in the network.

    Args:
        hosts (:obj:`list` of :obj:`Host`, optional): The hosts on which anchor peers should be updated. If not
            provided, anchor peers from all hosts in the network will be included.
    """
    if hosts is None:
        hosts = get_network_hosts()
    wide_print('Updating anchor peers')
    for peer_org in peer_orgs:
        anchor_peer = peer_org.get_anchor_peer()
        if anchor_peer.host not in hosts:
            continue
        anchor_peer.host.execute_cmd(anchor_peer.bash_cmd('update_anchors'))


def install_cc_through_cli(hosts=None):
    """Installs the Chaincode on peers via the CLI.

    Args:
        hosts (:obj:`list` of `Host`, optional): The hosts on which peers should install the Chaincode. If not
            provided, peers from all hosts in the network will be included.
    """
    wide_print('Installing Chaincode onto peers via CLI')
    if hosts is None:
        hosts = get_network_hosts()
    cli = get_cli()
    for host in hosts:
        for peer_node in host.docker_services[PeerNode.__name__]:
            cli.set_target_peer(peer_node)
            cli.host.execute_cmd(cli.bash_cmd('install_cc'))
    sleep(SLEEP_DELAYS['after_install_cc'])


def install_cc(hosts=None):
    """Installs the Chaincode on peers.

    Args:
        hosts (:obj:`list` of `Host`, optional): The hosts on which peers should install the Chaincode. If not
            provided, peers from all hosts in the network will be included.
    """
    wide_print('Installing Chaincode onto peers')
    if hosts is None:
        hosts = get_network_hosts()
    for host in hosts:
        for peer_node in host.docker_services[PeerNode.__name__]:
            # The addition before the command is used to resolve segfaults due to a bug in Hyperledger Fabric
            peer_node.host.execute_cmd('GODEBUG=netdns=go ' + peer_node.bash_cmd('install_cc'))
    sleep(SLEEP_DELAYS['after_install_cc'])


def instantiate_cc():
    """Instantiates the Chaincode."""
    wide_print('Instantiating Chaincode')
    main_peer = get_main_peer()
    main_peer.host.execute_cmd(main_peer.bash_cmd('instantiate_cc'))
    sleep(SLEEP_DELAYS['after_instantiate_cc'])


def prefix_to_peers(peer_name_prefix):
    """Helper function for turning prefixes of peer hostnames into a list of peers that match that prefix.

    Args:
        peer_name_prefix (str): The prefix to get the PeerNodes for.

    Returns:
        :obj:`list` of :obj:`PeerNode`: The list of peer nodes that match the prefix.
    """
    result = []
    for peer_node in peer_nodes:
        if peer_node.hostname.startswith(peer_name_prefix):
            result.append(peer_node)
    return list(set(result))


def get_source_peer(source_peer_name_prefix):
    """Wrapper function for prefix_to_peers, for finding the source PeerNode object for a CC interaction.

    Args:
        source_peer_name_prefix (str): The prefix of the hostname belonging to the intended source peer.

    Raises:
        AssertionError: If no peer matches the given prefix.

    Returns:
        PeerNode: The source peer object.
    """
    source_peer_candidates = prefix_to_peers(source_peer_name_prefix)
    assert len(source_peer_candidates) > 0, 'No valid source peer found'
    source_peer = source_peer_candidates[0]
    if len(source_peer_candidates) > 1:
        wide_print('Warning: source peer argument {:s} is ambiguous ({:s})'.format(
            source_peer_name_prefix, ', '.join([s.hostname for s in source_peer_candidates])))
        wide_print('Using {:s}...'.format(source_peer.hostname))
    return source_peer


def get_validating_peers(validating_peer_name_prefixes):
    """Wrapper function for prefix_to_peers, for finding the validating PeerNode objects for a CC interaction.

    Args:
        validating_peer_name_prefixes (:obj:`list` of :obj:`str`): List of the prefixes belonging to the intended
            validating peers.

    Raises:
        AssertionError: If no peer matches the given prefix.

    Returns:
        :obj:`list` of :obj:`PeerNode`: List of all matching PeerNode objects.
    """
    validating_peers = []
    for peer_prefix in validating_peer_name_prefixes:
        validating_peers += prefix_to_peers(peer_prefix)
    assert len(validating_peers) > 0, 'No valid validating peers found'
    return validating_peers


def make_invocation_cmd(source_peer_name_prefix, validating_peer_name_prefixes, *args, as_admin=False):
    """Helper function for creating invocation commands.

    Args:
        source_peer_name_prefix (str): prefix (up to full name) of the source peer for this invocation.
        validating_peer_name_prefixes (str): comma-separated string of prefixes of the validating peers for this
            invocation.
        *args: The arguments for this invocation, beginning with the function to invoke,
        as_admin (:obj:`bool`, optional): Whether to execute this invocation as administrator or not, defaults to False.

    Returns:
        str: the command to be executed for this invocation.
    """
    source_peer = get_source_peer(source_peer_name_prefix)
    validating_peers = get_validating_peers(validating_peer_name_prefixes.split(','))
    return source_peer.bash_cmd('invoke_cc', peers=validating_peers, args=args, as_admin=as_admin)


def make_query_cmd(source_peer_name_prefix, validating_peer_name_prefixes, *args, as_admin=False):
    """Helper function for creating query commands.

    Args:
        source_peer_name_prefix (str): prefix (up to full name) of the source peer for this query.
        validating_peer_name_prefixes (str): comma-separated string of prefixes of the validating peers for this
            query.
        *args: The arguments for this query, beginning with the function to call.
        as_admin (:obj:`bool`, optional): Whether to execute this query as administrator or not, defaults to False.

    Returns:
        str: the command to be executed for this query.
    """
    source_peer = get_source_peer(source_peer_name_prefix)
    validating_peers = get_validating_peers(validating_peer_name_prefixes.split(','))
    return source_peer.bash_cmd('query_cc', peers=validating_peers, args=args, as_admin=as_admin)


def invoke_cc(source_peer_name_prefix, validating_peer_names, *args, as_admin=False):
    """Helper function for performing a Chaincode invocation either locally or remotely.

    Args:
        source_peer_name_prefix (str): prefix (up to full name) of the source peer for this invocation.
        validating_peer_names (str): comma-separated string of prefixes of the validating peers for this invocation.
        *args: The arguments for this invocation, beginning with the function to invoke.
        as_admin (:obj:`bool`, optional): Whether or not this invocation should performed with Admin rights (of the
            source peer's organisation).
    """
    source_peer = get_source_peer(source_peer_name_prefix)
    cmd = make_invocation_cmd(source_peer_name_prefix, validating_peer_names, *args, as_admin=as_admin)
    source_peer.host.execute_cmd(cmd)


def query_cc(source_peer_name_prefix, validating_peer_names, *args, as_admin=False):
    """Helper function for performing a Chaincode query either locally or remotely.

    Args:
        source_peer_name_prefix (str): prefix (up to full name) of the source peer for this query.
        validating_peer_names (str): comma-separated string of prefixes of the validating peers for this query.
        *args: The arguments for this query, beginning with the (query) function to invoke.
        as_admin (:obj:`bool`, optional): Whether or not this query should performed with Admin rights (of the
            source peer's organisation).
    """
    source_peer = get_source_peer(source_peer_name_prefix)
    cmd = make_query_cmd(source_peer_name_prefix, validating_peer_names, *args, as_admin=as_admin)
    source_peer.host.execute_cmd(cmd)


def rejoin_hosts(*args):
    """Adds one or more hosts (back) to the network.

    Pis have a tendency to fail occasionally, hence this function. The procedure is comparable to starting a network
    from scratch, except only certain hosts will be considered.

    Args:
        *args (`str`): The SSH names of all hosts to be joined
    """
    ips = [SSH_NAMES_TO_IPS[piname] for piname in args]
    hosts = list(set([IPS_TO_HOSTS[ip] for ip in ips]))
    start_nodes(hosts=hosts)
    fetch_config_blocks(hosts=hosts)
    join_peers_to_channel(hosts=hosts)
    update_anchor_peers(hosts=hosts)
    update_affiliations(hosts=hosts)
    install_cc(hosts=hosts)


def start_local_chaincodes():
    """Causes local Chaincode containers to be started by sending an invocation to them."""
    peers_to_start = get_localhost().docker_services[PeerNode.__name__]
    wide_print('Starting local Chaincode containers')
    for peer_node in peers_to_start:
        debug_system(make_invocation_cmd(peer_node.hostname, peer_node.hostname, 'queryTestVar', 'redDevil'))


def start_remote_chaincodes(hosts=None):
    """Causes remote Chaincode containers to be started by sending an invocation to them, in parallel.

    The parallelism is possible because all containers only request endorsement for their invocation from themselves.
    Whether or not the invocation succeeds is irrelevant.

    Args:
        hosts (:obj:`list` of :obj:`Host`, optional): A list of all hosts on which containers should be started. If not
            provided, all containers will be started.
    """
    if hosts is None:
        hosts = get_remote_network_hosts()
    peers_to_start = sort_services(sum([host.docker_services[PeerNode.__name__] for host in hosts], []))
    wide_print('Starting remote Chaincode containers')
    host_cmd_pairs = []
    for peer_node in peers_to_start:
        host_cmd_pairs.append((peer_node.host, make_invocation_cmd(peer_node.hostname, peer_node.hostname,
                                                                   'queryTestVar', 'redDevil')))
    send_parallel_ssh_cmds(host_cmd_pairs)
    sleep(20)


def test_cc(hosts=None):
    if hosts is None:
        hosts = get_network_hosts()
    peers_to_test_on = sum([host.docker_services[PeerNode.__name__] for host in hosts], [])
    main_peer = get_main_peer()
    for peer in peers_to_test_on:
        invoke_cc(peer.hostname, peer.hostname, 'setTestVar', 'redDevil', 1337)
        sleep(20)
        invoke_cc(main_peer.hostname, peer.hostname, 'queryTestVar', 'redDevil')


if __name__ == '__main__':
    os.environ['PATH'] += ':{:s}/bin'.format(LOCAL_FS_DIR)
    os.environ['FABRIC_CFG_PATH'] = LOCAL_DESKTOP_WDIR
    os.environ['MSYS_NO_PATHCONV'] = '1'
    os.environ['CHANNEL_NAME'] = CHANNEL_NAME  # Remember this line of code, for it will haunt me for eternity
    os.environ['FABRIC_START_TIMEOUT'] = '50'
    os.environ['OS_ARCH'] = 'amd64'

    setup_hosts()
    make_network_layout()
    # create_channel fetch join update_anchor_peers update_affiliations install_cc instantiate_cc
    Command.all_commands = ([
        Command('all', Command.execute_pipeline, 'Execute the entire pipeline for starting the network',
                in_pipeline=False),
        Command('remove_cc', destruct_cc, 'Remove all Chaincode containers'),
        Command('down', make_mince_of_network, 'Take down the network'),
        Command('cleanup', cleanup, 'Remove all artifacts generated by this script'),
        Command('create_dirs', create_dirs, '(Re-)create all directories required for the network'),
        Command('generate_configs', generate_configs,
                'Generate the configuration files required for the network'),
        Command('generate_crypto', generate_crypto_materials, 'Generate the certificates required for the network'),
        Command('find_sks', find_sks,
                'Find the names of private key files required for the network (required before inserting them)'),
        Command('insert_sks', insert_sks,
                'Insert the names of private key files into the generated configuration files'),
        Command('transfer_network', transfer_networkfiles,
                'Transfer all network-related files to Pis that need them'),
        Command('transfer_cc', transfer_ccfiles, 'Transfer Chaincode to Pis that need it'),
        Command('up', start_nodes, 'Start the network nodes'),
        Command('create_channel', create_channel, 'Create the channel'),
        Command('fetch', fetch_config_blocks, 'Make peers fetch the configuration block'),
        Command('join', join_peers_to_channel, 'Make peers join the channel'),
        Command('update_anchor_peers', update_anchor_peers,
                'Make anchor peers process the anchor peer update block'),
        Command('update_affiliations', update_affiliations, 'Update the affiliations of the CAs'),
        Command('install_cc', install_cc, 'Install the Chaincode on all peers (defunct in some setup due to Hyperledger'
                                          'bug)',
                in_pipeline=False),
        Command('install_cc_cli', install_cc_through_cli, 'Install the Chaincode on all peers, via the CLI (try this if'
                                                          ' the regular method gives a segfault)'),
        Command('instantiate_cc', instantiate_cc, 'Instantiate the Chaincode on the channel'),
        Command('start_local_cc', start_local_chaincodes, 'Start all local Chaincode containers'),
        Command('start_remote_cc', start_remote_chaincodes, 'Start al remote Chaincode containers'),
        Command('test_cc', test_cc, 'Execute test_cc'),
        Command('rejoin', rejoin_hosts, 'Rejoin nodes from a given set of hosts to the network (useful after a crash)',
                in_pipeline=False, consumes_args=True,
                param_help_info=[('SSH_HOSTNAME', True,
                                  'The SSH hostnames of all hosts in which nodes should be rejoined')]),
        Command('invoke_cc', invoke_cc, 'Invoke the Chaincode', in_pipeline=False, consumes_args=True,
                param_help_info=[('SRC_PEER_HOSTNAME_PREFIX', False,
                                  'Prefix of the peer in name of whom the invocation is to be sent'),
                                 ('VALIDATING_PEER_HOSTNAME_PREFIXES', False,
                                  'Comma-separated list of prefixes of peers of whom endorsement is to be requested'),
                                 ('FUNCTION', False, 'The Chaincode function to be called'),
                                 ('ARGS', True, 'The parameters to be passed')]),
        Command('query_cc', query_cc, 'Query the Chaincode (without generating a transaction)', in_pipeline=False,
                consumes_args=True, param_help_info=[('SRC_PEER_HOSTNAME_PREFIX', False,
                                                      'Prefix of the peer in name of whom the query is to be sent'),
                                                     ('VALIDATING_PEER_HOSTNAME_PREFIXES', False,
                                                      'Comma-separated list of prefixes of peers of whom endorsement'
                                                      ' is to be requested'),
                                                     ('FUNCTION', False, 'The Chaincode function to be called'),
                                                     ('ARGS', True, 'The parameters to be passed')])
    ])

    Command.execute_commands(sys.argv[1:])
