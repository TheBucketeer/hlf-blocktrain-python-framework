#!/usr/bin/python
# -*- coding: utf-8 -*-

import os


def helper(cmd):
    print(cmd)
    os.system(cmd)


helper('docker kill $(docker ps -a | grep -v CONTAINER | cut -d \' \' -f 1)')
helper('docker rm $(docker ps -a | grep -v CONTAINER | cut -d \' \' -f 1)')
helper('docker rmi --force $(docker images | grep dev | tr -s \' \' | cut -d \' \' -f 3)')
