import os

image_names = ['hyperledger/fabric-peer:amd64-1.4.1',
               'hyperledger/fabric-javaenv:amd64-1.4.1',
               'hyperledger/fabric-tools:amd64-1.4.1',
               'hyperledger/fabric-ccenv:amd64-1.4.1',
               'hyperledger/fabric-orderer:amd64-1.4.1',
               'hyperledger/fabric-peer:amd64-1.4.1',
               'hyperledger/fabric-ca:amd64-1.4.1',
               'hyperledger/fabric-zookeeper:amd64-0.4.15',
               'hyperledger/fabric-kafka:amd64-0.4.15',
               'hyperledger/fabric-couchdb:amd64-0.4.15',
               'hyperledger/fabric-baseimage:amd64-0.4.15',
               'hyperledger/fabric-baseos:amd64-0.4.15',
               'hyperledger/fabric-basejvm:amd64-0.4.15']

for name in image_names:
    os.system('docker pull ' + name)

os.system('docker images > tmp.out')
with open('tmp.out') as f:
    contents = f.read()
lines = contents.split('\n')
for line in lines:
    try:
        line_split = filter(None, line.split(' '))
    except Exception as e:
        continue
    if len(line_split) < 3:
        continue
    name = line_split[0]
    tag  = line_split[1]
    identity = line_split[2]
    if 'self' in tag or 'IMAGE' in identity:
        continue
    os.system('docker tag ' + identity + ' ' + name + ':latest')
os.system('rm tmp.ou')