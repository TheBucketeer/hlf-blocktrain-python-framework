import xlsxwriter
from collections import defaultdict

NOT_GIVEN = "CA not given"
INCORRECT = "Incorrect CA given"
OUT_OF_SYNC = "CA given too early or too late"
GIVEN_BUT_IGNORED = "CA given but not executed"
LONG_SHORT = "CA given too long or too short"

all_categories = [NOT_GIVEN, INCORRECT, OUT_OF_SYNC, GIVEN_BUT_IGNORED, LONG_SHORT]

cellwidths = {
    'CL': -1,
    'CA': -1,
    'UCA': -1,
    'CS': -1
}

unique_hazard_sets = {}

accidents = defaultdict(int)
hazards = defaultdict(lambda: defaultdict(int))
loops = defaultdict(int)
cas = defaultdict(lambda: defaultdict(int))
ucas = defaultdict(lambda: defaultdict(lambda: defaultdict(int)))
cses = defaultdict(int)
all_cses = []


cs_detection_map = {}
cs_mitigation_map = {}
cs_recovery_map = {}
cs_test_map = {}

rgb_to_hex = lambda r, g, b: '#' + hex(r)[2:] + hex(g)[2:] + hex(b)[2:]
dark_blue_rgb = rgb_to_hex(48, 84, 150)
very_dark_blue_rgb = rgb_to_hex(32, 55, 100)
light_blue_rgb = rgb_to_hex(214, 220, 228)
black_rgb = '#000000'
white_rgb = '#FFFFFF'


def merge_helper(sheet, start_row, start_col, end_row, end_col, contents, fmt=None):
    if fmt is None:
        fmt = {}
    if start_row == end_row and start_col == end_col:
        sheet.write(start_row, start_col, contents, fmt)
    else:
        sheet.merge_range(start_row, start_col, end_row, end_col, contents, fmt)


def make_format(align, valign, border_color, color, bg_color, bold=False):
    global workbook
    fmt = workbook.add_format({'bold': bold, 'bg_color': bg_color, 'border_color': border_color, 'color': color})
    fmt.set_align(align)
    fmt.set_align(valign)
    fmt.set_text_wrap()
    return fmt


def header_format(align, bg_color=dark_blue_rgb):
    global white_rgb
    return make_format(align, 'vcenter', white_rgb, white_rgb, bg_color, bold=True)


def cell_format(align, dark=False):
    global light_blue_rgb, white_rgb, black_rgb
    if dark:
        return make_format(align, 'vcenter', white_rgb, black_rgb, light_blue_rgb)
    else:
        return make_format(align, 'vcenter', white_rgb, black_rgb, white_rgb)


def row_format(dark=False):
    global light_blue_rgb, white_rgb, workbook
    if dark:
        workbook.add_format({'bg_color': light_blue_rgb})
    else:
        workbook.add_format({'bg_color': white_rgb})


def make_uca_header(sheet, start_row, start_col):
    row = start_row
    col = start_col
    merge_helper(sheet, row, col, row + 1, col, 'Control loop', header_format('center'))
    merge_helper(sheet, row, col + 1, row, col + 2, 'Control actions', header_format('center'))
    sheet.write(row + 1, col + 1, '#CA')
    sheet.write(row + 1, col + 2, 'Description')
    col += 3
    for category in all_categories:
        sheet.set_column(col, col, 10)
        sheet.set_column(col + 1, col + 1, 28)
        sheet.set_column(col + 2, col + 2, 15)
        merge_helper(sheet, row, col, row, col + 2, category, header_format('center'))
        sheet.write(row + 1, col, '#UCA', header_format('center'))
        sheet.write(row + 1, col + 1, 'Description', header_format('center'))
        sheet.write(row + 1, col + 2, 'Related hazards', header_format('center'))
        col += 3
    sheet.set_row(start_row, 15, header_format('left'))
    sheet.set_row(start_row + 1, 15, header_format('center'))
    sheet.set_column(start_col, start_col, 15, header_format('left'))
    sheet.set_column(start_col + 1, start_col + 1, 10, header_format('center'))
    sheet.set_column(start_col + 2, start_col + 2, 28, header_format('left'))


def make_cs_header(sheet, start_row, start_col):
    global very_dark_blue_rgb
    c_h_f = header_format('center')
    l_c_f = cell_format('left')
    merge_helper(sheet, start_row, start_col, start_row + 1, start_col, '#CS-cat', c_h_f)
    merge_helper(sheet, start_row, start_col + 1, start_row + 1, start_col + 1, 'Related UCAs', c_h_f)
    merge_helper(sheet, start_row, start_col + 2, start_row, start_col + 3, 'Causal scenario', c_h_f)
    sheet.write(start_row + 1, start_col + 2, '#CS', c_h_f)
    sheet.write(start_row + 1, start_col + 3, 'Description', c_h_f)
    merge_helper(sheet, start_row, start_col + 4, start_row + 1, start_col + 4, 'Detection', c_h_f)
    merge_helper(sheet, start_row, start_col + 5, start_row, start_col + 6, 'Mitigation measures', c_h_f)
    sheet.write(start_row + 1, start_col + 5, 'Description', c_h_f)
    sheet.write(start_row + 1, start_col + 6, 'Assigned to', c_h_f)
    merge_helper(sheet, start_row, start_col + 7, start_row + 1, start_col + 7, 'Recovery measures', c_h_f)
    merge_helper(sheet, start_row, start_col + 8, start_row + 1, start_col + 8, 'Test scenario', c_h_f)

    sheet.set_column(start_col, start_col, 8, c_h_f)
    sheet.set_column(start_col + 1, start_col + 1, 12, c_h_f)
    sheet.set_column(start_col + 2, start_col + 2, 8, header_format('right', bg_color=very_dark_blue_rgb))
    sheet.set_column(start_col + 3, start_col + 3, 40, l_c_f)
    sheet.set_column(start_col + 4, start_col + 4, 40, l_c_f)
    sheet.set_column(start_col + 5, start_col + 5, 55, l_c_f)
    sheet.set_column(start_col + 6, start_col + 6, 23, l_c_f)
    sheet.set_column(start_col + 7, start_col + 7, 25, l_c_f)
    sheet.set_column(start_col + 8, start_col + 8, 18, l_c_f)


def make_root_cs_header(sheet, start_row, start_col):
    l_c_f = cell_format('left')
    sheet.set_column(start_row, start_col, 40, l_c_f)
    sheet.set_column(start_row + 1, start_col + 1, 40, l_c_f)
    sheet.set_column(start_row + 2, start_col + 2, 40, l_c_f)
    sheet.set_column(start_row + 3, start_col + 3, 40, l_c_f)
    sheet.set_column(start_row + 4, start_col + 4, 40, l_c_f)
    sheet.set_column(start_row + 5, start_col + 5, 40, l_c_f)
    sheet.set_column(start_row + 6, start_col + 6, 40, l_c_f)
    sheet.set_column(start_row + 7, start_col + 7, 40, l_c_f)


def export_ucas(sheet, row, col):
    global loops
    is_dark = True
    for loop_id in sorted(list(loops.keys())):
        row, is_dark = loops[loop_id].to_uca_sheet(sheet, row, col, is_dark=is_dark)


def export_cses(sheet, row, col):
    global cses, all_cses
    is_dark = True
    for cs_id in sorted(list(cses.keys())):
        row, is_dark = cses[cs_id].to_cs_sheet(sheet, row, col, is_dark=is_dark)


def export_root_cses(sheet, row, col):
    global all_cses
    is_dark = True
    for cs in all_cses:
        if len(cs.reasons) == 0:
            cs.to_uca_sheet_as_root_cs(sheet, row, col, is_dark=is_dark)


class STPAObject(object):
    def __init__(self, desc, parent=None):
        self.desc = desc
        self.parent = parent
        self.n = None

    def get_full_id(self):
        assert self.n is not None
        result = str(self.n)
        i = self
        while i.parent is not None:
            i = i.parent
            assert i.n is not None
            result = str(i.n) + '.' + result
        return result


class Accident(STPAObject):
    def __init__(self, desc):
        global accidents
        STPAObject.__init__(self, desc)
        self.n = len(accidents) + 1
        accidents[self.n] = self


class Hazard(STPAObject):
    def __init__(self, category_n, desc, accidents=None):
        if accidents is None:
            accidents = []
        global hazards
        STPAObject.__init__(self, desc)
        self.category_n = category_n
        self.n = len(hazards[category_n]) + 1
        hazards[category_n][self.n] = self
        self.accidents = accidents

    def get_full_id(self):
        return str(self.category_n) + '.' + str(self.n)


class ControlLoop(STPAObject):
    def __init__(self, desc):
        global loops
        STPAObject.__init__(self, desc)
        self.cas = []
        self.n = len(loops) + 1
        loops[self.n] = self

    def add_ca(self, ca):
        self.cas.append(ca)

    def to_uca_sheet(self, sheet, start_row, start_col, is_dark=False):
        row = start_row
        col = start_col + 1
        for ca in self.cas:
            row, _ = ca.to_uca_sheet(sheet, row, col, is_dark)
            is_dark = not is_dark
        lowest_used_row = max(start_row, row - 1)
        merge_helper(sheet, start_row, start_col, lowest_used_row, start_col, self.get_full_id() + ': ' + self.desc, header_format('left'))
        return row, is_dark

    def to_string(self):
        global loop_width, DISTANCE

        output = str(self.n) + '\t' + self.desc + '\n'
        for ca in self.cas:
            output += ca.to_string()
        return output


class CA(STPAObject):
    def __init__(self, desc, loop):
        global cas
        STPAObject.__init__(self, desc, loop)
        self.ucas = []
        self.ucas_per_cat = defaultdict(list)
        self.control_loop = loop
        self.control_loop.add_ca(self)
        self.n = len(cas[loop.n]) + 1
        cas[loop.n][self.n] = self

    def add_uca(self, uca):
        self.ucas.append(uca)
        self.ucas_per_cat[uca.category].append(uca)

    def to_uca_sheet(self, sheet, start_row, start_col, is_dark=False):
        global all_categories
        # Make room for ID and description of the CA
        row = start_row
        col = start_col + 2
        max_n = max([len(l) for l in self.ucas_per_cat.values()])  # Maximum number of UCAs in one column for this CA
        for category in all_categories:
            n = len(self.ucas_per_cat[category])  # Number of UCAs in this column
            for i, uca in enumerate(self.ucas_per_cat[category]):
                from_row_offset = int(round(i * max_n / float(n)))
                to_row_offset = int(round((i + 1) * max_n / float(n)))
                uca.to_uca_sheet(sheet, row + from_row_offset, row + to_row_offset, col, is_dark=is_dark)
            col += 3
        lowest_used_row = max(start_row, start_row + max_n - 1)
        merge_helper(sheet, start_row, start_col, lowest_used_row, start_col, self.get_full_id(), header_format('center'))
        merge_helper(sheet, start_row, start_col + 1, lowest_used_row, start_col + 1, self.desc, header_format('left'))
        return lowest_used_row + 1, col

    def list_ucas(self):
        for uca in self.ucas:
            print(uca.desc)

    def get_full_id(self):
        return 'CA' + STPAObject.get_full_id(self)

    def to_string(self):
        global loop_width, DISTANCE

        output = loop_width * ' ' + str(self.n) + '\t' + self.desc + '\n'
        for uca in self.ucas:
            output += uca.to_string()
        return output


class UCA(STPAObject):
    def __init__(self, desc, CA, category, hazards):
        global ucas
        STPAObject.__init__(self, desc, CA)
        self.category = category
        self.cses = []
        self.parent.add_uca(self)
        self.n = len(ucas[self.parent.control_loop.n][self.parent.n]) + 1
        self.hazards = hazards
        ucas[self.parent.control_loop.n][self.parent.n][self.n] = self

    def add_cs(self, cs):
        self.cses.append(cs)

    # from_row - to_row is an exclusive range
    def to_uca_sheet(self, sheet, from_row, to_row, col, is_dark=False):
        global unique_hazard_sets
        for row in range(from_row, to_row):
            sheet.set_row(row, 60, row_format(is_dark))
        merge_helper(sheet, from_row, col, to_row - 1, col, self.get_full_id(), cell_format('center', is_dark))
        merge_helper(sheet, from_row, col + 1, to_row - 1, col + 1, self.desc, cell_format('left', is_dark))
        hazards_output = ', '.join(['H' + h.get_full_id() for h in self.hazards])
        if hazards_output in unique_hazard_sets:
            hazards_output = 'Same as ' + unique_hazard_sets[hazards_output].get_full_id()
        else:
            unique_hazard_sets[hazards_output] = self
        merge_helper(sheet, from_row, col + 2, to_row - 1, col + 2, hazards_output, cell_format('center', is_dark))
        return to_row, col + 3

    def get_full_id(self):
        return 'UCA' + STPAObject.get_full_id(self)

    def to_string(self):
        global ca_width
        output = ca_width * ' ' + str(self.n) + '\t' + self.desc + '\n'
        for cs in self.cses:
            for cs_string in cs.to_string():
                output += cs_string
        return output


class CS(STPAObject):
    def __init__(self, desc, is_head_cs=False, UCAs=[], reasons=[], detection='', mitigation='', recovery='', test='', responsible=[]):
        global cses, all_cses
        STPAObject.__init__(self, desc)
        self.n = None
        if is_head_cs:
            self.n = len(cses) + 1
            cses[self.n] = self
        all_cses.append(self)
        self.ucas = UCAs
        for uca in self.ucas:
            uca.add_cs(self)
        self.reasons = reasons
        for reason in reasons:
            reason.can_cause.append(self)
        self.can_cause = []
        self.detection = detection
        self.mitigation = mitigation
        self.recovery = recovery
        self.test = test
        self.responsible = responsible
        self.desc_separator = ' <- '
        self.other_str_separator = '/'
        if (len(reasons) > 0 and len(mitigation) > 0):
            print('len(reasons) > 0 and len(mitigation) > 0')
            self.dump()
        if (len(mitigation) > 0 and len(responsible) == 0):
            print('len(mitigation) > 0 and len(responsible) == 0')
            self.dump()
        if (is_head_cs and len(UCAs) == 0):
            print('is_head_cs and len(UCAs) == 0')
            self.dump()
        if (not is_head_cs and len(UCAs) > 0):
            print('not is_head_cs and len(UCAs) > 0')
            self.dump()

    def dump(self):
        print(self.desc, '|'.join([r.desc for r in self.reasons]), self.mitigation, '|'.join(self.responsible), '|'.join([uca.get_full_id() for uca in self.ucas]))

    def output_helper(self, output_str, mapping, cs_n):
        if len(output_str) == 0:
            return output_str
        if output_str in mapping.keys():
            return 'Same as ' + mapping[output_str]
        else:
            mapping[output_str] = self.get_full_id() + '.' + str(cs_n)
            return output_str

    def merge_strings_from_chain_helper(self, list_of_strings, delim=None):
        if delim is None:
            return self.other_str_separator.join(list(set(sorted([s for s in list_of_strings if len(s) > 0]))))
        else:
            return delim.join(list(set(sorted([s for s in list_of_strings if len(s) > 0]))))

    def to_cs_sheet(self, sheet, row, col, is_dark=False):
        global dark_blue_rgb, very_dark_blue_rgb, cs_detection_map, cs_mitigation_map, cs_recovery_map, cs_test_map
        n_cas = len(list(self.get_reason_chains()))
        merge_helper(sheet, row, col, row + n_cas - 1, col, self.get_full_id(), header_format('center', bg_color=dark_blue_rgb))
        col += 1
        ucas_str = ', '.join([uca.get_full_id() for uca in self.ucas])
        merge_helper(sheet, row, col, row + n_cas - 1, col, ucas_str, header_format('center', bg_color=dark_blue_rgb))
        for i, reason_chain in enumerate(self.get_reason_chains()):
            cs_n = i + 1
            sheet.write(row, col + 1, str(cs_n), header_format('right', bg_color=very_dark_blue_rgb))
            sheet.set_row(row, 105, row_format(is_dark))

            # Handle capitalisation
            descs = [cs.desc for cs in reason_chain]
            desc = self.desc_separator.join(descs)
            desc = desc[0].upper() + desc[1:]

            detection_str = self.output_helper(self.merge_strings_from_chain_helper([cs.detection for cs in reason_chain]), cs_detection_map, cs_n)
            mitigation_str = self.output_helper(self.merge_strings_from_chain_helper([cs.mitigation for cs in reason_chain]), cs_mitigation_map, cs_n)
            recovery_str = self.output_helper(self.merge_strings_from_chain_helper([cs.recovery for cs in reason_chain]), cs_recovery_map, cs_n)
            test_str = self.output_helper(self.merge_strings_from_chain_helper([cs.test for cs in reason_chain]), cs_test_map, cs_n)
            responsible_str = self.merge_strings_from_chain_helper(reduce(lambda a, b: a + b, [cs.responsible for cs in reason_chain]), delim=', ')

            def sheet_write_helper(col_to_write, str_to_write):
                if len(str_to_write) == 0:
                    str_to_write = 'N/A'
                sheet.write(row, col_to_write, str_to_write, cell_format('left', is_dark))
            sheet_write_helper(col + 2, desc)
            sheet_write_helper(col + 3, detection_str)
            sheet_write_helper(col + 4, mitigation_str)
            sheet_write_helper(col + 5, responsible_str)
            sheet_write_helper(col + 6, recovery_str)
            sheet_write_helper(col + 7, test_str)
            row += 1
            is_dark = not is_dark
        return row, is_dark

    def to_uca_sheet_as_root_cs(self, sheet, row, col, is_dark=False):
        for i, cause_chain in enumerate(self.get_cause_chains()):
            # ucas = reduce(lambda a, b: a + b, [cs.ucas for cs in cause_chain if len(cs.ucas) > 0])
            ucas = cause_chain[-1].ucas
            ucas_str = self.merge_strings_from_chain_helper(sorted([uca.get_full_id() for uca in ucas]), delim=', ')
            detection_str = self.merge_strings_from_chain_helper([cs.detection for cs in cause_chain])
            mitigation_str = self.merge_strings_from_chain_helper([cs.mitigation for cs in cause_chain])
            recovery_str = self.merge_strings_from_chain_helper([cs.recovery for cs in cause_chain])
            test_str = self.merge_strings_from_chain_helper([cs.test for cs in cause_chain])
            responsible_str = self.merge_strings_from_chain_helper(reduce(lambda a, b: a + b, [cs.responsible for cs in cause_chain]), delim=', ')
            def sheet_write_helper(col_to_write, str_to_write):
                if len(str_to_write) == 0:
                    str_to_write = 'N/A'
                sheet.write(row, col_to_write, str_to_write, cell_format('left', is_dark))
            sheet.set_row(row, 105, row_format(is_dark))
            sheet_write_helper(col, self.desc[0].upper() + self.desc[1:])
            sheet_write_helper(col + 1, detection_str)
            sheet_write_helper(col + 2, mitigation_str)
            sheet_write_helper(col + 3, responsible_str)
            sheet_write_helper(col + 4, recovery_str)
            sheet_write_helper(col + 5, test_str)
            sheet_write_helper(col + 6, ucas_str)
            row += 1
            is_dark = not is_dark
        return row, is_dark

    def get_reason_chains(self):
        if len(self.reasons) == 0:
            yield [self]
        else:
            for cs in self.reasons:
                for reason_chain in cs.get_reason_chains():
                    yield [self] + reason_chain

    def get_cause_chains(self):
        if len(self.can_cause) == 0:
            yield [self]
        else:
            for cs in self.can_cause:
                for cause_chain in cs.get_cause_chains():
                    yield [self] + cause_chain

    def get_descriptions(self):
        if len(self.reasons) == 0:
            yield self.desc
        else:
            for cs in self.reasons:
                for desc in cs.get_descriptions():
                    yield self.desc + self.desc_separator + desc

    def get_full_id(self):
        return 'CS-' + str(self.n)

    def to_string(self):
        global uca_width
        if len(self.reasons) == 0:
            yield uca_width * ' ' + str(self.n) + '\t' + self.desc
        else:
            for cs in self.reasons:
                for cs_str in cs.get_descriptions():
                    yield uca_width * ' ' + '<' + str(self.n) + '>\t' + self.desc + self.desc_separator + cs_str + '\n'


def do_stpa():
    # Define all variables resulting from the STPA process
    Accident("People die because of a train accident")
    Accident("Material losses")
    Accident("Environmental contamination")
    Accident("Disruption of service")
    Accident("Railway company loses reputation")
    Accident("Railway company loses money")

    Hazard(1, "Train to train collision", accidents=[accidents[i] for i in range(1, 6)])
    Hazard(1, "Train derailment", accidents=hazards[1][1].accidents)
    Hazard(1, "Train on fire", accidents=hazards[1][1].accidents)
    Hazard(2, "Person on railway gets hit by train", accidents=[accidents[i] for i in range(1, 5)])
    Hazard(2, "Train collides with non-train obstacle on the rails", accidents=hazards[2][1].accidents)
    Hazard(3, "Structural integrity of train compromised", [accidents[2], accidents[3], accidents[5]])
    Hazard(3, "Railway infrastructure damaged", accidents=[accidents[2], accidents[4]])
    Hazard(4, "Train schedule is not followed", accidents=[accidents[4], accidents[5]])
    Hazard(5, "No pretty lights")
    Hazard(6, "Railway company loses money", accidents=[accidents[6]])

    ControlLoop("CC <-> TrainPi")
    ControlLoop("TrainPi/SensorPi <-> Train")
    ControlLoop("CC <-> SwitchPi")
    ControlLoop("SwitchPi <-> switch")
    ControlLoop("CC <-> LevelCrossingPi")
    ControlLoop("LevelCrossingPi <-> Level Crossing Control System")
    ControlLoop("CC <-> SignalPi")
    ControlLoop("SignalPi <-> Signal")
    ControlLoop("Clients <-> Endorsing peers")
    ControlLoop("Clients -> Orderer -> All peers")
    ControlLoop("Clients <-> Orderer")

    CA("CC sends speed change event to TrainPi", loops[1])
    CA("CC sends direction change event to TrainPi", loops[1])
    CA("TrainPi sends speed change command to train", loops[2])
    CA("TrainPi sends direction change command to train", loops[2])
    CA("CC sends switch state change event to SwitchPi", loops[3])
    CA("SwitchPi sends switch state change command to switch", loops[4])
    CA("CC sends state change event to LevelCrossingPi", loops[5])
    CA("LevelCrossingPi sends state change command to Level Crossing Control System", loops[6])
    CA("CC sends aspect change event from green/yellow to red to SignalPi", loops[7])
    CA("SignalPi sends aspect change command to signal", loops[8])
    CA("Endorsing peers verify transaction/invocation proposed by client", loops[9])
    CA("Orderer commits verified transaction to a block and spreads this new block to all peers (via anchor peers)", loops[10])
    CA("Orderer verifies that transaction is sufficiently endorsed", loops[11])

    UCA("TrainPi does not receive event to decrease speed when needed", cas[1][1], NOT_GIVEN, [hazards[1][1], hazards[1][2], hazards[1][3], hazards[2][1], hazards[2][2], hazards[3][2]])
    UCA("TrainPi does not receive event to increase speed when needed", cas[1][1], NOT_GIVEN, [hazards[1][1], hazards[4][1]])
    UCA("TrainPi receives speed event with too high a speed (including driving when braking is correct)", cas[1][1], INCORRECT, ucas[1][1][1].hazards)
    UCA("TrainPi receives speed event with too low a speed (including braking when driving is correct)", cas[1][1], INCORRECT, ucas[1][1][2].hazards)
    UCA("TrainPi receives event to increase speed before it is safe to do so", cas[1][1], OUT_OF_SYNC, ucas[1][1][1].hazards)
    UCA("TrainPi receives event to decrease speed before it is necessary/safe to do so", cas[1][1], OUT_OF_SYNC, ucas[1][1][2].hazards)
    UCA("TrainPi receives event to increase speed too late", cas[1][1], OUT_OF_SYNC, ucas[1][1][2].hazards)
    UCA("TrainPi receives event to decrease speed too late", cas[1][1], OUT_OF_SYNC, ucas[1][1][1].hazards)
    UCA("TrainPi receives event to increase speed but does not relay this signal to the train", cas[1][1], GIVEN_BUT_IGNORED, ucas[1][1][2].hazards)
    UCA("TrainPi receives event to decrease speed but does not relay this signal to the train", cas[1][1], GIVEN_BUT_IGNORED, ucas[1][1][1].hazards)

    UCA("TrainPi does not receive direction change event when needed", cas[1][2], NOT_GIVEN, [hazards[1][1], hazards[1][2], hazards[1][3], hazards[2][1], hazards[2][2], hazards[3][1], hazards[3][2], hazards[4][1]])
    UCA("TrainPi receives direction change event for the wrong direction", cas[1][2], INCORRECT, ucas[1][2][1].hazards)
    UCA("TrainPi receives direction change event before it is necessary/safe to do so", cas[1][2], OUT_OF_SYNC, ucas[1][2][1].hazards)
    UCA("TrainPi receives direction change event too late", cas[1][2], OUT_OF_SYNC, ucas[1][2][1].hazards)
    UCA("TrainPi receives event to change direction but does not relay this signal to the train", cas[1][2], GIVEN_BUT_IGNORED, ucas[1][2][1].hazards)

    UCA("Train does not receive command to decrease speed when needed", cas[2][1], NOT_GIVEN, ucas[1][1][1].hazards)
    UCA("Train does not receive command to increase speed when needed", cas[2][1], NOT_GIVEN, ucas[1][1][2].hazards)
    UCA("Train receives speed command with too high a speed", cas[2][1], INCORRECT, ucas[1][1][1].hazards)
    UCA("Train receives speed command with too low a speed", cas[2][1], INCORRECT, ucas[1][1][2].hazards)
    UCA("Train receives command to increase speed before it is safe to do so", cas[2][1], OUT_OF_SYNC, ucas[1][1][1].hazards)
    UCA("Train receives command to decrease speed before it is necessary/safe to do so", cas[2][1], OUT_OF_SYNC, ucas[1][1][2].hazards)
    UCA("Train receives command to increase speed too late", cas[2][1], OUT_OF_SYNC, ucas[1][1][2].hazards)
    UCA("Train receives command to decrease speed too late", cas[2][1], OUT_OF_SYNC, ucas[1][1][1].hazards)
    UCA("Train receives command to increase speed, but does not obey", cas[2][1], GIVEN_BUT_IGNORED, ucas[1][1][2].hazards)
    UCA("Train receives command to decrease speed, but does not obey", cas[2][1], GIVEN_BUT_IGNORED, ucas[1][1][1].hazards)

    UCA("Train does not receive commands to change direction when needed", cas[2][2], NOT_GIVEN, ucas[1][2][1].hazards)
    UCA("Train receives direction change commands for the wrong direction", cas[2][2], INCORRECT, ucas[1][2][1].hazards)
    UCA("Train receives direction change commands before it is safe/necessary to do so", cas[2][2], OUT_OF_SYNC, ucas[1][2][1].hazards)
    UCA("Train receives direction change commands too late", cas[2][2], OUT_OF_SYNC, ucas[1][2][1].hazards)
    UCA("Train receives direction change commands but does not obey", cas[2][2], GIVEN_BUT_IGNORED, ucas[1][2][1].hazards)

    UCA("SwitchPi does not receive state change event when needed", cas[3][1], NOT_GIVEN, [hazards[1][1], hazards[1][2], hazards[1][3], hazards[2][1], hazards[2][2], hazards[3][2], hazards[4][1]])
    UCA("SwitchPi receives incorrect state change event", cas[3][1], INCORRECT, ucas[3][1][1].hazards)
    UCA("SwitchPi receives state change event at an unsafe moment (ie. 1) a train is on it, 2) the train it was switched for has yet to pass it or 3) the switch flip is too late and train it was switched for has already passed it)", cas[3][1], OUT_OF_SYNC, ucas[3][1][1].hazards)
    UCA("SwitchPi receives state change event but does not relay this signal to the switch", cas[3][1], GIVEN_BUT_IGNORED, ucas[3][1][1].hazards)

    UCA("Switch does not receive state change command when needed", cas[4][1], NOT_GIVEN, ucas[3][1][1].hazards)
    UCA("Switch receives incorrect state change command", cas[4][1], INCORRECT, ucas[3][1][1].hazards)
    UCA("Switch receives state change command at the wrong moment", cas[4][1], OUT_OF_SYNC, ucas[3][1][1].hazards)
    UCA("Switch receives state change event but does not obey", cas[4][1], GIVEN_BUT_IGNORED, ucas[3][1][1].hazards)

    UCA("LevelCrossingPi does not receive event to close booms when needed", cas[5][1], NOT_GIVEN, [hazards[2][1], hazards[2][2], hazards[3][1], hazards[3][2], hazards[4][1]])
    UCA("LevelCrossingPi does not receive event to open booms when needed", cas[5][1], NOT_GIVEN, [hazards[4][1]])
    UCA("LevelCrossingPi receives event to close booms, when opening them is correct", cas[5][1], INCORRECT, [hazards[3][2], hazards[4][1]])
    UCA("LevelCrossingPi receives event to open booms, when closing them is correct", cas[5][1], INCORRECT, ucas[5][1][1].hazards)
    UCA("LevelCrossingPi receives event to close booms before it is safe/necessary to do so", cas[5][1], OUT_OF_SYNC, ucas[5][1][3].hazards)
    UCA("LevelCrossingPi receives event to open booms before it is safe to do so", cas[5][1], OUT_OF_SYNC, ucas[5][1][1].hazards)
    UCA("LevelCrossingPi receives event to close booms too late", cas[5][1], OUT_OF_SYNC, ucas[5][1][1].hazards)
    UCA("LevelCrossingPi receives event to open booms too late", cas[5][1], OUT_OF_SYNC, ucas[5][1][2].hazards)
    UCA("LevelCrossingPi receives event to close booms, but does not relay this signal to the Level Crossing Control System", cas[5][1], GIVEN_BUT_IGNORED, ucas[5][1][1].hazards)
    UCA("LevelCrossingPi receives event to open booms, but does not relay this signal to the Level Crossing Control System", cas[5][1], GIVEN_BUT_IGNORED, ucas[5][1][2].hazards)

    UCA("Level Crossing Control System does not receive command to close booms when needed", cas[6][1], NOT_GIVEN, ucas[5][1][1].hazards)
    UCA("Level Crossing Control System does not receive command to open booms when needed", cas[6][1], NOT_GIVEN, ucas[5][1][2].hazards)
    UCA("Level Crossing Control System receives command to close booms, when opening them is correct", cas[6][1], INCORRECT, ucas[5][1][3].hazards)
    UCA("Level Crossing Control System receives command to open booms, when closing them is correct", cas[6][1], INCORRECT, ucas[5][1][1].hazards)
    UCA("Level Crossing Control System receives command to close booms before it is safe/necessary to do so", cas[6][1], OUT_OF_SYNC, ucas[5][1][3].hazards)
    UCA("Level Crossing Control System receives command to open booms before it is safe to do so", cas[6][1], OUT_OF_SYNC, ucas[5][1][1].hazards)
    UCA("Level Crossing Control System receives command to close booms too late", cas[6][1], OUT_OF_SYNC, ucas[5][1][1].hazards)
    UCA("Level Crossing Control System receives command to open booms too late", cas[6][1], OUT_OF_SYNC, ucas[5][1][2].hazards)
    UCA("Level Crossing Control System receives command to close booms but does not obey", cas[6][1], GIVEN_BUT_IGNORED, ucas[5][1][1].hazards)
    UCA("Level Crossing Control System receives command to open booms but does not obey", cas[6][1], GIVEN_BUT_IGNORED, ucas[5][1][2].hazards)
    UCA("Level Crossing Control System receives command to close booms too long", cas[6][1], LONG_SHORT, [hazards[3][2]])
    UCA("Level Crossing Control System receives command to open booms too long", cas[6][1], LONG_SHORT, ucas[6][1][11].hazards)
    UCA("Level Crossing Control System receives command to close booms too short", cas[6][1], LONG_SHORT, ucas[6][1][11].hazards)
    UCA("Level crossing Control System receives command to open booms too short", cas[6][1], LONG_SHORT, ucas[6][1][11].hazards)

    UCA("SignalPi does not receive event to change aspect from green/yellow to red", cas[7][1], NOT_GIVEN, [hazards[5][1]])
    UCA("SignalPi does not receive event to change aspect from green to yellow", cas[7][1], NOT_GIVEN, [hazards[5][1]])
    UCA("SignalPi does not receive event to change aspect from red to yellow/green, or from yellow to green", cas[7][1], NOT_GIVEN, [hazards[5][1]])
    UCA("SignalPi receives event to change aspect to green/yellow when red is correct", cas[7][1], INCORRECT, ucas[7][1][1].hazards)
    UCA("SignalPi receives event to change aspect to green, when yellow is correct", cas[7][1], INCORRECT, ucas[7][1][2].hazards)
    UCA("SignalPi receives event to change aspect to yellow/red, when green is correct", cas[7][1], INCORRECT, ucas[7][1][3].hazards)
    UCA("SignalPi receives event to change aspect to red before it is necessary to do so", cas[7][1], OUT_OF_SYNC, ucas[7][1][3].hazards)
    UCA("SignalPi receives event to change aspect to red too late", cas[7][1], OUT_OF_SYNC, ucas[7][1][1].hazards)
    UCA("SignalPi receives event to change aspect from green to yellow too late", cas[7][1], OUT_OF_SYNC, ucas[7][1][2].hazards)
    UCA("SignalPi receives event to change aspect from green to yellow before it necessary to do so", cas[7][1], OUT_OF_SYNC, ucas[7][1][3].hazards)
    UCA("SignalPi receives event to change aspect from red to yellow/green before it is safe to do so", cas[7][1], OUT_OF_SYNC, ucas[7][1][1].hazards)
    UCA("SignalPi receives event to change aspect from red to yellow/green too late", cas[7][1], OUT_OF_SYNC, ucas[7][1][3].hazards)
    UCA("SignalPi receives event to change aspect to red, but does not relay this signal to the signal", cas[7][1], GIVEN_BUT_IGNORED, ucas[7][1][1].hazards)
    UCA("SignalPi receives event to change aspect from green to yellow, but does not relay this signal to the signal", cas[7][1], GIVEN_BUT_IGNORED, ucas[7][1][2].hazards)
    UCA("SignalPi receives event to change aspect to green, but does not relay this signal to the signal", cas[7][1], GIVEN_BUT_IGNORED, ucas[7][1][3].hazards)

    UCA("Signal does not receive command to change aspect from green/yellow to red", cas[8][1], NOT_GIVEN, ucas[7][1][1].hazards)
    UCA("Signal does not receive command to change aspect from green to yellow", cas[8][1], NOT_GIVEN, ucas[7][1][2].hazards)
    UCA("Signal does not receive command to change aspect from red to yellow/green, or from yellow to green", cas[8][1], NOT_GIVEN, ucas[7][1][3].hazards)
    UCA("Signal receives command to change aspect to green/yellow when red is correct", cas[8][1], INCORRECT, ucas[7][1][1].hazards)
    UCA("Signal receives command to change aspect to green, when yellow is correct", cas[8][1], INCORRECT, ucas[7][1][2].hazards)
    UCA("Signal receives command to change aspect to yellow/red, when green is correct", cas[8][1], INCORRECT, ucas[7][1][3].hazards)
    UCA("Signal receives command to change aspect to red before it is necessary to do so", cas[8][1], OUT_OF_SYNC, ucas[7][1][3].hazards)
    UCA("Signal receives command to change aspect to red too late", cas[8][1], OUT_OF_SYNC, ucas[7][1][1].hazards)
    UCA("Signal receives command to change aspect from green to yellow before it necessary to do so", cas[8][1], OUT_OF_SYNC, ucas[7][1][3].hazards)
    UCA("Signal receives command to change aspect from green to yellow too late", cas[8][1], OUT_OF_SYNC, ucas[7][1][2].hazards)
    UCA("Signal receives command to change aspect from red to yellow/green before it is safe to do so", cas[8][1], OUT_OF_SYNC, ucas[7][1][1].hazards)
    UCA("Signal receives command to change aspect from red to yellow/green too late", cas[8][1], OUT_OF_SYNC, ucas[7][1][3].hazards)
    UCA("Signal receives command to change aspect to red, but does not obey", cas[8][1], GIVEN_BUT_IGNORED, ucas[7][1][1].hazards)
    UCA("Signal receives command to change aspect from green to yellow, but does not obey", cas[8][1], GIVEN_BUT_IGNORED, ucas[7][1][2].hazards)
    UCA("Signal receives command to change aspect to green, but does not obey", cas[8][1], GIVEN_BUT_IGNORED, ucas[7][1][3].hazards)

    UCA("Client does not receive verified transaction from endorsing peers", cas[9][1], NOT_GIVEN, [hazards[1][1], hazards[1][2], hazards[1][3], hazards[2][1], hazards[2][2], hazards[3][1], hazards[3][2], hazards[4][1]])
    UCA("Endorsing peers do not verify valid transaction", cas[9][1], INCORRECT, ucas[9][1][1].hazards)
    UCA("Endorsing peers verify invalid transaction", cas[9][1], INCORRECT, ucas[9][1][1].hazards)
    UCA("Endorsing peers verify valid transaction too late", cas[9][1], OUT_OF_SYNC, ucas[9][1][1].hazards)
    UCA("Endorsing peers verify valid transaction, but client does not process it any further", cas[9][1], GIVEN_BUT_IGNORED, ucas[9][1][1].hazards)

    UCA("Orderer does not incorporate verified transaction into a block", cas[10][1], NOT_GIVEN, ucas[9][1][1].hazards)
    UCA("Peers do not receive new block from orderer", cas[10][1], NOT_GIVEN, ucas[9][1][1 ].hazards)
    UCA("Orderer sends incorrect information/block to peers", cas[10][1], INCORRECT, ucas[9][1][1].hazards)
    UCA("Orderer sends blocks to peers in the wrong order", cas[10][1], OUT_OF_SYNC, ucas[9][1][1].hazards)
    UCA("Orderer takes too long to send new block to peers", cas[10][1], OUT_OF_SYNC, ucas[9][1][1].hazards)
    UCA("Orderer sends block to peers, but the peers do not accept/store/process it", cas[10][1], GIVEN_BUT_IGNORED, ucas[9][1][1].hazards)

    UCA("Orderer does not verify if transaction is sufficiently endorsed", cas[11][1], NOT_GIVEN, ucas[9][1][1].hazards)
    UCA("Orderer incorrectly decides a transaction is sufficiently endorsed", cas[11][1], INCORRECT, ucas[9][1][1].hazards)
    UCA("Orderer incorrectly decides a transaction is not sufficiently endorsed", cas[11][1], INCORRECT, ucas[9][1][1].hazards)
    UCA("Orderer takes too long to process an incoming transaction", cas[11][1], OUT_OF_SYNC, ucas[9][1][1].hazards)

    # Detection/mitigation/test/recovery stirngs that are used multiple times
    attempted_invocation_detection = 'When the Pi client attempts to communicate with Blockchain network'
    log_detection = 'Logging'
    xcheck_cmds_sensor_data_detection = 'In the Chaincode, cross-check commands sent to Train Pis with incoming sensor invocations'
    client_auth_mitigation = 'Ensure that clients must authenticate themselves using a secret, predistributed key (done by Hyperledger). Keep the Pi clients themselves secure. Encrypt all network traffic from and to the clients (done by Hyperledger).'
    asset_conn_unreachable_mitigation = 'Ensure that Pi client - asset connections are unreachable for adversaries.'
    bc_network_unreachable_mitigation = 'Prevent adversaries from having physical access to the network and the Pi clients'
    cc_bb_test = 'Black-box testing of the Chaincode'
    cc_wb_test = 'White-box testing of the Chaincode'
    pen_test = 'Penetration testing'
    halt_system_recovery = 'Stop trains and halt the system'
    safe_state_recovery = 'Pi client must enter safest possible state'

    # Adversaries acting up again
    cc_pc_comp = CS('computer used to set up Blockchain network was compromised', mitigation='Secure the computer used to start the Blockchain network.', test=pen_test, responsible=['HW', 'SW', 'Organisation'])
    client_comp = CS('Pi client was taken control of by adversary', mitigation='Secure the Pi clients (firewall, strong passwords, etc.). Prevent adversaries from having physical access to the Pi clients.', test='Mimic the Pi client and send bogus Chaincode invocations in its name.', responsible=['HW', 'Organisation'])
    dcc_pc_comp = CS('computer running DCC server was taken control of by adversary', mitigation='Secure the computer running the DCC server', test=pen_test, responsible=['HW', 'SW', 'Organisation'])
    cc_tampered = CS('Chaincode itself was tampered with before deployment', reasons=[cc_pc_comp])
    tt_tampered = CS('track topology model was tampered with before deployment', reasons=[cc_pc_comp])
    client_poser = CS('adversary managed to fabricate Pi client invocations', mitigation=client_auth_mitigation, responsible=['HW', 'SW', 'Organisation'])
    asset_injection = CS('adversary managed to inject asset feedback to Pi client', mitigation=asset_conn_unreachable_mitigation, responsible=['HW', 'Organisation'])
    asset_blocked = CS('adversary blocked communication between Pi client and asset', mitigation=asset_conn_unreachable_mitigation, responsible=['HW', 'Organisation'])
    client_blocked = CS('adversary blocked communication between Pi client and Blockchain network', mitigation='Ensure that the network infrastructure is unreachable for adversaries.', responsible=['HW', 'Organisation'])
    event_modified = CS('adversary intercepted and replaced event by incorrect event, using for example a replay attack', mitigation='Prevent adversaries from having physical access to the network and the Pi clients. Also, use nonces or other unique identifiers to prevent replay attacks.', test='Modify the code such that Pi client processes a different event than it received.', responsible=['HW', 'SW', 'Organisation'])
    command_modified = CS('adversary intercepted and replaced command by incorrect command', mitigation=asset_conn_unreachable_mitigation, test='Modify the code such that Pi client sends a different command than intended.', responsible=['HW', 'Organisation'])
    dos_attack = CS('adversary was performing DoS attack', mitigation=bc_network_unreachable_mitigation + ' Also, avoid having bottlenecks (or single points of failure) in the network topology.', responsible=['HW', 'Organisation'])
    command_length_tampered = CS('adversary was prolonging/shortening amount of time command was applied for', mitigation=bc_network_unreachable_mitigation, responsible=['HW', 'Organisation'])
    # Human error
    prog_error = CS('programming mistake', detection=cc_bb_test, mitigation=cc_wb_test, responsible=['SW'])
    oversight = CS('oversight by programmer/operator', mitigation='In the Chaincode, implement logic for determining if the given track topology is valid.', test='Run the system, check for unexpected behaviours', responsible=['SW'])
    reboot_time = CS('Pi client had been running for too long', detection='System-specific commands to find moment of last reboot', mitigation='Reboot the Pis at least once a month', test='Let a Pi run for more than a month and observe what happens', responsible=['Organisation'])
    tracks_off = CS('tracks were not powered on', mitigation='Keep the tracks powered on by default, except during maintenance/emergencies', responsible=['Organisation'])
    dcc_booster_off = CS('DCC booster was not powered on', mitigation='Keep the DCC booster on by default, except during maintenance/emergencies', responsible=['Organisation'])
    dcc_pc_off = CS('computer running DCC server was not powered on', mitigation='Keep the PC running the DCC server on by default', responsible=['Organisation'])
    # Broken things
    proper_pi_mitigation = 'Use Pis of a model that is powerful enough for this application'
    unsuited_pi_model = CS('Pi was of insufficient quality', mitigation=proper_pi_mitigation, responsible=['Organisation'])
    overloaded_client = CS('Pi client was overloaded', reasons=[prog_error, unsuited_pi_model])
    dcc_pc_overloaded = CS('computer running DCC server was overloaded', mitigation='Do not run unnecessary processes on the computer running the DCC server', responsible=['Organisation'])
    no_bad_hw_mitigation = 'Use high-quality, new hardware that has not yet been dropped in water. Also, ensure a proper maintenance schedule is followed.'
    broken_hardware = CS('hardware failure', mitigation=no_bad_hw_mitigation, responsible=['HW', 'Organisation'])
    broken_software = CS('software failure', reasons=[prog_error, reboot_time])
    broken_client = CS('non-functional Pi client', reasons=[broken_hardware, broken_software, overloaded_client], test='Disable the Pi client')
    bad_asset = CS('broken asset', mitigation=no_bad_hw_mitigation, test='Replace the Pi client\'s asset with a fabricated signal containing no/bad data', responsible=['HW', 'Organisation'])
    bad_infrastructure = CS('faulty infrastructure (cabling)', mitigation=no_bad_hw_mitigation, test='Remove the involved cable and test how the system responds', responsible=['HW', 'Organisation'])
    slow_client = CS('Pi client was too slow to respond', reasons=[overloaded_client])
    slow_infrastructure = CS('slow infrastructure (cables too long/of poor quality)', detection='Hardware benchmarks before deployment', mitigation=no_bad_hw_mitigation + ' Do not use cables that are longer than necessary', responsible=['HW', 'Organisation'])
    high_voltage_off = CS('high-voltage power supply was turned off', mitigation='Prevent the system from being capable of turning off the high-voltage power supply', test='Manually turn off the high-voltage power supply', responsible=['HW, Organisation'])

    # Train-specific errors
    track_dirt = CS('dirt between rails and the train\'s wheels', mitigation='Clean the tracks frequently', responsible=['Organisation'])
    train_disconnect = CS('train not connected to rails', reasons=[track_dirt])
    bad_train_decoder = CS('train\'s decoder was programmed incorrectly', mitigation='Test the programming of the decoder before deployment', recovery=halt_system_recovery, responsible=['Organisation'])
    # Switch-specific errors
    switch_too_fast = CS('switch received commands in too rapid a succession', mitigation='Prevent the Pi client from sending commands to the switch too quickly', recovery='Wait a certain amount of time, then repeat the latest command', responsible=['SW'])
    # Level crossing-specific error
    boom_obstruction = CS('obstruction', mitigation='Keep the area around the booms free of obstructions', recovery=halt_system_recovery, responsible=['Organisation'])
    lc_servos_stuck = CS('servos are unable to move', reasons=[boom_obstruction, broken_hardware])
    # Magnet Myrtle
    magnet_myrtle = CS('electromagnetic interference', mitigation='Position the cables such that electromagnetic interference becomes impossible.', test='Run the system, check for unexpected sensor readings', recovery=halt_system_recovery, responsible=['HW', 'Organisation'])
    # Network stuff
    cc_network_down = CS('Blockchain network was down', detection=attempted_invocation_detection, mitigation=no_bad_hw_mitigation, recovery=safe_state_recovery, test='Disable the Blockchain network', responsible=['SW'])
    too_much_traffic = CS('system generated too much traffic for network to handle', mitigation='Do not generate more traffic than necessary.', responsible=['SW'])
    channel_congested = CS('required communication channel was congested with traffic', reasons=[dos_attack, too_much_traffic], mitigation='Use a suitable network topology, avoiding bottlenecks or single points of failure.', test='Generate an unrealistically large amount of traffic and observe the system\'s response.', responsible=['HW', 'SW', 'Organisation'])
    channel_down = CS('required communication channel was down', reasons=[bad_infrastructure, client_blocked, channel_congested], detection=attempted_invocation_detection, recovery=safe_state_recovery, test='Manually remove all network cables from the involved Pi client, observe the system\'s response.')
    # Errors with Pi clients
    bad_sensor_data = CS('incorrect/missing sensor feedback', reasons=[asset_blocked, asset_injection, bad_asset, magnet_myrtle])
    bad_switch_data = CS('incorrect/missing switch feedback', reasons=[asset_blocked, asset_injection, bad_asset])
    late_asset_data = CS('delayed feedback from asset', reasons=[channel_congested, slow_infrastructure])
    bad_sensorpi_data = CS('incorrect/missing SensorPi feedback invocation', reasons=[broken_client, bad_sensor_data, channel_down, client_comp, client_poser])
    bad_switchpi_data = CS('incorrect/missing SwitchPi feedback invocation', reasons=[broken_client, bad_switch_data, channel_down, client_comp, client_poser])
    late_pi_data = CS('delayed feedback invocation from Pi client', reasons=[channel_congested, late_asset_data, slow_client, slow_infrastructure])
    # Bad information in Chaincode
    cc_logic_error = CS('Chaincode logic error', reasons=[cc_tampered, prog_error])
    incorrect_mapping = CS('incorrect internal mapping of tracks in Chaincode', reasons=[oversight, tt_tampered], detection='Partially (sanity checks over the tracklayout)')
    incorrect_tp_model = CS('incorrect internal train positions model in Chaincode', reasons=[bad_sensorpi_data], detection='In the Chaincode, implement logic for determining whether or not a sensor reading is sensible by performing cross-checks with other SensorPi feedback')
    incorrect_top_model = CS('incorrect internal track topology model in Chaincode', reasons=[bad_switchpi_data], detection='In the Chaincode, detect unexpected switch positions using generated sensor readings.')
    delayed_tp_top_model = CS('delayed update to internal train positions/track topology models in Chaincode', reasons=[late_pi_data], detection='Hardware benchmarks before deployment, stress testing of the entire system.')
    # Broken Hyperledger stuff
    broken_event_system = CS('error in Hyperledger event generation system', detection=cc_bb_test, mitigation='None, we assume Hyperledger is functional', responsible=['Organisation'])
    # Root CSes
    event_not_sent = CS('event not sent', reasons=[broken_event_system, cc_logic_error, cc_network_down, incorrect_mapping, incorrect_top_model, incorrect_tp_model])
    event_not_received = CS('event not received', is_head_cs=True, reasons=[event_not_sent, channel_down], UCAs=[ucas[1][1][1], ucas[1][1][2], ucas[1][2][1], ucas[3][1][1], ucas[5][1][1], ucas[5][1][2], ucas[7][1][1], ucas[7][1][2], ucas[7][1][3]])

    incorrect_event_sent = CS('incorrect event sent', reasons=[cc_logic_error, incorrect_mapping, incorrect_top_model, incorrect_tp_model])
    correct_event_sent_but_arrived_incorrectly = CS('correct event sent, but arrived in incorrect state', reasons=[event_modified])
    incorrect_event_received = CS('incorrect event received', is_head_cs=True, reasons=[incorrect_event_sent, correct_event_sent_but_arrived_incorrectly], UCAs=[ucas[1][1][3], ucas[1][1][4], ucas[1][1][5], ucas[1][1][6], ucas[1][2][2], ucas[1][2][3], ucas[3][1][2], ucas[3][1][3], ucas[5][1][3], ucas[5][1][4], ucas[5][1][5], ucas[5][1][6], ucas[7][1][4], ucas[7][1][5], ucas[7][1][6], ucas[7][1][7], ucas[7][1][9], ucas[7][1][11]])
    event_sent_late = CS('event sent too late', reasons=[broken_event_system, cc_logic_error, delayed_tp_top_model])
    event_received_late = CS('event received too late', is_head_cs=True, reasons=[event_sent_late, channel_congested], UCAs=[ucas[1][1][7], ucas[1][1][8], ucas[1][2][4], ucas[3][1][3], ucas[5][1][7], ucas[5][1][8], ucas[7][1][8], ucas[7][1][10], ucas[7][1][12]])

    event_not_relayed = CS('event received, but corresponding command not relayed to asset', is_head_cs=True, reasons=[broken_client, client_comp], UCAs=[ucas[1][1][9], ucas[1][1][10], ucas[1][2][5], ucas[3][1][4], ucas[5][1][9], ucas[5][1][10], ucas[7][1][13], ucas[7][1][14], ucas[7][1][15]])
    CS('train received command but did not obey', is_head_cs=True, reasons=[bad_train_decoder, broken_hardware, train_disconnect], detection=xcheck_cmds_sensor_data_detection, test='Manually move (or stop) a train in a way that does not correspond with the commands sent to it', responsible=['SW'], UCAs=[ucas[2][1][9], ucas[2][1][10], ucas[2][2][5]])
    CS('twitch received command but did not obey', is_head_cs=True, reasons=[switch_too_fast], detection='Using the switch\'s built-in feedback lines', UCAs=[ucas[4][1][4]])

    CS('level crossing control system received command to open booms but did not obey', is_head_cs=True, reasons=[lc_servos_stuck], UCAs=[ucas[6][1][9], ucas[6][1][10]])

    CS('signal received aspect change command but did not obey', is_head_cs=True, reasons=[broken_hardware], detection='Using the signal\'s feedback lines', UCAs=[ucas[8][1][13], ucas[8][1][14], ucas[8][1][15]])

    command_not_sent = CS('required command not sent', reasons=[event_not_received, event_not_relayed])
    command_not_received_by_dcc_pc = CS('required command sent but did not arrive DCC server', reasons=[channel_down])
    command_received_but_no_dcc_signal = CS('required command reached DCC server but no DCC signal arrived at train', reasons=[dcc_booster_off, dcc_pc_off, tracks_off])
    CS('Train did not receive command to change speed/direction when needed', is_head_cs=True, reasons=[command_not_sent, command_not_received_by_dcc_pc, command_received_but_no_dcc_signal], detection=xcheck_cmds_sensor_data_detection, responsible=['SW'], UCAs=[ucas[2][1][1], ucas[2][1][2], ucas[2][2][1]])

    incorrect_command_sent = CS('incorrect command sent', reasons=[broken_software, client_comp, incorrect_event_received], detection=log_detection)
    incorrect_command_received_by_dcc_pc = CS('correct command sent, but arrived in incorrect state at DCC server', reasons=[bad_infrastructure, command_modified])
    correct_command_but_bad_dcc_signal = CS('correct command received by DCC server, but outputted incorrect DCC signal', reasons=[prog_error, dcc_pc_comp])
    CS('Train received incorrect speed/direction change command', is_head_cs=True, reasons=[incorrect_command_sent, incorrect_command_received_by_dcc_pc, correct_command_but_bad_dcc_signal], detection=xcheck_cmds_sensor_data_detection, UCAs=[ucas[2][1][3], ucas[2][1][4], ucas[2][2][2], ucas[2][1][5], ucas[2][1][6], ucas[2][2][3]])

    command_sent_late = CS('required command sent too late', reasons=[event_received_late, slow_client])
    command_received_late_by_dcc_pc = CS('required command sent on time but arrived late at DCC server', reasons=[channel_congested, slow_infrastructure])
    dcc_command_sent_late = CS('required command received on time by DCC server, but DCC signal came too late', reasons=[dcc_pc_comp, dcc_pc_overloaded])
    CS('Train received speed/direction change command too late', is_head_cs=True, reasons=[command_sent_late, command_received_late_by_dcc_pc, dcc_command_sent_late], detection=xcheck_cmds_sensor_data_detection, UCAs=[ucas[2][1][7], ucas[2][1][8], ucas[2][2][4]])

    command_not_arrived = CS('required command sent but did not arrive at target asset', reasons=[asset_blocked, channel_down, high_voltage_off])
    CS('Asset did not receive state change command when needed', is_head_cs=True, reasons=[command_not_sent, command_not_arrived], detection='For signals/switches: using feedback signals', UCAs=[ucas[4][1][1], ucas[6][1][1], ucas[6][1][2], ucas[8][1][1], ucas[8][1][2], ucas[8][1][3]])
    command_arrived_incorrectly = CS('correct command sent, but arrived in incorrect state', detection=log_detection, reasons=[bad_infrastructure, command_modified])
    CS('Asset received incorrect state change command', is_head_cs=True, reasons=[incorrect_command_sent, command_arrived_incorrectly], UCAs=[ucas[4][1][2], ucas[6][1][3], ucas[6][1][4], ucas[6][1][5], ucas[6][1][6], ucas[8][1][4], ucas[8][1][5], ucas[8][1][6], ucas[8][1][7], ucas[8][1][9], ucas[8][1][11]])
    command_arrived_late = CS('required command sent on time but arrived too late', reasons=[slow_infrastructure])
    CS('Asset received state change command too late', is_head_cs=True, reasons=[command_sent_late, command_arrived_late], UCAs=[ucas[4][1][3], ucas[6][1][7], ucas[6][1][8], ucas[8][1][8], ucas[8][1][10], ucas[8][1][12]])

    command_sent_too_long_short = CS('command sent too long/short', reasons=[broken_client, slow_client])
    command_arrived_too_long_short = CS('command sent for correct amount of time, but arrived at asset for too long/short', reasons=[bad_infrastructure, command_length_tampered])
    CS('Level crossing control system receives command to open/close booms for too long/short', is_head_cs=True, reasons=[command_sent_too_long_short, command_arrived_too_long_short], UCAs=[ucas[6][1][11], ucas[6][1][12], ucas[6][1][13], ucas[6][1][14]])
    CS('Hyperledger malfunction', is_head_cs=True, detection='Out of scope', mitigation='Out of scope', test='Out of scope', recovery='Out of scope', responsible=['IBM (and not us! ^o^ )'], UCAs=[ucas[9][1][1], ucas[9][1][2], ucas[9][1][3], ucas[9][1][4], ucas[9][1][5], ucas[10][1][1], ucas[10][1][2], ucas[10][1][3], ucas[10][1][4], ucas[10][1][5], ucas[10][1][6], ucas[11][1][1], ucas[11][1][2], ucas[11][1][3], ucas[11][1][4]])


workbook = xlsxwriter.Workbook('STPA-docs.xlsx')
root_cs_sheet = workbook.add_worksheet('Root Causal Scenarios')
cs_sheet = workbook.add_worksheet('Causal Scenarios')
uca_sheet = workbook.add_worksheet('UCAs')
make_uca_header(uca_sheet, 0, 0)
make_cs_header(cs_sheet, 0, 0)
make_root_cs_header(root_cs_sheet, 0, 0)
do_stpa()
export_ucas(uca_sheet, 2, 0)
export_cses(cs_sheet, 2, 0)
export_root_cses(root_cs_sheet, 0, 0)
workbook.close()
