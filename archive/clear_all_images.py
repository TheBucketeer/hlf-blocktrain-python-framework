import os, sys


os.system('docker images > tmp.out')
with open('tmp.out') as f:
    contents = f.read()
lines = contents.split('\n')
for line in lines:
    line_split = filter(None, line.split(' '))
    name = line_split[0]
    tag  = line_split[1]
    identity = line_split[2]
    if 'self' in tag or 'IMAGE' in tag:
        continue
    if len(sys.argv) > 1:
        print('docker rmi -f ' + name + ':' + tag)
    else:
        os.system('docker rmi -f ' + name + ':' + tag)
