import os, time, sys

default_s = '''from .globals import CC_NAME, CHANNEL_HOST_ORDERERS, CHANNEL_NAME, KAFKA_ENABLED, LEVEL_CROSSING_CLIENT, LOCALHOST, \\
    SC_CONTROLLER_CLIENT, SENSOR_CLIENT, LC_CLI_CLIENT, TEST_CLIENT_NAME, TRAIN_CLI_CLIENT, \\
    pis_available, pis_to_ips
from config_generation.nodes.CA import CA
from config_generation.nodes.CLI import CLI
from config_generation.nodes.Client import Client
from config_generation.nodes.CouchDB import CouchDB
from config_generation.nodes.Kafka import Kafka
from config_generation.nodes.OrdererOrg import OrdererOrg
from config_generation.nodes.PeerOrg import PeerOrg
from config_generation.nodes.Zookeeper import Zookeeper

total_n_pis_in_use = 0

n_orderers_on_scc = {:d}  # 2+
n_orderers_on_pis = {:d}
n_peers_on_scc = {:d}  # 2+
n_peers_on_pis = {:d}
n_extra_clients_on_scc = {:d}
n_extra_clients_on_pis = {:d}
n_kafka_nodes_on_scc = {:d}  # 3+
n_kafka_nodes_on_pis = {:d}
n_zookeeper_nodes_on_scc = {:d}  # 4+
n_zookeeper_nodes_on_pis = {:d}

def get_pi_ips(n_pis):
    global total_n_pis_in_use
    for i in range(total_n_pis_in_use, n_pis + total_n_pis_in_use):
        pi_i = i % len(pis_available)
        total_n_pis_in_use += 1
        yield pis_to_ips[pis_available[pi_i]]


orderer_node_ips = [LOCALHOST] * n_orderers_on_scc
orderer_node_ips += [pi_ip for pi_ip in get_pi_ips(n_orderers_on_pis)]
orderer_orgs = [OrdererOrg(kafka=KAFKA_ENABLED, orderer_ips=orderer_node_ips)]
CHANNEL_HOST_ORDERERS[CHANNEL_NAME] = orderer_orgs[0].orderers[0]

# Default client used for sending invocations
clientlist = [Client(pis_to_ips[TEST_CLIENT_NAME], chaincode=CC_NAME, channel=CHANNEL_NAME, client_type=SC_CONTROLLER_CLIENT)]

clientlist += [Client(LOCALHOST, chaincode=CC_NAME, channel=CHANNEL_NAME, client_type=SC_CONTROLLER_CLIENT) for _ in range(n_extra_clients_on_scc)]
clientlist += [Client(pi_ip, chaincode=CC_NAME, channel=CHANNEL_NAME, client_type=SC_CONTROLLER_CLIENT) for pi_ip in get_pi_ips(n_extra_clients_on_pis)]

peer_node_ips = [LOCALHOST] * n_peers_on_scc
peer_node_ips += [pi_ip for pi_ip in get_pi_ips(n_peers_on_pis)]
peer_orgs = [PeerOrg(admin_ips=peer_node_ips, couchdb_deps=[None] * len(peer_node_ips), clients=clientlist)]
kafkas = [Kafka() for _ in range(n_kafka_nodes_on_scc)]
kafkas += [Kafka(ip=pi_ip) for pi_ip in get_pi_ips(n_kafka_nodes_on_pis)]
zookeepers = [Zookeeper() for _ in range(n_zookeeper_nodes_on_scc)]
zookeepers += [Zookeeper(ip=pi_ip) for pi_ip in get_pi_ips(n_zookeeper_nodes_on_pis)]
cas = [CA(peer_orgs[0])]
clis = [CLI()]
LOCAL_CLI = clis[0]
LOCAL_MAIN_PEER = peer_orgs[0].peers[0]

tmp_peers = sum([peer_org.peers for peer_org in peer_orgs], [])
tmp_orderers = sum([orderer_org.orderers for orderer_org in orderer_orgs], [])
couchdbs = [peer_node.couchdb for peer_node in tmp_peers]
ALL_HOSTS = list(set([service.ip for service in filter(None, sum([couchdbs, kafkas, zookeepers, cas, clis, tmp_peers, tmp_orderers], []))]))
'''

pinames = ['Tr000', 'Tr001', 'Lv000', 'Sw000', 'Sw001', 'Sw002', 'Sw003', 'Sw004', 'Sw005', 'Sn000', 'Sn001', 'Sn002', 'Sn003', 'Sn004', 'Sn005', 'Sn006', 'Sn007', 'Sn008', 'Sn009', 'Sn010', 'Sn011', 'Sn012', 'Sn013', 'Sn014', 'Sn015', 'Sn016', 'Sn017', 'Sg000', 'Sg001', 'Sg002', 'Sg003', 'Sg004', 'Sg005', 'Sg006', 'Sg007', 'Sg008', 'Sg009', 'Sg010', 'Sg011', 'Sg012', 'Sg013', 'Sg014', 'Sg015']
has_compiled = False


def write_layout(s):
    with open('/home/blocktrain/network/config_generation/network_layout.py', 'w') as f:
        f.write(s)


def make_layout(n_orderers_on_scc,
                n_orderers_on_pis,
                n_peers_on_scc,
                n_peers_on_pis,
                n_extra_clients_on_scc,
                n_extra_clients_on_pis,
                n_kafka_nodes_on_scc,
                n_kafka_nodes_on_pis,
                n_zookeeper_nodes_on_scc,
                n_zookeeper_nodes_on_pis):
    global default_s
    return default_s.format(n_orderers_on_scc,
                            n_orderers_on_pis,
                            n_peers_on_scc,
                            n_peers_on_pis,
                            n_extra_clients_on_scc,
                            n_extra_clients_on_pis,
                            n_kafka_nodes_on_scc,
                            n_kafka_nodes_on_pis,
                            n_zookeeper_nodes_on_scc,
                            n_zookeeper_nodes_on_pis)


def do_command_and_read(cmd):
    system(cmd + ' > tmp.out')
    with open('tmp.out') as f:
        text = f.read()
    system('rm tmp.out')
    return text


def system(cmd, ignore_errors=False):
    if os.system(cmd) != 0 and not ignore_errors:
        sys.exit(-1)


def net_down():
    system('python deploy_network.py down')
    system('send_cmd_to_pis "sudo killall java -9"')


def check_date():
    global pinames

    text = do_command_and_read('send_cmd_to_pis "date"')
    lines = text.split('\n')
    send_cmd = 'send_cmd_to_pis'
    send = False
    for i in range(len(lines)):
        if 'Output for' in lines[i]:
            asset_id = lines[i].split('Output for ')[1].split(':')[0]
            if asset_id in pinames and '2019' not in lines[i + 1]:
                print('{:s} has no correct date!'.format(asset_id))
                send_cmd += ' {:s}'.format(asset_id)
                send = True
    if send:
        send_cmd += ' "sudo ifdown wlan0 && sudo ifup wlan0"'
        system(send_cmd)
        time.sleep(20)
        check_pi_internet_connectivity()
    else:
        print('All Pis have the correct date!')


def hard_reboot():
    print("ALARM! rebooting system...")
    system('send_cmd_to_pis "sudo poweroff"')
    time.sleep(20)
    system('echo -n "ab" > /dev/ttyACM0')
    time.sleep(20)
    system('echo -n "AB" > /dev/ttyACM0')
    time.sleep(60)


def check_online():
    global pinames

    text = do_command_and_read('send_cmd_to_pis ls')
    pinames_read = []
    lines = text.split('\n')
    for i, line in enumerate(lines):
        if line.startswith('Output for '):
            if i + 2 >= len(lines) or 'ssh: connect to host' in lines[i + 2]:
                return False
            else:
                pinames_read.append(line.split('Output for ')[1].split(':')[0])
    result = sorted(pinames) == sorted(pinames_read)
    if result:
        print('All Pis are up and running!')
    return result


def check_pi_internet_connectivity():
    global pinames

    pinames_todo = pinames[:]
    # Only try up to 3 times
    for _ in range(3):
        text = do_command_and_read('send_cmd_to_pis {:s} "host www.nu.nl"'.format(' '.join(pinames_todo)))
        send_cmd = 'send_cmds_to_pis'
        offline_pis = []
        for asset_id in pinames_todo:
            if 'Output for {:s}:\nwww.nu.nl is an alias for nunl.aws.sanomaservices.nl.'.format(asset_id) not in text:
                offline_pis.append(asset_id)
                send_cmd += ' {:s} "sudo ifdown wlan0 && sudo ifup wlan0"'.format(asset_id)
                print('{:s} has no internet connectivity!'.format(asset_id))
        if len(offline_pis) > 0:
            system(send_cmd)
            pinames_todo = offline_pis
            time.sleep(20)
        else:
            print('All Pis have internet connectivity!')
            return True
    # Houston, we've got a problem
    return False


def redo_test_if_needed(n_orderers_on_scc,
                        n_orderers_on_pis,
                        n_peers_on_scc,
                        n_peers_on_pis,
                        n_extra_clients_on_scc,
                        n_extra_clients_on_pis,
                        n_kafka_nodes_on_scc,
                        n_kafka_nodes_on_pis,
                        n_zookeeper_nodes_on_scc,
                        n_zookeeper_nodes_on_pis,
                        test_name,
                        test_delay):
    if not check_online() or not check_pi_internet_connectivity():
        # If a pis goes offline, redo the test (check at the start will reboot the system)
        do_test(
            n_orderers_on_scc=n_orderers_on_scc,
            n_orderers_on_pis=n_orderers_on_pis,
            n_peers_on_scc=n_peers_on_scc,
            n_peers_on_pis=n_peers_on_pis,
            n_extra_clients_on_scc=n_extra_clients_on_scc,
            n_extra_clients_on_pis=n_extra_clients_on_pis,
            n_kafka_nodes_on_scc=n_kafka_nodes_on_scc,
            n_kafka_nodes_on_pis=n_kafka_nodes_on_pis,
            n_zookeeper_nodes_on_scc=n_zookeeper_nodes_on_scc,
            n_zookeeper_nodes_on_pis=n_zookeeper_nodes_on_pis,
            test_name=test_name,
            test_delay=test_delay)
    check_date()


def do_test(n_orderers_on_scc=2,
            n_orderers_on_pis=0,
            n_peers_on_scc=2,
            n_peers_on_pis=0,
            n_extra_clients_on_scc=0,
            n_extra_clients_on_pis=0,
            n_kafka_nodes_on_scc=3,
            n_kafka_nodes_on_pis=0,
            n_zookeeper_nodes_on_scc=4,
            n_zookeeper_nodes_on_pis=0,
            test_name='TEST',
            test_delay=30,
            active_clients_on_pis=False):
    global has_compiled
    write_layout(make_layout(n_orderers_on_scc,
                             n_orderers_on_pis,
                             n_peers_on_scc,
                             n_peers_on_pis,
                             n_extra_clients_on_scc,
                             n_extra_clients_on_pis,
                             n_kafka_nodes_on_scc,
                             n_kafka_nodes_on_pis,
                             n_zookeeper_nodes_on_scc,
                             n_zookeeper_nodes_on_pis))
    while not check_online():
        hard_reboot()
    check_pi_internet_connectivity()
    check_date()
    system('python deploy_network.py all', ignore_errors=True)
    redo_test_if_needed(n_orderers_on_scc,
                        n_orderers_on_pis,
                        n_peers_on_scc,
                        n_peers_on_pis,
                        n_extra_clients_on_scc,
                        n_extra_clients_on_pis,
                        n_kafka_nodes_on_scc,
                        n_kafka_nodes_on_pis,
                        n_zookeeper_nodes_on_scc,
                        n_zookeeper_nodes_on_pis, test_name, test_delay)
    if not has_compiled:
        system('python deploy_clients.py transmit compile')
        has_compiled = True
    else:
        system('python deploy_clients.py transfer_settings')
    redo_test_if_needed(n_orderers_on_scc,
                        n_orderers_on_pis,
                        n_peers_on_scc,
                        n_peers_on_pis,
                        n_extra_clients_on_scc,
                        n_extra_clients_on_pis,
                        n_kafka_nodes_on_scc,
                        n_kafka_nodes_on_pis,
                        n_zookeeper_nodes_on_scc,
                        n_zookeeper_nodes_on_pis, test_name, test_delay)
    if active_clients_on_pis:
        system('python deploy_clients.py clients_up_test {:s} "{:d}"'.format(test_name, test_delay))
    else:
        system('python deploy_clients.py clients_up')
        system('ssh Sw002 "cd /home/pi/golang/src/github.com/hyperledger/fabric-samples/my-network && sudo chmod +x ./run_Sm000.sh && ./run_Sm000.sh {:s} {:d}"'.format(test_name, test_delay))
    redo_test_if_needed(n_orderers_on_scc,
                        n_orderers_on_pis,
                        n_peers_on_scc,
                        n_peers_on_pis,
                        n_extra_clients_on_scc,
                        n_extra_clients_on_pis,
                        n_kafka_nodes_on_scc,
                        n_kafka_nodes_on_pis,
                        n_zookeeper_nodes_on_scc,
                        n_zookeeper_nodes_on_pis, test_name, test_delay)
    time.sleep(300)
    redo_test_if_needed(n_orderers_on_scc,
                        n_orderers_on_pis,
                        n_peers_on_scc,
                        n_peers_on_pis,
                        n_extra_clients_on_scc,
                        n_extra_clients_on_pis,
                        n_kafka_nodes_on_scc,
                        n_kafka_nodes_on_pis,
                        n_zookeeper_nodes_on_scc,
                        n_zookeeper_nodes_on_pis, test_name, test_delay)
    net_down()


if __name__ == '__main__':
    #do_test(n_peers_on_pis=20, test_name='20extrapeers')  # Done
    #do_test(n_peers_on_pis=30, test_name='30extrapeers2')  # Done
    #do_test(n_peers_on_pis=40, test_name='40extrapeers')  # Done
    #do_test(n_extra_clients_on_pis=30, test_name='30extraclients')  # Done
    #do_test(n_extra_clients_on_pis=40, test_name='40extraclients')  # Done
    #do_test(n_orderers_on_pis=1, test_name='1ordereronpis')  # Done
    #do_test(n_orderers_on_pis=2, test_name='2orderersonpis')  # Done
    #do_test(n_orderers_on_pis=4, test_name='4orderersonpis')  # Done
    #do_test(n_orderers_on_pis=8, test_name='8orderersonpis')  # Done
    #do_test(test_delay=0, test_name='delay0basic')  # Done
    #do_test(test_delay=1, test_name='delay1basic')  # Done
    #do_test(test_delay=5, test_name='delay5basic')  # Done
    #do_test(test_delay=10, test_name='delay10basic')  # Done
    #do_test(n_orderers_on_pis=2, n_peers_on_pis=5, test_delay=0, test_name='delay0heavy')  # Done
    #do_test(n_orderers_on_pis=2, n_peers_on_pis=5, test_delay=1, test_name='delay1heavy')  # Done
    #do_test(n_orderers_on_pis=2, n_peers_on_pis=5, test_delay=5, test_name='delay5heavy')  # Done
    #do_test(n_orderers_on_pis=2, n_peers_on_pis=5, test_delay=10, test_name='delay10heavy')  # Done
    #do_test(n_orderers_on_scc=4, test_name='2extraorderersonscc')  # +2 Done
    #do_test(n_orderers_on_scc=6, test_name='4extraorderersonscc')  # +4 Done
    #do_test(n_orderers_on_pis=2, test_name='2extraorderersonpis')  # Done
    #do_test(n_orderers_on_pis=4, test_name='4extraorderersonpis')
    #do_test(n_peers_on_pis=1, test_name='1extrapeeronpi')  # +1
    #do_test(n_peers_on_pis=5, test_name='5extrapeersonpis')
    #do_test(n_peers_on_scc=3, test_name='1extrapeeronscc')  # +1
    #do_test(n_peers_on_scc=7, test_name='5extrapeersonscc')  # +5
    #do_test(n_extra_clients_on_scc=2, test_name='2extraclientsonscc') # Done
    #do_test(n_extra_clients_on_scc=6, test_name='6extraclientsonscc') # Done
    #do_test(n_extra_clients_on_pis=2, test_name='2extraclientsonpis')  # Done
    #do_test(n_extra_clients_on_pis=6, test_name='6extraclientsonpis')  # Done
    #do_test(n_extra_clients_on_pis=1, active_clients_on_pis=True, test_name='1extraactiveclientonpi') # DONE
    #do_test(n_extra_clients_on_pis=10, active_clients_on_pis=True, test_name='10extraactiveclientsonpis') # DONE
    #do_test(n_extra_clients_on_pis=20, active_clients_on_pis=True, test_name='20extraactiveclientsonpis') # DONE
    #do_test(n_extra_clients_on_pis=30, active_clients_on_pis=True, test_name='30extraactiveclientsonpis') # DONE
    #do_test(n_extra_clients_on_pis=40, active_clients_on_pis=True, test_name='40extraactiveclientsonpis') # DONE
    #do_test(n_kafka_nodes_on_pis=1, test_name='1kafkanodeonpis') # DONE
    #do_test(n_kafka_nodes_on_pis=2, test_name='2kafkanodesonpis') # DONE
    #do_test(n_kafka_nodes_on_pis=4, test_name='4kafkanodesonpis') # DONE
    #do_test(n_kafka_nodes_on_pis=8, test_name='8kafkanodesonpis') # DONE
    #do_test(n_kafka_nodes_on_scc=5, test_name='2extrakafkanodesonscc')  # +2 # DONE
    #do_test(n_kafka_nodes_on_scc=7, test_name='4extrakafkanodesonscc')  # +4 # DONE
    #do_test(n_zookeeper_nodes_on_pis=1, test_name='1zookeepernodeonpis') # DONE
    #do_test(n_zookeeper_nodes_on_pis=2, test_name='2zookeepernodesonpis') # DONE
    #do_test(n_zookeeper_nodes_on_pis=4, test_name='4zookeepernodesonpis') # DONE
    #do_test(n_zookeeper_nodes_on_pis=8, test_name='8zookeepernodesonpis') # DONE
    #do_test(n_zookeeper_nodes_on_scc=6, test_name='2extrazookeepernodesonscc')  # +2 # DONE
    #do_test(n_zookeeper_nodes_on_scc=8, test_name='4extrazookeepernodesonscc')  # +4 # DONE
    print(make_layout(2, 0, 2, 0, 0, 0, 3, 0, 4, 0))
