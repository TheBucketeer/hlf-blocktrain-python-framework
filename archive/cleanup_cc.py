#!/usr/bin/python
# -*- coding: utf-8 -*-

import os


def helper(cmd):
    print(cmd)
    os.system(cmd + ' > /dev/null 2>&1')

helper('docker kill $(docker ps -a | grep dev | cut -d \' \' -f 1) > /dev/null 2>&1')
helper('docker rmi --force $(docker images | grep dev | tr -s \' \' | cut -d \' \' -f 3) > /dev/null 2>&1')
