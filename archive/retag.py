import os, sys

'''
docker pull ptunstad/fabric-baseos:arm64-0.4.15 &&
docker pull ptunstad/fabric-basejvm:arm64-0.4.15 &&
docker pull ptunstad/fabric-baseimage:arm64-0.4.15 &&
docker pull ptunstad/fabric-ccenv:arm64-1.4.1 &&
docker pull ptunstad/fabric-peer:arm64-1.4.1 &&
docker pull ptunstad/fabric-orderer:arm64-1.4.1 &&
docker pull ptunstad/fabric-zookeeper:arm64-1.4.1 &&
docker pull ptunstad/fabric-kafka:arm64-1.4.1 &&
docker pull ptunstad/fabric-couchdb:arm64-1.4.1 &&
docker pull ptunstad/fabric-tools:arm64-1.4.1
'''

os.system('docker images > tmp.out')
with open('tmp.out') as f:
    contents = f.read()
lines = contents.split('\n')
toremove = []
for line in lines:
    try:
        name = filter(None, line.split(' '))[0]
        tag  = filter(None, line.split(' '))[1]
        identity = filter(None, line.split(' '))[2]
        for maker in ['ptunstad', 'apelser']:
            if maker in name:
                new_tag = 'hyperledger' + name.split(maker)[1] + ':' + tag
                cmd = 'docker tag ' + identity + ' ' + new_tag
                if len(sys.argv) > 1:
                    print(cmd)
                else:
                    os.system(cmd)
                new_tag = 'hyperledger' + name.split(maker)[1].split(':')[0] + ':latest'
                cmd = 'docker tag ' + identity + ' ' + new_tag
                if len(sys.argv) > 1:
                    print(cmd)
                else:
                    os.system(cmd)
                cmd = 'docker rmi ' + name + ':' + tag
                if len(sys.argv) > 1:
                    print(cmd)
                else:
                    os.system(cmd)
    except Exception:
        pass

# toremove = list(set(toremove))
# for identity in toremove:
#     if len(sys.argv) > 1:
#         print('docker rmi ' + identity + ' --force')
#     else:
#         os.system('docker rmi ' + identity + ' --force')