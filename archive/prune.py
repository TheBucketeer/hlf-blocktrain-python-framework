from collections import defaultdict
import os, sys

'''
docker pull ptunstad/fabric-baseos:arm64-0.4.15 &&
docker pull ptunstad/fabric-basejvm:arm64-0.4.15 &&
docker pull ptunstad/fabric-baseimage:arm64-0.4.15 &&
docker pull ptunstad/fabric-ccenv:arm64-1.4.1 &&
docker pull ptunstad/fabric-peer:arm64-1.4.1 &&
docker pull ptunstad/fabric-orderer:arm64-1.4.1 &&
docker pull ptunstad/fabric-zookeeper:arm64-1.4.1 && (WRONG version)
docker pull ptunstad/fabric-kafka:arm64-1.4.1 && (WRONG version)
docker pull ptunstad/fabric-couchdb:arm64-1.4.1 && (WRONG version)
docker pull ptunstad/fabric-tools:arm64-1.4.1
'''

keep = [
    '12bf060ff89f',
    '519b49fd66ef',
    '4fa95365ae99',
    '695b0eeba54f',
    'f99bba7a0065',
    'f4e77a4a98d2',
    '766b177ec446',
    'c0f96a0d93ec',
    '9272b8494273',
    'a75ee5535058',
    'd43949a125e8'
]
os.system('docker images > tmp.out')
with open('tmp.out') as f:
    contents = f.read()
lines = contents.split('\n')
toremove = defaultdict(list)
for line in lines:
    try:
        line_split = filter(None, line.split(' '))
        name = line_split[0]
        tag  = line_split[1]
        identity = line_split[2]
        if 'ubuntu' not in name and 'busybox' not in name and identity not in keep:
            assert len(identity) == len('13241934c8c2')
            toremove[identity].append(name + ':' + tag)
    except Exception:
        pass

toremove = list(toremove.items())
while len(toremove) > 0:
    item = toremove.pop()
    identity = item[0]
    names = item[1]
    if len(sys.argv) > 1:
        print('docker rmi ' + identity + ' --force (' + ','.join(names) + ')')
    else:
        ret = os.system('docker rmi ' + identity + ' --force')
        if ret != 0:
            toremove = [item] + toremove