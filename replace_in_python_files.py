import os
import sys


def replace_in_file(filename, frm, to):
    with open(filename) as f:
        text = f.read()
    text = text.replace(frm, to)
    with open(filename, 'w') as f:
        f.write(text)


if __name__ == '__main__':
    _, frm, to = sys.argv
    for root, dirs, files in os.walk(os.getcwd()):
        for file in files:
            if file.endswith('.py'):
                file = os.path.join(root, file)
                replace_in_file(file, frm, to)
