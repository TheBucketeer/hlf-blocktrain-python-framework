import os
import random
import sys
import time

from config_generation.config import get_remote_network_hosts, get_remote_client_hosts, identity_register, \
    LOCAL_DESKTOP_WDIR


def sort_services(service_list):
    """Sorts Docker Services by their hostnames.

    Used to obtain predictable configuration files.

    Args:
        service_list (:obj:`list` of :obj:`DockerService`): A list of DockerService objects to sort.

    Returns:
        :obj:`list` of :obj:`DockerService`: The sorted list.
    """
    return sorted(service_list, key=lambda service: service.hostname)


def sleep(delay):
    """Prints for how many seconds the program will sleep, then sleeps."""
    print('Sleeping {:d} seconds...'.format(delay))
    time.sleep(delay)


def debug_system(cmd, ignore_error=False, silence=False):
    """Prints and executes a command, and optionally stops the program if it fails.

    Args:
        cmd (str): The command to be executed.
        ignore_error (:obj:`bool`, optional): When set to True, the program will not halt on errors.
        silence (:obj:`bool`, optional: Whether or not to surpress the output.

    Returns:
        int: The returned status code.
    """
    # if silence:
    #     cmd += ' > /dev/null 2&>1'
    print(cmd)
    ret = os.system(cmd)
    if ret != 0 and not ignore_error:
        print('Command FAILED with status {:d} - aborting!'.format(ret))
        sys.exit(-1)
    print('Returned status {:d}'.format(ret))
    return ret


def format_for_ssh(cmd):
    """Adds quotes to a command and escapes special characters for the purpose of passing it to SSH.

    Args:
        cmd (str): The command to be executed.

    Returns:
        str: The formatted command.
    """
    return '"{:s}"'.format(cmd.replace("\\", "\\\\").replace('"', '\\"').replace("$", "\\$"))


def make_ssh_cmd(hostname, cmd):
    """Turns commands into SSH commands.

    Given an SSH hostname and a command, returns an SSH command that will execute the command correctly on the remote
    host.

    Args:
        hostname (str): The SSH hostname on which the command is to be executed. These must be configured in
            ~/.ssh/config.
        cmd (str): The command to be executed.

    Returns:
        str: The SSH command.
    """
    return 'ssh {:s} {:s}'.format(hostname, format_for_ssh(cmd))


def make_chmod_and_exec_cmd(file_location, filename):
    """Creates a command that makes a file executable and then executes it.

    Args:
        file_location (str): The location of the file.
        filename (str): The name of the file to be executed.

    Returns:
        str: The resulting command.
    """
    return 'cd {0} && sudo chmod +x ./{1} && ./{1}'.format(file_location, filename)


def send_parallel_ssh_cmds(host_cmd_pairs, ignore_error=False, silence=False):
    """Sends commands to a set of hosts in parallel.

    Given a list of tuples (host, cmd), calls send_cmds_to_pis to send multiple commands to hosts in parallel.

    Args:
        host_cmd_pairs (:obj:`list` of :obj:`tuple`): A list of tuples containing a host object and a
            command to be executed.
        ignore_error (:obj:`bool`, optional): The ignore_error parameter for debug_system.
        silence (:obj:`bool`, optional): The silence parameter for debug_system.
    Returns:
        int: The exit status code of the command.
    """
    if len(host_cmd_pairs) > 0:
        send_cmd_parts = []
        for host, cmd in host_cmd_pairs:
            send_cmd_parts.append('{:s} {:s}'.format(host.ssh_name, format_for_ssh(cmd)))
        return debug_system('send_cmds_to_pis ' + ' '.join(send_cmd_parts), ignore_error=ignore_error, silence=silence)
    return 0


def broadcast_cmd_to_hosts(cmd, hosts, ignore_error=False, silence=False):
    """Wrapper for send_parallel_ssh_cmds, sends a cmd to a list of hosts."""
    send_parallel_ssh_cmds([(host, cmd) for host in hosts], ignore_error=ignore_error, silence=silence)


def broadcast_cmd_to_network(cmd, ignore_error=False, silence=False):
    """Wrapper for broadcast_cmd_to_hosts, sends a cmd to all Pis that are part of the network."""
    broadcast_cmd_to_hosts(cmd, get_remote_network_hosts(), ignore_error=ignore_error, silence=silence)


def broadcast_cmd_to_clients(cmd, ignore_error=False, silence=False):
    """Wrapper for broadcast_cmd_to_hosts, sends a cmd to all Pis that host clients."""
    broadcast_cmd_to_hosts(cmd, get_remote_client_hosts(), ignore_error=ignore_error, silence=silence)


def broadcast_cmd_to_all(cmd, ignore_error=False, silence=False):
    """Sends a command to all known Pis, including ones not in the network

    Args:
        cmd (str): The command to be executed.
        ignore_error (:obj:`bool`, optional): The ignore_error parameter for debug_system.
        silence (:obj:`bool`, optional): The silence parameter for debug_system.

    Returns:
        int: The exit status code of the command.
    """
    return debug_system('send_cmd_to_pis ' + format_for_ssh(cmd), ignore_error=ignore_error, silence=silence)


def send_data_to_hosts(cmd, params, ignore_error=False, silence=False, from_local_wdir=False):
    """Sends files or directories to Pis, in parallel. (Multiple Data. Multiple Pis)

    Given a list of tuples (Host, local_source, remote_destination), calls cmd to send multiple files or
    directories to Pis in parallel.

    Args:
        cmd (str): The command to be used for sending the files/dirs (send_files_to_hosts or send_files_to_dirs).
        params (:obj:`list` of :obj:`tuple`): A list of tuples containing a host, the local
            file/directory to be sent and the destination on the remote host to send it to. Follows the same semantics
             as scp.
        ignore_error (:obj:`bool`, optional): The ignore_error parameter for debug_system.
        silence (:obj:`bool`, optional): The silence parameter for debug_system.
        from_local_wdir (:obj:`bool`, optional): Flag to send the files from the local working directory. 
    Returns:
        int: The exit status code of the command.
    """
    if len(params) > 0:
        arg_parts = []
        for host, local_src, remote_dest in params:
            if from_local_wdir:
                local_src = '{:s}/{:s}'.format(LOCAL_DESKTOP_WDIR, local_src)
            arg_parts.append('{:s} {:s} {:s}'.format(host.ssh_name, local_src, remote_dest))
        return debug_system(cmd + ' ' + ' '.join(arg_parts), ignore_error=ignore_error, silence=silence)
    return 0


def send_files_to_hosts(params, ignore_error=False, silence=False, from_local_wdir=False):
    """Wrapper for send_data_to_hosts with send_files_to_hosts as cmd."""
    return send_data_to_hosts('send_files_to_pis', params, ignore_error=ignore_error, silence=silence,
                              from_local_wdir=from_local_wdir)


def send_file_to_hosts(hosts, local_src, remote_dest, ignore_error=False, silence=False, from_local_wdir=False):
    """Wrapper for send_files_to_hosts, sends a single file to multiple hostss."""
    return send_files_to_hosts([(host, local_src, remote_dest) for host in hosts], ignore_error=ignore_error,
                               silence=silence, from_local_wdir=from_local_wdir)


def send_file_to_clients(local_src, remote_dest, ignore_error=False, silence=False, from_local_wdir=False):
    """Wrapper for send_file_to_hosts, sends file to all Pis hosting clients."""
    return send_file_to_hosts(get_remote_client_hosts(), local_src, remote_dest, ignore_error=ignore_error,
                              silence=silence, from_local_wdir=from_local_wdir)


def send_file_to_network(local_src, remote_dest, ignore_error=False, silence=False, from_local_wdir=False):
    """Wrapper for send_file_to_hosts, sends file to all Pis in the network."""
    return send_file_to_hosts(get_remote_network_hosts(), local_src, remote_dest, ignore_error=ignore_error,
                              silence=silence, from_local_wdir=from_local_wdir)


def send_dirs_to_hosts(params, ignore_error=False, silence=False, from_local_wdir=False):
    """Wrapper for send_data_to_hosts with send_dirs_to_hosts as cmd."""
    return send_data_to_hosts('send_dirs_to_pis', params, ignore_error=ignore_error, silence=silence,
                              from_local_wdir=from_local_wdir)


def send_dir_to_hosts(hosts, local_src, remote_dest, ignore_error=False, silence=False, from_local_wdir=False):
    """Wrapper for send_dirs_to_hosts, sends a single directory to multiple hosts."""
    return send_dirs_to_hosts([(host, local_src, remote_dest) for host in hosts], ignore_error=ignore_error,
                              silence=silence, from_local_wdir=from_local_wdir)


def send_dir_to_clients(local_src, remote_dest, ignore_error=False, silence=False, from_local_wdir=False):
    """Wrapper for send_dir_to_hosts, sends directory to all Pis that host clients."""
    return send_dir_to_hosts(get_remote_client_hosts(), local_src, remote_dest, ignore_error=ignore_error,
                             silence=silence, from_local_wdir=from_local_wdir)


def send_dir_to_network(local_src, remote_dest, ignore_error=False, silence=False, from_local_wdir=False):
    """Wrapper for send_dir_to_hosts, sends directory to all Pis in the network."""
    return send_dir_to_hosts(get_remote_network_hosts(), local_src, remote_dest, ignore_error=ignore_error,
                             silence=silence, from_local_wdir=from_local_wdir)


def wide_print(s):
    """Prints stuff visibly amongst the tsunami of debugging spam generated by Hyperledger.

    Args:
        s (str): String to print.
    """
    print('=' * 25 + ' ' + s + ' ' + 25 * '=')


def indent(n_indents, s):
    """Adds indentation to a string.

    Args:
        n_indents (int): The number of indentation spaces to add.
        s (str): The string to indent.

    Returns:
        str: The indented string.
    """
    lines = s.split('\n')
    output_lines = []
    for line in lines:
        # This happens if a newline was at the start/end of s, or if multiple consecutive newlines were used
        # In any case, don't indent empty lines
        if line == '':
            output_lines.append(line)
        else:
            output_lines.append(' ' * n_indents + line)
    return '\n'.join(output_lines)


def print_useful_msg():
    """Prints an inspiring message."""
    messages = [
        'Guess I\'ll just sit here watch grass grow.',
        'My batteries are low and it\'s getting dark.',
        'You\'re in lizard squad, Harry.',
        'Mistakes are always forgivable, if one has the courage to admit them.',
        'Quidquid Latine dictum sit altum videtur.',
        'How hard can it be?',
        'Change gear, change gear, check mirror, murder a prostitute, change gear, change gear, murder. That\'s a lot '
        'of effort in a day.',
        'You\'re a dum-dum!',
        'Best not to include this in your resume...',
        'Checkmate, atheists.',
        'apt moo'
    ]
    print(messages[random.randint(1, len(messages) - 1)] + '\n(No command given)')


def register_identity(identity, identifier=None):
    """Returns a unique number for an identity.

    Derives and returns a unique number for an identity (a client, organisation or Docker service), based on how many
    times this function has been called for a given identifier (or the identity's class name if not provided).

    Args:
        identity (:obj:): the object to return an identifier for; it's class name is used as identifier if the
            identifier-argument is not provided,
        identifier (:obj:`str`, optional): the identifier that identifies this object; to be provided if the identity's
            class name should not be used as identifier,

    Returns:
        int: a unique number that shall not be returned again for any other calls to this function with the same
            identifier (or of the same class, if the identifier was not provided).
    """
    if identifier is None:
        identifier = identity.__class__.__name__
    result = identity_register[identifier]
    identity_register[identifier] += 1
    return result
