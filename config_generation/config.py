import datetime
from collections import defaultdict

COMPOSE_PROJECT_NAME    = 'net'
DOCKER_NETWORK          = 'blocktraindockernet'  # Name identifying the network for Docker
CONN_PROFILE_NETWORK    = 'blocktrain-network'
CONN_PROFILE_VERSION    = '1.0'
CC_NAME                 = 'blocktraincc'
CC_VERSION              = '1.02'
CC_LANGUAGE             = 'java'
CONSORTIUM_NAME         = 'BlockTrain'
# Names should ONLY consist of letters to prevent bugs caused by weird characters
CHANNEL_NAME            = 'testchannel'
ORDERER_SYSCHANNEL_NAME = 'testordsyschannel'
ORDERER_PROFILE         = 'OrdererProfile'
ORDERER_PROFILE_KAFKA   = 'OrdererProfileKafka'
PEER_PROFILE            = 'PeerProfile'

# (Partial) filenames
CLIENT_COMPILE_FILE     = 'compile_client.sh'
CLIENT_CONNECTION_FILE  = 'connection.json'
CLIENT_EVENTLOG_FILE    = 'eventlog.txt'
CLIENT_LOG_FILE         = 'log.txt'
CLIENT_LOG4J_FILE       = 'log4j.log'
CLIENT_LOG4J_PROPS_FILE = 'log4j.properties'
CLIENT_RUN_FILE         = 'run.sh'
CLIENT_SETTINGS_FILE    = 'client_settings.json'
DOCKER_COMPOSE_FILE     = 'docker-compose.yaml'
CRYPTO_CONFIG_FILE      = 'crypto-config.yaml'
CONFIGTX_FILE           = 'configtx.yaml'
DOTENV_FILE             = '.env'
UPDATE_AFF_FILE         = 'update_affiliations.sh'

# Directory locations that are not within docker containers, thus 'local'
# *_REL means the location is relative and can either be on a Pi or on the SCM
LOCAL_CA_SCRIPTS_REL    = 'ca-scripts'
LOCAL_CCROOT_REL        = 'chaincode'
LOCAL_CLIENTROOT_REL    = 'piclient'
LOCAL_CLIENTDIR_REL     = 'blockchainclients'
LOCAL_CONFIGTXDIR_REL   = 'config'
LOCAL_CRYPTODIR_REL     = 'crypto-config'
LOCAL_KEYSTORE_REL      = '.hfc-key-store'
LOCAL_FS_DIR            = '/home/blocktrain/fabric-samples'

# TODO: Edit the next two variables
# Working directory on this machine
LOCAL_DESKTOP_WDIR      = '/home/blocktrain/network'
# Working directory on the Raspberry Pis
LOCAL_PI_WDIR           = '/home/pi/golang/src/github.com/hyperledger/fabric-samples/my-network'

# Directory mounting points within the various docker containers
DOCKER_GOPATH           = '/opt/gopath'
ORDERER_CRYPTODIR       = '/var/hyperledger/crypto'
ORDERER_PRODDIR         = '/var/hyperledger/production/orderer'
ORDERER_WDIR            = '{:s}/src/github.com/hyperledger/fabric'.format(DOCKER_GOPATH)
PEER_CCROOT             = '{:s}/src/github.com/chaincode'.format(DOCKER_GOPATH)
if CC_LANGUAGE == 'golang':
    PEER_CCINSTALLROOT  = PEER_CCROOT.split('{:s}/src/'.format(DOCKER_GOPATH))[1]
else:
    PEER_CCINSTALLROOT  = PEER_CCROOT
PEER_CCPATH             = ''
PEER_WDIR               = '{:s}/peer'.format(ORDERER_WDIR)
PEER_CRYPTODIR          = '{:s}/crypto'.format(PEER_WDIR)
PEER_PRODDIR            = '/var/hyperledger/production'
DOCKER_CONFIGTXDIR      = '{:s}/configtx'.format(PEER_WDIR)
FABRIC_CA_HOME          = '{:s}/fabric-ca-server'.format(PEER_WDIR)
FABRIC_CA_CONFIG        = '{:s}/fabric-ca-server-config'.format(PEER_WDIR)
FABRIC_CA_SCRIPTS       = '{:s}/scripts'.format(FABRIC_CA_HOME)
LOGLEVEL                = 'DEBUG'  # 'INFO'  # 'DEBUG'
LOCALHOST_IP            = '10.0.0.1'

# These sleep delays are long, because Pis are slow
SLEEP_DELAYS            = {'after_join': 30,
                           'after_affiliation_update': 30,
                           'after_network_up': 180,
                           'after_channel_create': 60,
                           'after_fetch_config': 30,
                           'after_install_cc': 30,
                           'after_instantiate_cc': 30}
# In seconds, should be high to deal with potential delays due to the slow Pis
CHANNEL_CREATE_TIMEOUT  = 60

# Client-related variables
LC_CLIENT_NAME          = 'level_crossing'
SENSOR_CLIENT_NAME      = 'sensor'
TRAIN_CLIENT_NAME       = 'train'
SCC_CLIENT_NAME         = 'semi-central_controller'
SIGNAL_CLIENT_NAME      = 'signal'
SWITCH_CLIENT_NAME      = 'switch'
CLIENT_INFO             = {
    LC_CLIENT_NAME: {'main_class': 'client.LevelCrossingClient.LevelCrossingClient', 'aid_prefix': 'Lv'},
    SENSOR_CLIENT_NAME: {'main_class': 'client.SensorReading.SensorClient', 'aid_prefix': 'Sn'},
    TRAIN_CLIENT_NAME: {'main_class': 'client.TrainClient.TrainClient', 'aid_prefix': 'Tr'},
    SCC_CLIENT_NAME: {'main_class': 'client.SemiCentralController.SemiCentralClient', 'aid_prefix': 'Sm'},
    SIGNAL_CLIENT_NAME: {'main_class': 'client.SignalClient.SignalClient', 'aid_prefix': 'Sg'},
    SWITCH_CLIENT_NAME: {'main_class': 'client.SwitchClient.SwitchClient', 'aid_prefix': 'Sw'}
}

TLS_ENABLED             = True
if TLS_ENABLED:
    GRPC_PROTOC = 'grpcs'
else:
    GRPC_PROTOC = 'grpc'
# Set to False to use the 'solo' orderer configuration
KAFKA_ENABLED           = True

# Do not touch. I repeat: DO NOT TOUCH!
COUCHDB_BASE_PORT       = 5984
ORDERER_BASE_PORT       = 7050
PEER_BASE_PORT0         = 7051
PEER_BASE_PORT1         = 7053
PEER_BASE_PORT2         = 7052
CA_BASE_PORT            = 7054
KAFKA_EXTERNAL_PORT     = 9092
KAFKA_REPLICATE_PORT    = 9093
ZOOKEEPER_BASE_PORT     = 2181
ZOOKEEPER_CONNECT_PORT  = 2888
ZOOKEEPER_ELECT_PORT    = 3888

TIME_MS_STRING          = datetime.datetime.utcnow().microsecond
# The minimal difference between the used ports of two instances of the same Docker service running on 1 IP
PORT_INTERVAL           = 50

# Mapping from SSH identifiers for the PIs to IP addresses
# Make sure this mapping is also encoded in your SSH config! (~/.ssh/config)
# Also, make sure you use SSH keys to avoid having to type a password for every SSH connection
SSH_NAMES_TO_IPS = {
    'pi0': '10.0.0.56',
    'pi1': '10.0.0.57',
    'pi2': '10.0.0.58',
    'pi3': '10.0.0.59',
    'pi4': '10.0.0.60',
    'Tr000': '10.0.0.61',
    'Tr001': '10.0.0.62',
    'Lv000': '10.0.0.63',
    'Sw000': '10.0.0.64',
    'Sw001': '10.0.0.65',
    'Sw002': '10.0.0.66',
    'Sw003': '10.0.0.67',
    'Sw004': '10.0.0.68',
    'Sw005': '10.0.0.69',
    'Sn000': '10.0.0.70',
    'Sn001': '10.0.0.71',
    'Sn002': '10.0.0.72',
    'Sn003': '10.0.0.73',
    'Sn004': '10.0.0.74',
    'Sn005': '10.0.0.75',
    'Sn006': '10.0.0.76',
    'Sn007': '10.0.0.77',
    'Sn008': '10.0.0.78',
    'Sn009': '10.0.0.79',
    'Sn010': '10.0.0.80',
    'Sn011': '10.0.0.81',
    'Sn012': '10.0.0.82',
    'Sn013': '10.0.0.83',
    'Sn014': '10.0.0.84',
    'Sn015': '10.0.0.85',
    'Sn016': '10.0.0.86',
    'Sn017': '10.0.0.87',
    'Sg000': '10.0.0.88',
    'Sg001': '10.0.0.89',
    'Sg002': '10.0.0.90',
    'Sg003': '10.0.0.91',
    'Sg004': '10.0.0.92',
    'Sg005': '10.0.0.93',
    'Sg006': '10.0.0.94',
    'Sg007': '10.0.0.95',
    'Sg008': '10.0.0.96',
    'Sg009': '10.0.0.97',
    'Sg010': '10.0.0.98',
    'Sg011': '10.0.0.99',
    'Sg012': '10.0.0.100',
    'Sg013': '10.0.0.101',
    'Sg014': '10.0.0.102',
    'Sg015': '10.0.0.103',
    'pi5': '10.0.0.104',  # The last 2 are spare Pis, hence why they're at the bottom
    'pi6': '10.0.0.105'
}

# Reverse mapping
IPS_TO_SSH_NAMES = {val: key for key, val in SSH_NAMES_TO_IPS.items()}

# Format: {<service_name>: amount registered}
# Used for providing unique IDs to services and organisations
identity_register = defaultdict(int)

# To be filled in in network_layout.py:
all_hosts = []  # All host objects in the network
channel_host_orderers = {}  # Mapping from channel names to their main orderers
cas = []
clients = []
clis = []
couchdbs = []
IPS_TO_HOSTS = {}  # Mapping from IP to host objects for convenience
kafkas = []
orderer_nodes = []
orderer_orgs = []
peer_nodes = []
peer_orgs = []
zookeepers = []


def sort_hosts(hosts):
    """Sorts a list of host objects.

    Hosts are sorted alphabetically by their ssh names, except localhost which will always be at the start of the list.

    Args:
        hosts (:obj:`list` of :obj:`Host`): A list of Host objects.

    Returns:
        :obj:`list` of :obj:`Host`: The sorted list.
    """
    hosts = sorted(hosts, key=lambda host: host.ssh_name)
    localhost = get_localhost()
    if localhost in hosts:
        hosts.remove(localhost)
        hosts = [localhost] + hosts
    return hosts


def get_hosts_of_services(*args):
    """Returns all unique hosts for a list of services.

    Args:
        *args: Lists with Docker services in it

    Examples:
        get_hosts_of_services(peer_nodes, orderer_nodes, kafkas)

    Returns:
        :obj:`list` of :obj:`str`: A list of all unique hosts belonging to the total set of services passed as
            parameters
    """
    services_flattened = filter(None, sum(args, []))
    service_hosts = [service.host for service in services_flattened]
    # Remove duplicates
    return sort_hosts(list(set(service_hosts)))


def get_all_hosts():
    """Returns all hosts that have a role in this network, as client or network node.

    Returns:
        :obj:`list` of :obj:`Host`: The list of hosts.
    """
    return get_hosts_of_services(cas, clis, clients, couchdbs, kafkas, orderer_nodes, peer_nodes, zookeepers)


def get_client_hosts():
    """Returns all hosts that host at least 1 client in this network.

    Returns:
        :obj:`list` of :obj:`Host`: The list of hosts.
    """
    return get_hosts_of_services(clients)


def get_network_hosts():
    """Returns all hosts that host at least 1 network node in this network.

    Returns:
        :obj:`list` of :obj:`Host`: The list of hosts.
    """
    return get_hosts_of_services(cas, clis, couchdbs, kafkas, orderer_nodes, peer_nodes, zookeepers)


def get_remote_hosts():
    """Returns all hosts except localhost that have a role in this network, as client or network node.

    Returns:
        :obj:`list` of :obj:`Host`: The list of hosts.
    """
    localhost = get_localhost()
    return [host for host in get_all_hosts() if host != localhost]


def get_remote_client_hosts():
    """Returns all hosts except localhost that host at least 1 client node in this network.

    Returns:
        :obj:`list` of :obj:`Host`: The list of hosts.
    """
    localhost = get_localhost()
    return [host for host in get_client_hosts() if host != localhost]


def get_remote_network_hosts():
    """Returns all hosts except localhost that host at least 1 network node in this network.

    Returns:
        :obj:`list` of :obj:`Host`: The list of hosts.
    """
    localhost = get_localhost()
    return [host for host in get_network_hosts() if host != localhost]


def get_localhost():
    """Return the localhost, ie. the Host object that represents this computer.

    Raises:
        Exception: if the number of host objects with the localhost IP is not equal to 1.

    Returns:
        Host: The localhost.
    """
    result = None
    for host in all_hosts:
        if host.ip == LOCALHOST_IP:
            if result is not None:
                raise Exception("More than localhost object found!")
            result = host
    if result is not None:
        return result
    raise Exception("No localhost found!")


def get_main_peer():
    """Returns the first PeerNode object in peer_nodes.

    It is important to have a peer for doing important stuff.

    Returns:
        PeerNode: the `main peer`.
    """
    if len(peer_nodes) == 0:
        raise Exception("No local peer found!")
    return peer_nodes[0]


def get_cli():
    """Returns the first CLI object in clis.

    This CLI is used for a number of important tasks. You usually won't need more than 1 CLI. Hosting CLIs on remote
    nodes is probably useless.

    Returns:
        CLI: The CLI.
    """
    if len(clis) == 0:
        raise Exception("No CLI found!")
    return clis[0]
