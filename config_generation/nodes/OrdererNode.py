import os

from config_generation.config import peer_nodes, \
    DOCKER_CONFIGTXDIR, DOCKER_NETWORK, LOCAL_CONFIGTXDIR_REL, LOCAL_CRYPTODIR_REL, LOGLEVEL, ORDERER_BASE_PORT, \
    ORDERER_CRYPTODIR, ORDERER_PRODDIR, ORDERER_WDIR, TLS_ENABLED, GRPC_PROTOC
from config_generation.util import indent
from config_generation.nodes.DockerService import DockerService
from config_generation.nodes.Kafka import Kafka
from config_generation.nodes.PeerNode import PeerNode
from config_generation.nodes.Zookeeper import Zookeeper


class OrdererNode(DockerService):
    def __init__(self, orderer_org, hostname, ip):
        super().__init__(ip)
        self.orderer_org = orderer_org
        self.hostname = hostname
        self.mapped_port = ORDERER_BASE_PORT + self.port_offset
        self.cryptodir_rel = '{:s}/orderers/{:s}'.format(self.orderer_org.cryptodir_rel, self.hostname)

    def connection_dict(self, client):
        return {
            'url': '{:s}://{:s}:{:d}'.format(GRPC_PROTOC, self.host.ip, self.mapped_port),
            'grpcOptions': {
                'ssl-target-name-override': self.hostname
            },
            'tlsCACerts': {'path': '{:s}/{:s}/{:s}/tls/server.crt'.format(
                client.wdir, LOCAL_CRYPTODIR_REL, self.cryptodir_rel)
            }
        }

    def docker_compose_yaml(self, n_indents=2):
        extra_hosts_str = self.host.get_extra_hosts_contents(
            [Kafka.__name__, OrdererNode.__name__, PeerNode.__name__], n_indents=n_indents)
        depends_on_str = self.host.get_depends_on_contents([Kafka.__name__, Zookeeper.__name__], n_indents=n_indents)
        root_cas = [
            '{:s}/{:s}/tls/ca.crt'.format(ORDERER_CRYPTODIR, self.cryptodir_rel)  # Own root CA
        ]
        for peer in peer_nodes:
            root_cas.append('{:s}/{:s}/tls/ca.crt'.format(ORDERER_CRYPTODIR, peer.cryptodir_rel))
        root_cas_str = ','.join(root_cas)
        return indent(
            n_indents,
            '{:s}:\n'.format(self.hostname) + \
            '  container_name: {:s}\n'.format(self.hostname) + \
            '  image: hyperledger/fabric-orderer\n' + \
            '  environment:\n' + \
            '    - FABRIC_LOGGING_SPEC={:s}\n'.format(LOGLEVEL) + \
            '    - ORDERER_GENERAL_LISTENADDRESS=0.0.0.0\n' + \
            '    - ORDERER_GENERAL_GENESISMETHOD=file\n' + \
            '    - ORDERER_GENERAL_GENESISFILE={:s}/{:s}\n'.format(DOCKER_CONFIGTXDIR, 'genesis.block') + \
            '    - ORDERER_GENERAL_LOCALMSPID={:s}\n'.format(self.orderer_org.msp_id) + \
            '    - ORDERER_GENERAL_LOCALMSPDIR={:s}/{:s}/msp\n'.format(ORDERER_CRYPTODIR, self.cryptodir_rel) + \
            '    - ORDERER_FILELEDGER_LOCATION={:s}\n'.format(ORDERER_PRODDIR) + \
            '    - ORDERER_GENERAL_TLS_ENABLED={:s}\n'.format(str(TLS_ENABLED).lower()) + \
            '    - ORDERER_GENERAL_TLS_PRIVATEKEY={:s}/{:s}/tls/server.key\n'.format(
                ORDERER_CRYPTODIR, self.cryptodir_rel) + \
            '    - ORDERER_GENERAL_TLS_CERTIFICATE={:s}/{:s}/tls/server.crt\n'.format(
                ORDERER_CRYPTODIR, self.cryptodir_rel) + \
            '    - ORDERER_GENERAL_TLS_ROOTCAS=[{:s}]\n'.format(root_cas_str) + \
            '    - ORDERER_KAFKA_TOPIC_REPLICATIONFACTOR=2\n' + \
            '    - ORDERER_KAFKA_VERBOSE=true\n' + \
            '    - ORDERER_KAFKA_RETRY_METADATA_RETRYBACKOFF=5000ms\n' +
            # '    - ORDERER_GENERAL_LEDGERTYPE=json\n' + \ # default: file
            '  working_dir: {:s}\n'.format(ORDERER_WDIR) + \
            '  command: orderer\n' + \
            '  ports:\n' + \
            # '    - {:d}:{:d}\n'.format(self.mapped_port, self.mapped_port)  + \
            '    - {:d}:{:d}\n'.format(self.mapped_port, ORDERER_BASE_PORT) + \
            '  volumes:\n' + \
            '    - {:s}/{:s}:{:s}\n'.format(self.host.wdir, LOCAL_CONFIGTXDIR_REL, DOCKER_CONFIGTXDIR) + \
            '    - {:s}/{:s}:{:s}\n'.format(self.host.wdir, LOCAL_CRYPTODIR_REL, ORDERER_CRYPTODIR) + \
            # '    - {:s}:{:s}\n'.format(self.hostname, ORDERER_PRODDIR)  + \
            '{:s}'.format(extra_hosts_str) + \
            '{:s}'.format(depends_on_str) + \
            '  networks:\n' + \
            '    - {:s}\n'.format(DOCKER_NETWORK))
