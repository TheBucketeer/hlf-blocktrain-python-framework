from config_generation.config import kafkas, orderer_nodes, \
    CONSORTIUM_NAME, ORDERER_PROFILE, ORDERER_PROFILE_KAFKA, LOCALHOST_IP, LOCAL_DESKTOP_WDIR, LOCAL_CRYPTODIR_REL
from config_generation.util import sort_services, indent, register_identity
from .OrdererNode import OrdererNode


class OrdererOrg(object):
    def __init__(self, name='', domain='example.com', orderer_ips=None, kafka_enabled=False):
        if orderer_ips is None:
            orderer_ips = []
        if name == '':
            name = 'OrdererOrg' + str(register_identity(self))
        self.name = name
        self.domain = domain
        self.cryptodir_rel = 'ordererOrganizations/{:s}'.format(self.domain)
        self.msp_id = name + 'MSP'
        if kafka_enabled:
            self.orderer_type = 'kafka'
            self.profile_name = ORDERER_PROFILE_KAFKA
        else:
            self.orderer_type = 'solo'
            self.profile_name = ORDERER_PROFILE
        self.orderers = []
        for i, ip in enumerate(orderer_ips):
            self.orderers.append(OrdererNode(self, 'orderer{:d}.{:s}'.format(i, self.domain), ip))

    def configtx_yaml(self, n_indents=4):
        return indent(
            n_indents,
            '- &{:s}\n'.format(self.name) + \
            '    Name: {:s}\n'.format(self.name) + \
            '    ID: {:s}\n'.format(self.msp_id) + \
            '    MSPDir: {:s}/{:s}/{:s}/msp\n'.format(LOCAL_DESKTOP_WDIR, LOCAL_CRYPTODIR_REL, self.cryptodir_rel) + \
            '    Policies:\n' + \
            '        Readers:\n' + \
            '            Type: Signature\n' + \
            '            Rule: OR(\'{:s}.member\')\n'.format(self.msp_id) + \
            '        Writers:\n' + \
            '            Type: Signature\n' + \
            '            Rule: OR(\'{:s}.member\')\n'.format(self.msp_id) + \
            '        Admins:\n' + \
            '            Type: Signature\n' + \
            '            Rule: OR(\'{:s}.admin\')\n\n'.format(self.msp_id))

    def configtx_yaml_defaults(self, n_indents=0):
        addresses_str = ''
        for orderer in sort_services(orderer_nodes):
            addresses_str += '\n        - {:s}:{:d}\n'.format(orderer.hostname, orderer.mapped_port)

        brokers_str = ''
        for kafka in sort_services(kafkas):
            brokers_str += '\n            - {:s}:{:d}'.format(kafka.host.ip, kafka.mapped_ports[0])
        return indent(
            n_indents,
            'Orderer: &OrdererDefaults\n' + \
            '    OrdererType: {:s}\n'.format(self.orderer_type) + \
            '    Addresses:{:s}\n'.format(addresses_str) + \
            '    BatchTimeout: 2s\n' + \
            '    BatchSize:\n' + \
            '        MaxMessageCount: 10\n' + \
            '        AbsoluteMaxBytes: 99 MB\n' + \
            '        PreferredMaxBytes: 512 KB\n' + \
            '    Policies:\n' + \
            '        Readers:\n' + \
            '            Type: ImplicitMeta\n' + \
            '            Rule: "ANY Readers"\n' + \
            '        Writers:\n' + \
            '            Type: ImplicitMeta\n' + \
            '            Rule: "ANY Writers"\n' + \
            '        Admins:\n' + \
            '            Type: ImplicitMeta\n' + \
            '            Rule: "MAJORITY Admins"\n' + \
            '        BlockValidation:\n' + \
            '            Type: ImplicitMeta\n' + \
            '            Rule: "ANY Writers"\n' + \
            '    Organizations:\n' + \
            '    Kafka:\n' + \
            '        Brokers:{:s}\n'.format(brokers_str))

    def configtx_yaml_profile(self, peer_orgs, n_indents=4):
        brokers_str = ''
        peer_org_names = ''
        for kafka in sort_services(kafkas):
            brokers_str += '\n              - {:s}:{:d}'.format(kafka.host.ip, kafka.mapped_ports[0])
        for peer_org in peer_orgs:
            peer_org_names += '\n                - *{:s}'.format(peer_org.name)
        return indent(
            n_indents,
            '{:s}:\n'.format(self.profile_name) + \
            '    Capabilities:\n' + \
            '        <<: *ChannelCapabilities\n' + \
            '    Policies:\n' + \
            '        Readers:\n' + \
            '            Type: ImplicitMeta\n' + \
            '            Rule: "ANY Readers"\n' + \
            '        Writers:\n' + \
            '            Type: ImplicitMeta\n' + \
            '            Rule: "ANY Writers"\n' + \
            '        Admins:\n' + \
            '            Type: ImplicitMeta\n' + \
            '            Rule: "MAJORITY Admins"\n' + \
            '    Orderer:\n' + \
            '        <<: *OrdererDefaults\n' + \
            '        OrdererType: {:s}\n'.format(self.orderer_type) + \
            '        Kafka:\n' + \
            '            Brokers:{:s}\n'.format(brokers_str) + \
            '        Organizations:\n' + \
            '            - *{:s}\n'.format(self.name) + \
            '        Capabilities:\n' + \
            '            <<: *OrdererCapabilities\n' + \
            '    Consortiums:\n' + \
            '        {:s}:\n'.format(CONSORTIUM_NAME) + \
            '            Organizations:{:s}\n'.format(peer_org_names))

    def crypto_config_yaml(self, n_indents=2):
        hosts_str = ''
        for orderer in self.orderers:
            hosts_str += '    - Hostname: {:s}\n'.format(orderer.hostname.split('.')[0]) + \
                         '      SANS:\n'
            if orderer.host.ip == LOCALHOST_IP:
                hosts_str += '        - "localhost"\n' + \
                             '        - "0.0.0.0"\n' + \
                             '        - "127.0.0.1"\n'
            hosts_str += '        - "{:s}"\n'.format(orderer.host.ip)
        return indent(
            n_indents,
            '- Name: {:s}\n'.format(self.name) + \
            '  Domain: {:s}\n'.format(self.domain) + \
            '  CA:\n' + \
            '      Country: US\n' + \
            '      Province: California\n' + \
            '      Locality: San Francisco\n' + \
            '  Specs:\n' + \
            '{:s}'.format(hosts_str))
