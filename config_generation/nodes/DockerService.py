from config_generation.config import IPS_TO_HOSTS
from config_generation.util import register_identity


class DockerService(object):
    def __init__(self, ip):
        self.host = IPS_TO_HOSTS[ip]
        self.host.register_service(self.__class__.__name__, self)
        self.port_offset = self.host.get_port_offset_for_service(self)
        self.service_id = register_identity(self)
