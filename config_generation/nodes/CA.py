import os

from config_generation.config import peer_orgs, \
    CA_BASE_PORT, DOCKER_NETWORK, FABRIC_CA_CONFIG, FABRIC_CA_HOME, FABRIC_CA_SCRIPTS, LOCAL_CA_SCRIPTS_REL, \
    LOCAL_CRYPTODIR_REL, LOCALHOST_IP, LOGLEVEL, TLS_ENABLED, UPDATE_AFF_FILE, LOCAL_DESKTOP_WDIR
from config_generation.nodes.DockerService import DockerService
from config_generation.util import indent


class CA(DockerService):
    def __init__(self, peer_org, ip=LOCALHOST_IP, username='admin', password='adminpw'):
        super().__init__(ip)
        # For now, we assume we never have more than 1 CA per organization
        self.hostname = 'ca.{:s}'.format(peer_org.hostname)
        self.peer_org = peer_org
        peer_org.cas.append(self)
        self.username = username
        self.password = password
        self.mapped_port = CA_BASE_PORT + self.port_offset
        self.sk_filename_placeholder = '<<CA_{:s}_SECRET_KEY>>'.format(self.peer_org.hostname)
        self.sk_filename = self.sk_filename_placeholder
        self.update_affiliations_file = '{:s}_{:s}'.format(UPDATE_AFF_FILE, self.hostname)

    def set_sk(self):
        sk_location = '{:s}/{:s}/{:s}/ca'.format(LOCAL_DESKTOP_WDIR, LOCAL_CRYPTODIR_REL, self.peer_org.cryptodir_rel)
        for file in os.listdir(sk_location):
            if file.endswith('_sk'):
                self.sk_filename = file
                print('Found SK for ' + self.hostname + ': ' + self.sk_filename)
                return
        raise Exception('No secret key found for {:s}'.format(self.hostname))

    def update_affiliations(self):
        script_location = '{:s}/{:s}'.format(FABRIC_CA_SCRIPTS, self.update_affiliations_file)
        self.host.execute_cmd('docker exec {:s} chmod +x {:s}'.format(self.hostname, script_location))
        self.host.execute_cmd('docker exec {:s} {:s}'.format(self.hostname, script_location))

    def connection_dict(self, client):
        return {
            'url': 'https://{:s}:{:d}'.format(self.host.ip, self.mapped_port),
            # 'url': 'https://{:s}:{:d}'.format(self.hostname, self.mapped_port),
            'x-mspid': self.peer_org.msp_id,
            # 'httpOptions': {'verify': False},
            'tlsCACerts': {
                'path': '{:s}/{:s}/{:s}/ca/tls/server.crt'.format(
                    client.wdir, LOCAL_CRYPTODIR_REL,self.peer_org.cryptodir_rel)
            },
            'registrar': [
                {'affiliation': self.peer_org.name.lower(), 'enrollId': self.username, 'enrollSecret': self.password}
            ],
            'caName': self.hostname
        }

    def update_affiliations_sh(self):
        contents = '#!/bin/bash\n' + \
                   'set -x\n' + \
                   'fabric-ca-client enroll -u ' + \
                   '"https://{:s}:{:s}@{:s}:{:d}" --tls.certfiles $FABRIC_CA_SERVER_TLS_CERTFILE\n'.format(
                       self.username, self.password, self.hostname, self.mapped_port)
        for peer_org in peer_orgs:
            contents += 'fabric-ca-client affiliation add ' + \
                        '{:s} --tls.certfiles $FABRIC_CA_SERVER_TLS_CERTFILE --url https://{:s}:{:d}\n'.format(
                            peer_org.name.lower(), self.hostname, self.mapped_port)
        contents += 'set +x\n'
        return contents

    def docker_compose_yaml(self, n_indents=2):
        from config_generation.nodes.OrdererNode import OrdererNode
        from config_generation.nodes.PeerNode import PeerNode

        extra_hosts_str = self.host.get_extra_hosts_contents(
            [OrdererNode.__name__, PeerNode.__name__], n_indents=n_indents)
        csr_hosts = '{:s},{:s}'.format(self.hostname, self.host.ip)
        if self.host.ip == LOCALHOST_IP:
            # This MUST be done if we want to work with the Java SDK
            csr_hosts += ',localhost'
        return indent(
            n_indents,
            '{:s}:\n'.format(self.hostname) + \
            '  container_name: {:s}\n'.format(self.hostname) + \
            '  image: hyperledger/fabric-ca\n' + \
            '  environment:\n' + \
            '    - FABRIC_CA_HOME={:s}\n'.format(FABRIC_CA_HOME) + \
            '    - FABRIC_CA_PORT={:d}\n'.format(self.mapped_port) + \
            '    - FABRIC_CA_SERVER_CA_NAME={:s}\n'.format(self.hostname) + \
            '    - FABRIC_CA_SERVER_CA_CERTFILE={:s}/{:s}-cert.pem\n'.format(FABRIC_CA_CONFIG, self.hostname) + \
            '    - FABRIC_CA_SERVER_CA_KEYFILE={:s}/{:s}\n'.format(FABRIC_CA_CONFIG, self.sk_filename) + \
            '    - FABRIC_CA_SERVER_CSR_HOSTS={:s}\n'.format(csr_hosts) + \
            '    - FABRIC_CA_SERVER_TLS_ENABLED={:s}\n'.format(str(TLS_ENABLED).lower()) + \
            '    - FABRIC_CA_SERVER_TLS_CERTFILE={:s}/tls/server.crt\n'.format(FABRIC_CA_CONFIG,
                                                                               self.hostname) + \
            '    - FABRIC_CA_SERVER_TLS_KEYFILE={:s}/tls/server.key\n'.format(FABRIC_CA_CONFIG,
                                                                              self.hostname) + \
            '    - FABRIC_LOGGING_SPEC={:s}\n'.format(LOGLEVEL) + \
            '  ports:\n' + \
            '    - {:d}:{:d}\n'.format(self.mapped_port, self.mapped_port) + \
            '  command: sh -c \'fabric-ca-server start -b {:s}:{:s} -p {:d} -d\'\n'.format(
                self.username, self.password, self.mapped_port) + \
            '  volumes:\n' + \
            '    - {:s}/{:s}/{:s}/ca/:{:s}\n'.format(
                self.host.wdir, LOCAL_CRYPTODIR_REL, self.peer_org.cryptodir_rel, FABRIC_CA_CONFIG) + \
            '    - {:s}/{:s}:{:s}\n'.format(self.host.wdir, LOCAL_CA_SCRIPTS_REL, FABRIC_CA_SCRIPTS) + \
            '{:s}'.format(extra_hosts_str) + \
            '  networks:\n' + \
            '    - {:s}\n'.format(DOCKER_NETWORK))
