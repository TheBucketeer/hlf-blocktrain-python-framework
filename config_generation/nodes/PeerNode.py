from config_generation.config import CC_NAME, channel_host_orderers, CHANNEL_NAME, COMPOSE_PROJECT_NAME, \
    DOCKER_CONFIGTXDIR, COUCHDB_BASE_PORT, \
    DOCKER_NETWORK, LOCAL_CCROOT_REL, LOGLEVEL, LOCAL_CONFIGTXDIR_REL, LOCAL_CRYPTODIR_REL, PEER_BASE_PORT0, \
    PEER_BASE_PORT1, PEER_BASE_PORT2, PEER_CCROOT, PEER_CRYPTODIR, PEER_PRODDIR, PEER_WDIR, TLS_ENABLED, peer_nodes, \
    CHANNEL_CREATE_TIMEOUT, GRPC_PROTOC, CC_VERSION, CC_LANGUAGE, PEER_CCPATH, PEER_CCINSTALLROOT
from config_generation.util import indent
from config_generation.nodes.DockerService import DockerService


class PeerNode(DockerService):
    def __init__(self, peer_org, hostname, ip, couchdb, channels=None):
        super().__init__(ip)
        if channels is None:
            channels = [CHANNEL_NAME]
        self.channels = channels
        self.peer_org = peer_org
        self.hostname = hostname
        self.couchdb = couchdb
        self.mapped_ports = (PEER_BASE_PORT0 + self.port_offset,
                             PEER_BASE_PORT1 + self.port_offset,
                             PEER_BASE_PORT2 + self.port_offset)
        self.tls_flags = {}
        self.cryptodir_rel = '{:s}/peers/{:s}'.format(self.peer_org.cryptodir_rel, self.hostname)

        if TLS_ENABLED:
            for channel in channels:
                orderer = channel_host_orderers[channel]
                self.tls_flags[channel] = ' --tls --cafile {:s}/{:s}/msp/tlscacerts/tlsca.{:s}-cert.pem'.format(
                    PEER_CRYPTODIR, orderer.cryptodir_rel, self.peer_org.domain)
        else:
            for channel in channels:
                self.tls_flags[channel] = ''

    # Wrapper function for the various *_cmd functions in this class
    def bash_cmd(self, cmd_type, as_admin=True, **kwargs):
        base_cmd = {
            'channel_create': self.channel_create_cmd,
            'fetch': self.channel_fetch_config_cmd,
            'install_cc': self.install_cc_cmd,
            'instantiate_cc': self.instantiate_cc_cmd,
            'invoke_cc': self.invoke_cc,
            'join': self.channel_join_cmd,
            'query_cc': self.query_cc_cmd,
            'update_anchors': self.update_anchors_cmd,
        }[cmd_type](**kwargs)
        return self.make_docker_cmd(base_cmd, as_admin=as_admin)

    def make_docker_cmd(self, cmd, as_admin=True):
        if as_admin:
            return 'docker exec {:s} {:s} {:s}'.format(self.peer_org.admin_msp_env_flag, self.hostname, cmd)
        else:
            return 'docker exec {:s} {:s}'.format(self.hostname, cmd)

    def channel_create_cmd(self, channel=CHANNEL_NAME):
        channel_host = channel_host_orderers[channel]
        return 'peer channel create -o {:s}:{:d} -c {:s} -f {:s}/channel.tx{:s} -t {:d}s'.format(
            channel_host.hostname, channel_host.mapped_port, channel, DOCKER_CONFIGTXDIR, self.tls_flags[channel],
            CHANNEL_CREATE_TIMEOUT)

    # Returns the Bash command to fetch the config block of an existing channel
    def channel_fetch_config_cmd(self, channel=CHANNEL_NAME):
        channel_host = channel_host_orderers[channel]
        return 'peer channel fetch 0 {:s}.block -o {:s}:{:d} -c {:s}{:s}'.format(
            channel, channel_host.hostname, channel_host.mapped_port, CHANNEL_NAME, self.tls_flags[channel])

    # Returns the Bash command to join an existing channel
    def channel_join_cmd(self, channel=CHANNEL_NAME):
        return 'peer channel join -b {:s}.block{:s}'.format(channel, self.tls_flags[channel])

    # Formats any arguments to be passed to the Chaincode
    @staticmethod
    def format_cc_args(args):
        return '\'{{"Args":[{:s}]}}\''.format(','.join(['"{:s}"'.format(str(a)) for a in args]))

    def install_cc_cmd(self):
        install_cc_param = '{:s}/{:s}'.format(PEER_CCINSTALLROOT, PEER_CCPATH)
        return 'peer chaincode install -n {:s} -v {:s} -l {:s} -p {:s}\n'.format(
            CC_NAME, CC_VERSION, CC_LANGUAGE, install_cc_param)

    def instantiate_cc_cmd(self, channel=CHANNEL_NAME, args=None):
        if args is None:
            args = []
        channel_host = channel_host_orderers[channel]
        args_str = PeerNode.format_cc_args(['init'] + args)
        return ('peer chaincode instantiate -o {:s}:{:d}{:s} -n {:s} -v {:s} -l {:s} -C {:s} -c '
                '{:s} -P "OR(\'{:s}.member\')"\n').format(
                    channel_host.hostname, channel_host.mapped_port, self.tls_flags[channel], CC_NAME,
                    CC_VERSION, CC_LANGUAGE, channel, args_str, self.peer_org.msp_id)

    @staticmethod
    def make_connection_params(peers):
        # Helper function for invoke_cc and query_cc: creates the connection parameters for a query/invocation.
        peer_connection_params = []
        for peer in peers:
            peer_connection_params.append('--peerAddresses {:s}:{:d}'.format(peer.hostname, peer.mapped_ports[0]))
            peer_connection_params.append('--tlsRootCertFiles {:s}/{:s}/tls/ca.crt'.format(
                PEER_CRYPTODIR, peer.cryptodir_rel))
        return ' '.join(peer_connection_params)

    def invoke_cc(self, channel=CHANNEL_NAME, chaincode=CC_NAME, peers=None, args=None):
        if args is None:
            args = []
        # If no set of peers is specified, ask all peers to endorse this transaction
        if peers is None:
            peers = peer_nodes
        channel_host = channel_host_orderers[channel]
        peer_connection_params_str = PeerNode.make_connection_params(peers)
        args_str = PeerNode.format_cc_args(args)
        return 'peer chaincode invoke --waitForEvent -o {:s}:{:d} -C {:s} -n {:s} {:s} -c {:s}{:s}'.format(
            channel_host.hostname, channel_host.mapped_port, channel, chaincode, peer_connection_params_str,
            args_str, self.tls_flags[channel])

    def query_cc_cmd(self, channel=CHANNEL_NAME, chaincode=CC_NAME, peers=None, args=None):
        if args is None:
            args = []
        # If no set of peers is specified, ask all peers to endorse this query
        if peers is None:
            peers = peer_nodes
        channel_host = channel_host_orderers[channel]
        peer_connection_params_str = PeerNode.make_connection_params(peers)
        args_str = PeerNode.format_cc_args(args)
        return ('peer chaincode query -o {:s}:{:d} -C {:s} -n {:s} {:s} '
                '-c {:s}{:s}').format(channel_host.hostname, channel_host.mapped_port, channel, chaincode,
                                      peer_connection_params_str, args_str, self.tls_flags[channel])

    def update_anchors_cmd(self, channel=CHANNEL_NAME):
        channel_host = channel_host_orderers[channel]
        return 'peer channel update -o {:s}:{:d} -c {:s} -f {:s}/{:s}anchors.tx{:s}'.format(
            channel_host.hostname, channel_host.mapped_port, channel, DOCKER_CONFIGTXDIR, self.peer_org.msp_id,
            self.tls_flags[channel])

    def connection_dict(self, client):
        return {
            'url': '{:s}://{:s}:{:d}'.format(GRPC_PROTOC, self.host.ip, self.mapped_ports[0]),
            'eventUrl': '{:s}://{:s}:{:d}'.format(GRPC_PROTOC, self.host.ip, self.mapped_ports[1]),
            'grpcOptions': {
                'ssl-target-name-override': self.hostname,
                'hostnameOverride': self.hostname
                # 'grpc.keepalive_time_ms': 1200000,
                # 'grpc.keepalive_timeout_ms': 30000,
            },
            'tlsCACerts': {
                'path': '{:s}/{:s}/{:s}/tls/server.crt'.format(client.wdir, LOCAL_CRYPTODIR_REL, self.cryptodir_rel)
            }
        }

    def docker_compose_yaml(self, n_indents=2):
        from config_generation.nodes.CA import CA
        from config_generation.nodes.OrdererNode import OrdererNode

        extra_hosts_str = self.host.get_extra_hosts_contents([CA.__name__, OrdererNode.__name__, PeerNode.__name__])
        # TODO: couchdb not on this host?
        deps = []
        if self.couchdb and self.couchdb.host == self.host:
            deps.append([self.couchdb])
        depends_on_str = self.host.get_depends_on_contents(
            [OrdererNode.__name__], known_dependencies=deps, n_indents=n_indents)
        # Fetch relevant data from Orderer/CouchDB, and don't crash if this peer has no local dependencies on these
        if self.couchdb:
            db_name = 'CouchDB'
            couchdb_name = self.couchdb.hostname
            couchdb_username = self.couchdb.username
            couchdb_password = self.couchdb.password
        else:
            db_name = 'goleveldb'
            couchdb_name = couchdb_username = couchdb_password = ''
        if len(self.peer_org.peers) == 1:
            gossip_bootstrap_peer = self
        else:
            # Choose the 'next' peer in the list as our gossip bootstrap
            self_i = self.peer_org.peers.index(self)
            gossip_bootstrap_peer = self.peer_org.peers[(self_i + 1) % len(self.peer_org.peers)]

        return indent(
            n_indents,
            '{:s}:\n'.format(self.hostname) + \
            '  container_name: {:s}\n'.format(self.hostname) + \
            '  image: hyperledger/fabric-peer\n' + \
            '  environment:\n' + \
            '    - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock\n' + \
            '    - CORE_PEER_ID={:s}\n'.format(self.hostname) + \
            '    - FABRIC_LOGGING_SPEC={:s}\n'.format(LOGLEVEL) + \
            '    - CORE_CHAINCODE_LOGGING_LEVEL={:s}\n'.format(LOGLEVEL) + \
            '    - CORE_CHAINCODE_LOGGING_SHIM={:s}\n'.format(LOGLEVEL) + \
            '    - CORE_CHAINCODE_STARTUPTIMEOUT=3000s\n' + \
            '    - CORE_CHAINCODE_EXECUTETIMEOUT=300s\n' + \
            '    - CORE_LOGGING_GRPC={:s}\n'.format(LOGLEVEL) + \
            '    - GRPC_VERBOSITY={:s}\n'.format(LOGLEVEL) + \
            '    - GRPC_TRACE=connectivity_state,health_check_client,timer,timer_check,tcp,api\n' + \
            # '    - CORE_CHAINCODE_KEEPALIVE=10\n' + \
            '    - CORE_PEER_LOCALMSPID={:s}\n'.format(self.peer_org.msp_id) + \
            '    - CORE_PEER_MSPCONFIGPATH={:s}/{:s}/msp\n'.format(PEER_CRYPTODIR, self.cryptodir_rel) + \
            '    - CORE_PEER_FILESYSTEMPATH={:s}\n'.format(PEER_PRODDIR) + \
            '    - CORE_PEER_ADDRESS={:s}:{:d}\n'.format(self.hostname, self.mapped_ports[0]) + \
            '    - CORE_PEER_CHAINCODEADDRESS={:s}:{:d}\n'.format(self.hostname, self.mapped_ports[2]) + \
            '    - CORE_PEER_LISTENADDRESS={:s}:{:d}\n'.format(self.hostname, self.mapped_ports[0]) + \
            '    - CORE_PEER_CHAINCODELISTENADDRESS={:s}:{:d}\n'.format(self.hostname, self.mapped_ports[2]) + \
            '    - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE={:s}_{:s}\n'.format(
                COMPOSE_PROJECT_NAME, DOCKER_NETWORK) + \
            '    - CORE_PEER_TLS_ENABLED={:s}\n'.format(str(TLS_ENABLED).lower()) + \
            '    - CORE_PEER_TLS_CERT_FILE={:s}/{:s}/tls/server.crt\n'.format(PEER_CRYPTODIR, self.cryptodir_rel) + \
            '    - CORE_PEER_TLS_KEY_FILE={:s}/{:s}/tls/server.key\n'.format(PEER_CRYPTODIR, self.cryptodir_rel) + \
            '    - CORE_PEER_TLS_ROOTCERT_FILE={:s}/{:s}/tls/ca.crt\n'.format(PEER_CRYPTODIR, self.cryptodir_rel) + \
            # Default:
            '    - CORE_PEER_GOSSIP_USELEADERELECTION=true\n' + \
            # Default:
            '    - CORE_PEER_GOSSIP_ORGLEADER=false\n' + \
            '    - CORE_PEER_GOSSIP_BOOTSTRAP={:s}:{:d}\n'.format(
                gossip_bootstrap_peer.hostname, gossip_bootstrap_peer.mapped_ports[0]) + \
            '    - CORE_PEER_GOSSIP_ENDPOINT={:s}:{:d}\n'.format(self.hostname, self.mapped_ports[0]) + \
            '    - CORE_PEER_GOSSIP_EXTERNALENDPOINT={:s}:{:d}\n'.format(self.hostname, self.mapped_ports[0]) + \
            # For profiling services over http port 6060, see
            # https://github.com/hyperledger-archives/fabric/wiki/Profiling-the-Hyperledger-Fabric
            # '    - CORE_PEER_PROFILE_ENABLED=true\n' + \
            '    - CORE_LEDGER_STATE_STATEDATABASE={:s}\n'.format(db_name) + \
            '    - CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS={:s}:{:d}\n'.format(
                couchdb_name, COUCHDB_BASE_PORT) + \
            '    - CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME={:s}\n'.format(couchdb_username) + \
            '    - CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD={:s}\n'.format(couchdb_password) + \
            '  working_dir: {:s}\n'.format(PEER_WDIR) + \
            '  command: peer node start --peer-chaincodedev=true\n' + \
            '  ports:\n' + \
            '    - {:d}:{:d}\n'.format(self.mapped_ports[0], self.mapped_ports[0]) + \
            '    - {:d}:{:d}\n'.format(self.mapped_ports[1], self.mapped_ports[1]) + \
            '    - {:d}:{:d}\n'.format(self.mapped_ports[2], self.mapped_ports[2]) + \
            '  volumes:\n' + \
            '    - /var/run/:/host/var/run/\n' + \
            '    - {:s}/{:s}:{:s}\n'.format(self.host.wdir, LOCAL_CRYPTODIR_REL, PEER_CRYPTODIR) + \
            '    - {:s}/{:s}:{:s}\n'.format(self.host.wdir, LOCAL_CONFIGTXDIR_REL, DOCKER_CONFIGTXDIR) + \
            '    - {:s}/{:s}:{:s}\n'.format(self.host.wdir, LOCAL_CCROOT_REL, PEER_CCROOT) + \
            # '    - {:s}:{:s}\n'.format(self.hostname, PEER_PRODDIR) + \
            '{:s}'.format(extra_hosts_str) + \
            '{:s}'.format(depends_on_str) + \
            '  networks:\n' + \
            '    - {:s}\n'.format(DOCKER_NETWORK))
