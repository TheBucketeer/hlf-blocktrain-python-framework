from config_generation.config import COMPOSE_PROJECT_NAME, DOCKER_GOPATH, \
    DOCKER_NETWORK, LOGLEVEL, PEER_CCROOT, \
    PEER_CRYPTODIR, LOCAL_CCROOT_REL, LOCAL_CRYPTODIR_REL
from config_generation.util import indent
from config_generation.nodes.DockerService import DockerService
from config_generation.nodes.OrdererNode import OrdererNode
from config_generation.nodes.PeerNode import PeerNode


class Chaincode(DockerService):
    def __init__(self, peer_node):
        super().__init__(peer_node.ip)
        self.peer_node = peer_node
        self.ip = peer_node.ip
        self.hostname = 'chaincode{:d}'.format(self.service_id)

    def docker_compose_yaml(self, n_indents=2):
        depends_on_str = self.host.get_depends_on_contents(
            [OrdererNode.__name__, PeerNode.__name__], n_indents=n_indents)
        return indent(
            n_indents,
            '{:s}:\n'.format(self.hostname) + \
            '  image: hyperledger/fabric-ccenv\n' + \
            '  tty: true\n' + \
            '  environment:\n' + \
            '    - GOPATH={:s}\n'.format(DOCKER_GOPATH) + \
            '    - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock\n' + \
            '    - FABRIC_LOGGING_SPEC={:s}\n'.format(LOGLEVEL) + \
            '    - CORE_PEER_ID={:s}\n'.format(self.hostname) + \
            '    - CORE_PEER_ADDRESS={:s}:{:d}\n'.format(self.peer_node.hostname, self.peer_node.mapped_ports[0]) + \
            '    - CORE_PEER_LOCALMSPID={:s}\n'.format(self.peer_node.peer_org.msp_id) + \
            '    - CORE_PEER_MSPCONFIGPATH={:s}/{:s}/msp\n'.format(
                PEER_CRYPTODIR, self.peer_node.peer_org.cryptodir_admin_rel) + \
            '    - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE={:s}_{:s}\n'.format(
                COMPOSE_PROJECT_NAME, DOCKER_NETWORK) + \
            '  working_dir: {:s}\n'.format(PEER_CCROOT) + \
            '  command: /bin/bash -c \'sleep 6000000\'\n' + \
            '  volumes:\n' + \
            '    - /var/run/:/host/var/run/\n' + \
            '    - {:s}/{:s}:{:s}\n'.format(self.host.wdir, LOCAL_CRYPTODIR_REL, PEER_CRYPTODIR) + \
            '    - {:s}/{:s}:{:s}\n'.format(self.host.wdir, LOCAL_CCROOT_REL, PEER_CCROOT) +
            '{:s}'.format(depends_on_str))
