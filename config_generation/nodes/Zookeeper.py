from config_generation.config import zookeepers, \
    DOCKER_NETWORK, LOCALHOST_IP, ZOOKEEPER_BASE_PORT, ZOOKEEPER_CONNECT_PORT, ZOOKEEPER_ELECT_PORT
from config_generation.util import sort_services, indent
from config_generation.nodes.DockerService import DockerService
from config_generation.nodes.Kafka import Kafka


class Zookeeper(DockerService):
    def __init__(self, domain='example.com', ip=LOCALHOST_IP):
        super().__init__(ip)
        self.hostname = 'zookeeper{:d}.{:s}'.format(self.service_id, domain)
        # Keep connect/elect ports the same for now
        self.mapped_ports = (ZOOKEEPER_BASE_PORT + self.port_offset,
                             ZOOKEEPER_CONNECT_PORT + self.port_offset,
                             ZOOKEEPER_ELECT_PORT + self.port_offset)
        self.service_id += 1  # Zookeeper IDs must be > 1

    def docker_compose_yaml(self, n_indents=2):
        zoo_servers_str_list = []
        # Sort the zookeepers to ensure predictable output
        for zookeeper in sort_services(zookeepers):
            zoo_servers_str_list.append(
                'server.{:d}={:s}:{:d}:{:d}'.format(
                    zookeeper.service_id, zookeeper.hostname, zookeeper.mapped_ports[1], zookeeper.mapped_ports[2]))
        zoo_servers_str = ' '.join(zoo_servers_str_list)
        extra_hosts_str = self.host.get_extra_hosts_contents([Kafka.__name__, Zookeeper.__name__], n_indents=n_indents)
        return indent(
            n_indents,
            '{:s}:\n'.format(self.hostname) + \
            '  container_name: {:s}\n'.format(self.hostname) + \
            '  image: hyperledger/fabric-zookeeper\n' + \
            '  restart: always\n' + \
            '  environment:\n' + \
            '    - ZOO_MY_ID={:d}\n'.format(self.service_id) + \
            '    - ZOO_SERVERS={:s}\n'.format(zoo_servers_str) + \
            '    - ZOOKEEPER_CLIENT_PORT={:d}\n'.format(self.mapped_ports[0]) + \
            '    - ZOOKEEPER_TICK_TIME=2000\n' + \
            '    - ZOO_INIT_LIMIT=10\n' + \
            '    - ZOO_SYNC_LIMIT=5\n' + \
            '  ports:\n' + \
            '    - {:d}:{:d}\n'.format(self.mapped_ports[0], self.mapped_ports[0]) + \
            '    - {:d}:{:d}\n'.format(self.mapped_ports[1], self.mapped_ports[1]) + \
            '    - {:d}:{:d}\n'.format(self.mapped_ports[2], self.mapped_ports[2]) + \
            '{:s}'.format(extra_hosts_str) + \
            '  networks:\n' + \
            '    - {:s}\n'.format(DOCKER_NETWORK))
