from config_generation.config import zookeepers, \
    DOCKER_NETWORK, KAFKA_EXTERNAL_PORT, LOCALHOST_IP, KAFKA_REPLICATE_PORT, kafkas
from config_generation.nodes.DockerService import DockerService
from config_generation.util import sort_services, indent


class Kafka(DockerService):
    def __init__(self, domain='example.com', ip=LOCALHOST_IP):
        super().__init__(ip)
        self.hostname = 'kafka{:d}.{:s}'.format(self.service_id, domain)
        self.mapped_ports = [KAFKA_EXTERNAL_PORT + self.port_offset, KAFKA_REPLICATE_PORT + self.port_offset]
        self.service_id += 1  # Kafka IDs must be > 1

    def docker_compose_yaml(self, n_indents=2):
        from config_generation.nodes.OrdererNode import OrdererNode
        from config_generation.nodes.Zookeeper import Zookeeper

        extra_hosts_str = self.host.get_extra_hosts_contents(
            [Kafka.__name__, Zookeeper.__name__, OrdererNode.__name__], n_indents=n_indents)
        depends_on_str = self.host.get_depends_on_contents([Zookeeper.__name__], n_indents=n_indents)

        # Sort the zookeepers to ensure predictable output
        zookeepers_sorted = sort_services(zookeepers)
        zookeeper_connect_str = ','.join(['{:s}:{:d}'.format(z.hostname, z.mapped_ports[0]) for z in zookeepers_sorted])
        replication_factor = max(int(2. * len(kafkas) / 3.), 1)
        min_replicas_synced = max(replication_factor - 1, 1)
        return indent(
            n_indents,
            '{:s}:\n'.format(self.hostname) +
            '  container_name: {:s}\n'.format(self.hostname) +
            '  image: hyperledger/fabric-kafka\n' +
            '  restart: always\n' +
            '  environment:\n' +
            '    - KAFKA_PORT={:d}\n'.format(self.mapped_ports[0]) +
            '    - KAFKA_ADVERTISED_HOST_NAME={:s}\n'.format(self.host.ip) +
            '    - KAFKA_ADVERTISED_PORT={:d}\n'.format(self.mapped_ports[0]) +
            '    - KAFKA_BROKER_ID={:d}\n'.format(self.service_id) +
            '    - KAFKA_ZOOKEEPER_CONNECT={:s}\n'.format(zookeeper_connect_str) +
            '    - KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR=2\n' +
            '    - KAFKA_MESSAGE_MAX_BYTES={:d}\n'.format(1024 ** 2) +
            '    - KAFKA_REPLICA_FETCH_MAX_BYTES={:d}\n'.format(1024 ** 2) +
            '    - KAFKA_UNCLEAN_LEADER_ELECTION_ENABLE=false\n' +
            '    - KAFKA_LOG_RETENTION_MS=-1\n' +
            '    - KAFKA_MIN_INSYNC_REPLICAS={:d}\n'.format(min_replicas_synced) +
            '    - KAFKA_DEFAULT_REPLICATION_FACTOR={:d}\n'.format(replication_factor) +
            '  ports:\n' +
            # '    - {:d}:{:d}\n'.format(self.mapped_port, self.mapped_port) +
            '    - {:d}:{:d}\n'.format(self.mapped_ports[0], self.mapped_ports[0]) +
            '    - {:d}:{:d}\n'.format(self.mapped_ports[1], KAFKA_REPLICATE_PORT) +
            '{:s}'.format(extra_hosts_str) +
            '{:s}'.format(depends_on_str) +
            '  networks:\n' +
            '    - {:s}\n'.format(DOCKER_NETWORK))
