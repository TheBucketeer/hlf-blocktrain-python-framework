import json
import os

from config_generation.config import cas, orderer_nodes, peer_nodes, peer_orgs, CC_NAME, CC_VERSION, CHANNEL_NAME, \
    CLIENT_COMPILE_FILE, CLIENT_CONNECTION_FILE, CLIENT_EVENTLOG_FILE, CLIENT_INFO, \
    CLIENT_LOG_FILE, CLIENT_RUN_FILE, CLIENT_SETTINGS_FILE, CONN_PROFILE_NETWORK, CONN_PROFILE_VERSION, \
    LOCAL_CLIENTROOT_REL, LOCAL_CRYPTODIR_REL, LOCAL_KEYSTORE_REL, LOCAL_DESKTOP_WDIR, LOCALHOST_IP, LOCAL_PI_WDIR, \
    TIME_MS_STRING, \
    TRAIN_CLIENT_NAME, CLIENT_LOG4J_PROPS_FILE, CLIENT_LOG4J_FILE, IPS_TO_HOSTS, LOCAL_CLIENTDIR_REL
from config_generation.util import register_identity


class Client(object):
    def __init__(self, ip, client_type, chaincode=CC_NAME, channel=CHANNEL_NAME, is_cli=False,
                 client_code_subdir=LOCAL_CLIENTDIR_REL, train_address=None):
        asset_nr = register_identity(self, identifier=client_type)
        self.asset_id = '{:s}{:s}'.format(CLIENT_INFO[client_type]['aid_prefix'], str(asset_nr).rjust(3, '0'))
        self.chaincode = chaincode
        self.channel = channel
        if ip == LOCALHOST_IP:
            self.wdir = LOCAL_DESKTOP_WDIR
        else:
            self.wdir = LOCAL_PI_WDIR
        self.client_code_dir = '{:s}/{:s}/{:s}'.format(self.wdir, LOCAL_CLIENTROOT_REL, client_code_subdir)
        self.client_type = client_type
        self.compile_filename = '{:s}_{:s}'.format(self.asset_id, CLIENT_COMPILE_FILE)
        self.clientsettings_filename = '{:s}_{:s}'.format(self.asset_id, CLIENT_SETTINGS_FILE)
        self.connection_json_filename = '{:s}_{:s}'.format(self.asset_id, CLIENT_CONNECTION_FILE)
        self.run_filename = '{:s}_{:s}'.format(self.asset_id, CLIENT_RUN_FILE)
        self.log_filename = '{:s}_{:s}'.format(self.asset_id, CLIENT_LOG_FILE)
        self.log4j_log_filename = '{:s}_{:s}'.format(self.asset_id, CLIENT_LOG4J_FILE)
        self.log4j_props_filename = '{:s}_{:s}'.format(self.asset_id, CLIENT_LOG4J_PROPS_FILE)
        self.eventlog_filename = '{:s}_{:s}'.format(self.asset_id, CLIENT_EVENTLOG_FILE)
        self.host = IPS_TO_HOSTS[ip]
        self.host.clients.append(self)
        self.main_class = CLIENT_INFO[client_type]['main_class']
        if is_cli:
            self.main_class += "CLI"
        self.peer_org = None  # to be filled in later
        self.sk_filename_placeholder = '<<CLIENT_{:s}_SECRET_KEY>>'.format(self.asset_id)
        self.sk_filename = self.sk_filename_placeholder
        self.train_address = train_address
        self.cryptodir_rel = None  # to be filled in later

    def set_peer_org(self, peer_org):
        self.peer_org = peer_org
        self.cryptodir_rel = 'peerOrganizations/{:s}/users/{:s}@{:s}'.format(
            self.peer_org.hostname, self.asset_id, self.peer_org.hostname)

    def set_sk(self):
        sk_location = '{:s}/{:s}/{:s}/msp/keystore'.format(LOCAL_DESKTOP_WDIR, LOCAL_CRYPTODIR_REL, self.cryptodir_rel)
        for file in os.listdir(sk_location):
            if file.endswith('_sk'):
                self.sk_filename = file
                print('Found SK for ' + self.asset_id + '@' + self.peer_org.hostname + ': ' + self.sk_filename)
                return
        raise Exception('No secret key found for {:s}'.format(self.asset_id))

    def connection_dict_clientinfo(self):
        assert self.peer_org is not None
        return {
            'organization': self.peer_org.name,
            'credentialStore': {
                'path': '{:s}/{:s}/kvs'.format(self.wdir, LOCAL_KEYSTORE_REL, 'kvs'),
                'cryptoStore': {
                    'path': '{:s}/{:s}/cvs'.format(self.wdir, LOCAL_KEYSTORE_REL)
                },
                'wallet': 'wedontusewallets'
            }
        }

    def connection_dict(self):
        assert self.peer_org is not None
        assert len(cas) > 0
        assert len(orderer_nodes) > 0
        assert len(peer_nodes) > 0
        assert len(peer_orgs) > 0
        return {
            'name': CONN_PROFILE_NETWORK,
            'x-type': 'hlfv1',
            'description': 'The network that can cure SPARCs',
            'version': '1.0.0',
            'x-fabricVersion': 'v1.4.0',
            'client': self.connection_dict_clientinfo(),
            'certificateAuthorities': {ca.hostname: ca.connection_dict(self) for ca in cas},
            'channels': {
                CHANNEL_NAME: {
                    'chaincodes': ['{:s}:{:s}'.format(CC_NAME, CC_VERSION)],
                    'orderers': [orderer_node.hostname for orderer_node in orderer_nodes],
                    'peers': {
                        peer_node.hostname: {
                            'endorsingPeer': True,
                            'chaincodeQuery': True,
                            'ledgerQuery': True,
                            'eventSource': True
                        } for peer_node in peer_nodes
                    }
                }
            },
            'orderers': {orderer_node.hostname: orderer_node.connection_dict(self) for orderer_node in orderer_nodes},
            'organizations': {peer_org.name: peer_org.connection_dict(self) for peer_org in peer_orgs},
            'peers': {peer_node.hostname: peer_node.connection_dict(self) for peer_node in peer_nodes}
        }

    def connection_json(self):
        return json.dumps(self.connection_dict(), indent=4, sort_keys=True)

    def clientsettings_json(self):
        assert self.peer_org is not None
        clientsettings_dict = {
            'asset_id': self.asset_id,
            'affiliation': '{:s}'.format(self.peer_org.name.lower()),
            'certfile': '{:s}/{:s}/{:s}/msp/signcerts/{:s}@{:s}-cert.pem'.format(
                self.wdir, LOCAL_CRYPTODIR_REL, self.cryptodir_rel, self.asset_id, self.peer_org.hostname),
            'channel': self.channel,
            'chaincode': {
                'name': self.chaincode
            },
            'connection_profile': '{:s}/{:s}'.format(self.wdir, self.connection_json_filename),
            'eventlogfile': self.eventlog_filename,
            'keyfile': '{:s}/{:s}/{:s}/msp/keystore/{:s}'.format(
                self.wdir, LOCAL_CRYPTODIR_REL, self.cryptodir_rel, self.sk_filename),
            'logfile': self.log_filename,
            'main_class': self.main_class,
            'network_version': '{:s}:{:s}_{:d}'.format(CONN_PROFILE_NETWORK, CONN_PROFILE_VERSION, TIME_MS_STRING),
            'organization': self.peer_org.name,
            'username': self.asset_id,
            'working_directory': self.wdir
        }
        if self.client_type == TRAIN_CLIENT_NAME:
            assert self.train_address is not None, 'Train clients can not have uninitialised train address'
            clientsettings_dict.update({'train_address': self.train_address})
        return json.dumps(clientsettings_dict, indent=4, sort_keys=True)

    def run_client_sh(self):
        return 'set -ev\n' + \
               'sudo java -D"log4j.configuration=file:{:s}/{:s}" '.format(self.wdir, self.log4j_props_filename) + \
               '-cp {:s}/target/BlockchainClient.jar {:s} {:s}/{:s} $@\n'.format(
                   self.client_code_dir, self.main_class, self.wdir, self.clientsettings_filename)

    def compile_client_sh(self):
        return 'set -ev\n' + \
               'cd {:s}\n'.format(self.client_code_dir) + \
               'mvn clean install\n'

    def log4j_props(self):
        return 'log4j.rootLogger=DEBUG, file\n' + \
               'log4j.appender.file=org.apache.log4j.RollingFileAppender\n' + \
               'log4j.appender.file.File={:s}/{:s}\n'.format(self.wdir, self.log4j_log_filename) + \
               'log4j.appender.file.MaxFileSize=10MB\n' + \
               'log4j.appender.file.MaxBackupIndex=10\n' + \
               'log4j.appender.file.layout=org.apache.log4j.PatternLayout\n' + \
               'log4j.appender.file.layout.ConversionPattern=%d{yyyy-MM-dd HH:mm:ss} %-5p %c{1}:%L - %m%n\n'
