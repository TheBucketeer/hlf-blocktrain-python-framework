import os

from config_generation.config import LOCAL_CRYPTODIR_REL, PEER_CRYPTODIR, LOCAL_DESKTOP_WDIR
from config_generation.nodes.PeerNode import PeerNode
from config_generation.util import indent, register_identity


class PeerOrg(object):
    def __init__(self, name='', domain='example.com', admin_ips=None, couchdb_deps=None, clients=None,
                 enable_node_ous=True, endorsement_role='ADMIN'):
        if admin_ips is None:
            admin_ips = []
        if couchdb_deps is None:
            couchdb_deps = []
        if clients is None:
            clients = []
        if name == '':
            name = 'Org{:d}'.format(register_identity(self))
        assert len(admin_ips) > 0, '{:s} has no peers?!'.format(name)
        self.admin_sk_filename_placeholder = '<<ORG_{:s}_SECRET_KEY>>'.format(name)
        self.admin_sk_filename = self.admin_sk_filename_placeholder
        # To be filled in when the CAs are constructed
        self.cas = []
        self.domain = domain
        self.enable_node_ous = enable_node_ous
        # A peer is a Member who's OU is defined as the peer OU in the MSP channel configuration
        # A client is a Member who's OU is defined as the client OU in the MSP channel configuration.
        assert endorsement_role.lower() in ['admin', 'client', 'member', 'peer']
        self.endorsement_role = 'Role.{:s}'.format(endorsement_role.upper())
        self.hostname = '{:s}.{:s}'.format(name.lower(), self.domain)
        self.cryptodir_rel = 'peerOrganizations/{:s}'.format(self.hostname)
        self.cryptodir_admin_rel = '{:s}/users/Admin@{:s}'.format(self.cryptodir_rel, self.hostname)
        self.msp_id = name + 'MSP'
        self.name = name
        self.n_admins = len(admin_ips)
        self.admin_msp_env_flag = '-e "CORE_PEER_MSPCONFIGPATH={:s}/{:s}/msp"'.format(
            PEER_CRYPTODIR, self.cryptodir_admin_rel)

        self.peers = []
        for i in range(len(admin_ips)):
            self.peers.append(PeerNode(self, 'peer{:d}.{:s}'.format(i, self.hostname), admin_ips[i], couchdb_deps[i]))
        # For now, the first peer will be anchor peer
        self.anchor_peer = self.get_anchor_peer()

        self.clients = []
        for client in clients:
            client.set_peer_org(self)
            self.clients.append(client)

    def get_anchor_peer(self):
        assert len(self.peers) > 0
        return self.peers[0]

    def set_admin_sk(self):
        for file in os.listdir('{:s}/{:s}/{:s}/msp/keystore/'.format(
                LOCAL_DESKTOP_WDIR, LOCAL_CRYPTODIR_REL, self.cryptodir_admin_rel)):
            if file.endswith('_sk'):
                self.admin_sk_filename = file
                print('Found SK for admin of ' + self.hostname + ': ' + self.admin_sk_filename)
                return
        raise Exception('No admin secret key found for {:s}'.format(self.hostname))

    # Output must be edited later to fill in the actual secret key
    def connection_dict(self, client):
        print(self.admin_sk_filename)
        return {
            'mspid': self.msp_id,
            'peers': [peer_node.hostname for peer_node in self.peers],
            'certificateAuthorities': [ca.hostname for ca in self.cas],
            'adminPrivateKey': {
                'path': '{:s}/{:s}/{:s}/msp/keystore/{:s}'.format(
                    client.wdir, LOCAL_CRYPTODIR_REL, self.cryptodir_admin_rel, self.admin_sk_filename)
            },
            'signedCert': {
                'path': '{:s}/{:s}/{:s}/msp/signcerts/Admin@{:s}-cert.pem'.format(
                    client.wdir, LOCAL_CRYPTODIR_REL, self.cryptodir_admin_rel, self.hostname)
            }
        }

    def configtx_yaml(self, n_indents=4):
        return indent(
            n_indents,
            '- &{:s}\n'.format(self.name) + \
            '    Name: {:s}\n'.format(self.name) + \
            '    ID: {:s}\n'.format(self.msp_id) + \
            '    MSPDir: {:s}/{:s}/{:s}/msp\n'.format(LOCAL_DESKTOP_WDIR, LOCAL_CRYPTODIR_REL, self.cryptodir_rel) + \
            # '    AdminPrincipal: {:s}\n'.format(self.endorsement_role) + \
            '    Policies:\n' + \
            '        Readers:\n' + \
            '            Type: Signature\n' + \
            '            Rule: OR(\'{0}.admin\', \'{0}.peer\', \'{0}.client\', \'{0}.member\')\n'.format(
                self.msp_id) + \
            # '            Rule: OR(\'{0}.admin\', \'{0}.peer\', \'{0}.client\')\n'.format(self.msp_id) + \
            '        Writers:\n' + \
            '            Type: Signature\n' + \
            '            Rule: OR(\'{0}.admin\', \'{0}.client\', \'{0}.member\')\n'.format(self.msp_id) + \
            # '            Rule: OR(\'{0}.admin\', \'{0}.client\')\n'.format(self.msp_id) + \
            # '            Rule: OR(\'{0}.admin\', \'{0}.peer\', \'{0}.client\')\n'.format(self.msp_id) + \
            '        Admins:\n' + \
            '            Type: Signature\n' + \
            '            Rule: OR(\'{0}.admin\')\n'.format(self.msp_id) + \
            # '            Rule: OR(\'{0}.admin\', \'{0}.peer\', \'{0}.client\')\n'.format(self.msp_id) + \
            '    AnchorPeers:\n' + \
            '        - Host: {:s}\n'.format(self.anchor_peer.hostname) + \
            '          Port: {:d}\n'.format(self.anchor_peer.mapped_ports[0]))

    def crypto_config_yaml(self, n_indents=2):
        hosts_str = ''
        for peer_node in self.peers:
            hosts_str += indent(
                n_indents + 2,
                '- Hostname: {:s}\n'.format(peer_node.hostname.split('.')[0]) + \
                '  SANS:\n' + \
                '    - "localhost"\n' + \
                '    - "0.0.0.0"\n' + \
                '    - "127.0.0.1"\n' + \
                '    - "{:s}"\n'.format(peer_node.host.ip))
        cas_sans = indent(n_indents + 4, '\n'.join(['- "{:s}"'.format(ca.host.ip) for ca in self.cas]) + '\n')
        usernames = sorted([client.asset_id for client in self.clients])
        usernames_str = ''.join(['      - {:s}\n'.format(username) for username in usernames])
        return indent(
            n_indents,
            '- Name: {:s}\n'.format(self.name) + \
            '  Domain: {:s}\n'.format(self.hostname) + \
            '  EnableNodeOUs: {:s}\n'.format(str(self.enable_node_ous).lower()) + \
            '  Specs:\n' + \
            '{:s}'.format(hosts_str) + \
            '  Users:\n' + \
            '    Count: 0\n' + \
            '    Names:\n' + \
            '{:s}'.format(usernames_str) + \
            '  CA:\n' + \
            '    Hostname: ca\n' + \
            '    Country: US\n' + \
            '    Province: California\n' + \
            '    Locality: San Francisco\n' + \
            '    SANS:\n' + \
            '      - "0.0.0.0"\n' + \
            '      - "localhost"\n' + \
            '      - "127.0.0.1"\n' + \
            '{:s}'.format(cas_sans))
