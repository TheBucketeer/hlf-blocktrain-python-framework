from config_generation.config import channel_host_orderers, CC_LANGUAGE, CC_NAME, CC_VERSION, CHANNEL_NAME, \
    DOCKER_GOPATH, DOCKER_NETWORK, LOCAL_CCROOT_REL, \
    LOCAL_CRYPTODIR_REL, LOCALHOST_IP, LOGLEVEL, PEER_CCINSTALLROOT, PEER_CCPATH, PEER_CCROOT, \
    PEER_CRYPTODIR, PEER_WDIR, TLS_ENABLED
from config_generation.nodes.DockerService import DockerService
from config_generation.nodes.PeerNode import PeerNode
from config_generation.util import indent


class CLI(DockerService):
    def __init__(self, ip=LOCALHOST_IP):
        super().__init__(ip)
        self.peer_org = None
        self.peer_node = None
        self.hostname = 'cli' + str(self.service_id)
        self.env_flags = ''

    def set_target_peer(self, peer_node):
        assert isinstance(peer_node, PeerNode), '{:s} is not a peer node!'.format(peer_node.hostname)
        self.peer_org = peer_node.peer_org
        self.peer_node = peer_node
        env_flags = [
            '-e "CORE_PEER_ADDRESS={:s}:{:d}"'.format(self.peer_node.hostname, self.peer_node.mapped_ports[0]),
            '-e "CORE_PEER_TLS_ROOTCERT_FILE={:s}/peerOrganizations/{:s}/peers/{:s}/tls/ca.crt"'.format(
                PEER_CRYPTODIR, self.peer_org.hostname, self.peer_node.hostname),
            '-e "CORE_PEER_LOCALMSPID={:s}"'.format(self.peer_org.msp_id),
            '-e "CORE_PEER_MSPCONFIGPATH={0}/peerOrganizations/{1}/users/Admin@{1}/msp"'.format(
                PEER_CRYPTODIR, self.peer_org.hostname)]
        self.env_flags = ' '.join(env_flags)

    # Wrapper function for the various *_cmd functions in this class
    def bash_cmd(self, cmd_type, **kwargs):
        assert self.peer_node is not None, '{:s} can\'t interact with ghosts (peer_node == None)!'.format(self.hostname)
        base_cmd = {
            'install_cc': self.install_cc_cmd,
            'instantiate_cc': self.instantiate_cc_cmd,
            'invoke_cc': self.invoke_cc_cmd,
            'query_cc': self.query_cc_cmd
        }[cmd_type](**kwargs)
        return self.make_docker_cmd(base_cmd)

    def make_docker_cmd(self, cmd):
        return 'docker exec {:s} {:s} {:s}'.format(self.env_flags, self.hostname, cmd)

    @staticmethod
    def install_cc_cmd():
        cc_param = '{:s}/{:s}'.format(PEER_CCINSTALLROOT, PEER_CCPATH)

        return 'peer chaincode install -n {:s} -v {:s} -l {:s} -p {:s}\n'.format(
            CC_NAME, CC_VERSION, CC_LANGUAGE, cc_param)

    def instantiate_cc_cmd(self, channel=CHANNEL_NAME, args=None):
        if args is None:
            args = []
        channel_host = channel_host_orderers[channel]
        args = ['init'] + args
        args_str = ','.join(['"{:s}"'.format(str(a)) for a in args])
        return ('peer chaincode instantiate -o {:s}:{:d}{:s} -n {:s} -v {:s} -l {:s} -C {:s} -c '
                '\'{{"Args":[{:s}]}}\' -P "OR(\'{:s}.member\')"\n').format(
            channel_host.hostname, channel_host.mapped_port, self.peer_node.tls_flags[channel], CC_NAME,
            CC_VERSION, CC_LANGUAGE, channel, args_str, self.peer_org.msp_id)

    def invoke_cc_cmd(self, channel=CHANNEL_NAME, chaincode=CC_NAME, peers=None, args=None):
        if args is None:
            args = []
        return self.peer_node.invoke_cc(channel=channel, chaincode=chaincode, peers=peers, args=args)

    @staticmethod
    def query_cc_cmd(channel=CHANNEL_NAME, variable='a'):
        return 'peer chaincode query -C {:s} -n {:s} -c \'{{"Args":["query","{:s}"]}}\''.format(
            channel, CC_NAME, variable)

    def docker_compose_yaml(self, n_indents=2):
        from config_generation.nodes.OrdererNode import OrdererNode

        depends_on_str = self.host.get_depends_on_contents(
            [OrdererNode.__name__, PeerNode.__name__], n_indents=n_indents)
        extra_hosts_str = self.host.get_extra_hosts_contents(
            [OrdererNode.__name__, PeerNode.__name__], n_indents=n_indents)

        return indent(
            n_indents,
            '{:s}:\n'.format(self.hostname) +
            '  container_name: {:s}\n'.format(self.hostname) +
            '  image: hyperledger/fabric-tools\n' +
            '  tty: true\n' +
            '  stdin_open: true\n' +
            '  environment:\n' +
            '    - GOPATH={:s}\n'.format(DOCKER_GOPATH) +
            '    - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock\n' +
            '    - FABRIC_LOGGING_SPEC={:s}\n'.format(LOGLEVEL) +
            '    - CORE_PEER_ID={:s}\n'.format(self.hostname) +
            '    - CORE_PEER_TLS_ENABLED={:s}\n'.format(str(TLS_ENABLED).lower()) +
            '  working_dir: {:s}\n'.format(PEER_WDIR) +
            '  command: /bin/bash\n' +
            '  volumes:\n' +
            '    - /var/run/:/host/var/run/\n' +
            '    - {:s}/{:s}:{:s}\n'.format(self.host.wdir, LOCAL_CCROOT_REL, PEER_CCROOT) +
            '    - {:s}/{:s}:{:s}\n'.format(self.host.wdir, LOCAL_CRYPTODIR_REL, PEER_CRYPTODIR) +
            '{:s}'.format(extra_hosts_str) +
            '{:s}'.format(depends_on_str) +
            '  networks:\n' +
            '    - {:s}\n'.format(DOCKER_NETWORK))
