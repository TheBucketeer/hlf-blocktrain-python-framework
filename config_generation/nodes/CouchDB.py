from config_generation.config import COUCHDB_BASE_PORT, DOCKER_NETWORK, LOCALHOST_IP
from config_generation.util import indent
from config_generation.nodes.DockerService import DockerService


class CouchDB(DockerService):
    # CouchDB names should be unique!
    def __init__(self, ip=LOCALHOST_IP, name='', username='', password=''):
        super().__init__(ip)
        # We typically assume there is only one couchdb per IP, but just in case:
        if name == '':
            name = 'couchdb' + str(self.service_id)
        self.hostname = name
        self.username = username
        self.password = password
        self.mapped_port = COUCHDB_BASE_PORT + self.port_offset

    def docker_compose_yaml(self, n_indents=2):
        return indent(
            n_indents,
            '{:s}:\n'.format(self.hostname) +
            '  container_name: {:s}\n'.format(self.hostname) +
            '  image: hyperledger/fabric-couchdb\n' +
            '  environment:\n' +
            '    - COUCHDB_USER={:s}\n'.format(self.username) +
            '    - COUCHDB_PASSWORD={:s}\n'.format(self.password) +
            '  ports:\n' +
            '    - {:d}:{:d}\n'.format(self.mapped_port, self.mapped_port) +
            '  networks:\n' +
            '    - {:s}\n'.format(DOCKER_NETWORK))
