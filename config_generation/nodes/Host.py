from collections import defaultdict

from config_generation.config import DOCKER_COMPOSE_FILE, SLEEP_DELAYS, DOCKER_NETWORK, all_hosts, LOCALHOST_IP, \
    LOCAL_PI_WDIR, PORT_INTERVAL, LOCAL_DESKTOP_WDIR
from config_generation.nodes.PeerNode import PeerNode
from config_generation.util import sort_services, debug_system, make_ssh_cmd, indent


class Host(object):
    def __init__(self, ip, name):
        self.ip = ip
        # As configured in ~/.ssh/config
        self.ssh_name = name
        # Format: {<service_name>: [<service_objects>]}
        self.docker_services = defaultdict(list)
        self.clients = []

        if self.ip == LOCALHOST_IP:
            self.docker_compose_filename = DOCKER_COMPOSE_FILE
            self.wdir = LOCAL_DESKTOP_WDIR
        else:
            self.docker_compose_filename = '{:s}_{:s}'.format(self.ssh_name, DOCKER_COMPOSE_FILE)
            self.wdir = LOCAL_PI_WDIR

    def register_service(self, service_name, service):
        """Adds a docker service to this host's collection of services.

        Args:
            service_name (str): The identifier to register this service with.
            service (:obj:`DockerService`): The service to register.
        """
        self.docker_services[service_name].append(service)

    def execute_cmd(self, cmd):
        """Given a command, either executes the command locally or on a remote host.

        Args:
            cmd (str): The command to be executed.

        Returns:
            int: The exit status code of the command.
        """
        if self.ip != LOCALHOST_IP:
            cmd = make_ssh_cmd(self.ssh_name, cmd)
        return debug_system(cmd)

    def start_services_cmd(self, service_names=None):
        """Returns the command to start all Docker containers of a given list of types on this host.

        Why this script, rather than a direct command? Because the latter SOMEHOW lead to Kafka nodes being unable to
        elect a leader, thereby making the cluster non-functional. I have yet to figure out why.

        Args:
            service_names (:obj:`list` of :obj:`str`): List of Docker service types. If not provided, all types will be
                used.

        Returns:
            str: The command described above.
        """
        if service_names is None:
            service_names = self.docker_services.keys()

        service_hostnames = []
        for service_name in sorted(service_names):
            for service in self.docker_services[service_name]:
                service_hostnames.append(service.hostname)
        cmd = None
        if len(service_hostnames) > 0:
            cmd = 'cd {:s} && docker-compose -f {:s} up -d {:s}'.format(
                self.wdir, DOCKER_COMPOSE_FILE, ' '.join(service_hostnames))
        return cmd

    def stop_services_cmd(self):
        """Returns the command to stop all Docker containers on this host.

        Returns:
            str: The command described above.
        """
        return 'cd {:s} && docker-compose -f {:s} down --remove-orphans'.format(self.wdir, DOCKER_COMPOSE_FILE)

    def get_port_offset_for_service(self, service):
        """Returns the offset that a Docker service should add to any ports it listens to.

        Args:
            service: The service object itself, must be in self.docker_services[service.__class__.__name__].

        Returns:
            int: The port offset.
        """
        name = service.__class__.__name__
        assert service.host == self
        assert service in self.docker_services[name], \
            'IP {:s} already does not know service {:s}!'.format(self.ip, service.hostname)
        return self.docker_services[name].index(service) * PORT_INTERVAL

    def get_depends_on_contents(self, service_name_list, known_dependencies=None, n_indents=2):
        """Returns the contents of the 'depends_on' field in docker-compose.yaml.

        Given a selected set of service types, creates and returns the contents of the 'depends_on' field in
        docker-compose.yaml.

        Args:
            service_name_list (:obj:`list` of :obj:`str`): List of service types ('kafka', 'zookeeper', etc.) to be
                included for this node. Nodes don't need to be aware of ALL other nodes in the network!
            known_dependencies (:obj:`list` of :obj:`object`): Objects of Docker services that must be added in addition
                to the services specified in service_name_list. This parameter is useful if you do not want all services
                of a certain type to be added, but only one or a few.
            n_indents (:obj:`int`, optional): Number of indents to use for the resulting string, in spaces. Default is
                2.

        Returns:
            str: The resulting contents for the 'depends_on' field.
        """
        if known_dependencies is None:
            known_dependencies = []

        depends_on_str = ''
        for service in sort_services(known_dependencies):
            depends_on_str += indent(n_indents + 2, '- {:s}\n'.format(service.hostname))
        for name in sorted(service_name_list):
            for service in self.docker_services[name]:
                depends_on_str += indent(n_indents + 2, '- {:s}\n'.format(service.hostname))
        if depends_on_str != '':
            depends_on_str = indent(n_indents, 'depends_on:\n') + depends_on_str
        return depends_on_str

    def get_extra_hosts_contents(self, service_name_list, n_indents=2):
        """Returns the contents of the 'extra_hosts' field in docker-compose.yaml, given a selected set of service
        types.

        Args:
            service_name_list (:obj:`list` of :obj:`str`): List of services ('kafka', 'zookeeper', etc.) to be included
                for this node. Nodes don't need to be aware of ALL other nodes in the network!.
            n_indents (:obj:`int`, optional): Number of indents to use for the resulting string, in spaces. Defaults to
                2.

        Returns:
            str: The resulting contents for the 'extra_hosts' field.
        """
        extra_hosts_str = ''
        # For every service type, find all services of that type that are not on this host
        for name in sorted(service_name_list):
            for host in all_hosts:
                if host == self:
                    continue
                for service in host.docker_services[name]:
                    extra_hosts_str += indent(n_indents + 2, '- "{:s}:{:s}"\n'.format(service.hostname, host.ip))
        if len(extra_hosts_str) > 0:
            extra_hosts_str = indent(n_indents, 'extra_hosts:\n') + extra_hosts_str
        return extra_hosts_str

    def join_sh(self):
        """Creates and returns the contents of the join.sh file for this host.

        Returns:
            str: The contents of the join.sh file for this host.
        """
        contents = 'set -ev\n' + \
                   'export MSYS_NO_PATHCONV=1\n' + \
                   'export FABRIC_START_TIMEOUT=10\n'
        # Make peers join the channel
        for peer_node in self.docker_services[PeerNode.__name__]:
            contents += peer_node.bash_cmd('fetch') + '\n'
        contents += 'sleep {:d}\n'.format(SLEEP_DELAYS['after_fetch_config'])
        for peer_node in self.docker_services[PeerNode.__name__]:
            contents += peer_node.bash_cmd('join') + '\n'
        contents += 'sleep {:d}\n'.format(SLEEP_DELAYS['after_join'])
        return contents

    def docker_compose_yaml(self, service_names=None):
        """Creates the docker-compose.yaml file for this host.

           Args:
               service_names (:obj:`list` of :obj:`str`, optional): A list of the names of all service types that must
                   be started.

           Returns:
               str: The contents of the docker-compose.yaml file.
        """
        if service_names is None:
            service_names = list(self.docker_services.keys())
        contents = 'version: \'2\'\n\n'
        contents += 'networks:\n' + \
                    '  {:s}:\n\n'.format(DOCKER_NETWORK) + \
                    'services:\n\n'
        for service_name in sorted(service_names):
            for service in self.docker_services[service_name]:
                contents += service.docker_compose_yaml() + '\n'
        return contents
