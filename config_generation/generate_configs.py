import os

from config_generation.config import channel_host_orderers, cas, orderer_orgs, peer_orgs, clients, \
    CHANNEL_NAME, COMPOSE_PROJECT_NAME, CONSORTIUM_NAME, LOCAL_CA_SCRIPTS_REL, PEER_PROFILE, get_network_hosts, \
    CRYPTO_CONFIG_FILE, CONFIGTX_FILE, DOTENV_FILE, LOCAL_DESKTOP_WDIR
from config_generation.util import debug_system


def make_crypto_config_yaml():
    """Creates the crypto-config.yaml file for this network."""
    contents = 'OrdererOrgs:\n'
    for orderer_org in orderer_orgs:
        contents += orderer_org.crypto_config_yaml()
    contents += 'PeerOrgs:\n'
    for peer_org in peer_orgs:
        contents += peer_org.crypto_config_yaml()
    return contents


def make_configtx_yaml(channel=CHANNEL_NAME):
    assert len(orderer_orgs) == 1, str(len(orderer_orgs))

    contents = 'Organizations:\n'
    for orderer in orderer_orgs:
        contents += orderer.configtx_yaml()

    orderer = channel_host_orderers[channel]

    for peer_org in peer_orgs:
        contents += peer_org.configtx_yaml()
    contents += ('Capabilities:\n' +
                 '    Global: &ChannelCapabilities\n' +
                 '        V1_1: true\n' +
                 '    Orderer: &OrdererCapabilities\n' +
                 '        V1_1: true\n' +
                 '    Application: &ApplicationCapabilities\n' +
                 '        V1_1: true\n' +
                 'Application: &ApplicationDefaults\n' +
                 '    Organizations:\n' +
                 '    Policies:\n' +
                 '        Readers:\n' +
                 '            Type: ImplicitMeta\n' +
                 '            Rule: "ANY Readers"\n' +
                 '        Writers:\n' +
                 '            Type: ImplicitMeta\n' +
                 '            Rule: "ANY Writers"\n' +
                 '        Admins:\n' +
                 '            Type: ImplicitMeta\n' +
                 '            Rule: "MAJORITY Admins"\n' +
                 '    Capabilities:\n' +
                 '        <<: *ApplicationCapabilities\n')
    contents += orderer.orderer_org.configtx_yaml_defaults()
    contents += 'Profiles:\n'
    contents += orderer.orderer_org.configtx_yaml_profile(peer_orgs)
    contents += '    {:s}:\n'.format(PEER_PROFILE) + \
                '        Consortium: {:s}\n'.format(CONSORTIUM_NAME) + \
                '        Application:\n' + \
                '            <<: *ApplicationDefaults\n' + \
                '            Organizations:\n'
    for peer_org in peer_orgs:
        contents += '                - *{:s}\n'.format(peer_org.name)
    contents += '            Capabilities:\n' + \
                '                <<: *ApplicationCapabilities\n'
    return contents


def make_dotenv():
    return 'COMPOSE_PROJECT_NAME={:s}\n'.format(COMPOSE_PROJECT_NAME) + \
           'IMAGE_TAG=latest\n'


def generate_configs():
    """Generates all files required for the network."""
    with open('{:s}/{:s}'.format(LOCAL_DESKTOP_WDIR, CRYPTO_CONFIG_FILE), 'w') as f:
        f.write(make_crypto_config_yaml())
    with open('{:s}/{:s}'.format(LOCAL_DESKTOP_WDIR, CONFIGTX_FILE), 'w') as f:
        f.write(make_configtx_yaml())
    with open('{:s}/{:s}'.format(LOCAL_DESKTOP_WDIR, DOTENV_FILE), 'w') as f:
        f.write(make_dotenv())
    for ca in cas:
        with open('{:s}/{:s}/{:s}'.format(LOCAL_DESKTOP_WDIR, LOCAL_CA_SCRIPTS_REL, ca.update_affiliations_file), 'w') as f:
            f.write(ca.update_affiliations_sh())
    for host in get_network_hosts():
        with open('{:s}/{:s}'.format(LOCAL_DESKTOP_WDIR, host.docker_compose_filename), 'w') as f:
            f.write(host.docker_compose_yaml())
    for client in clients:
        with open('{:s}/{:s}'.format(LOCAL_DESKTOP_WDIR, client.compile_filename), 'w') as f:
            f.write(client.compile_client_sh())
        with open('{:s}/{:s}'.format(LOCAL_DESKTOP_WDIR, client.clientsettings_filename), 'w') as f:
            f.write(client.clientsettings_json())
        with open('{:s}/{:s}'.format(LOCAL_DESKTOP_WDIR, client.run_filename), 'w') as f:
            f.write(client.run_client_sh())
        with open('{:s}/{:s}'.format(LOCAL_DESKTOP_WDIR, client.connection_json_filename), 'w') as f:
            f.write(client.connection_json())
        with open('{:s}/{:s}'.format(LOCAL_DESKTOP_WDIR, client.log4j_props_filename), 'w') as f:
            f.write(client.log4j_props())
        debug_system('chmod +x {:s}'.format(
            ' '.join(['{:s}/{:s}'.format(LOCAL_DESKTOP_WDIR, client.run_filename),
                      '{:s}/{:s}'.format(LOCAL_DESKTOP_WDIR, client.compile_filename)])))


if __name__ == '__main__':
    generate_configs()
