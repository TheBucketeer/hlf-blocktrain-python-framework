from config_generation.config import CHANNEL_NAME, KAFKA_ENABLED, LOCALHOST_IP, SCC_CLIENT_NAME, \
    SSH_NAMES_TO_IPS, \
    cas, channel_host_orderers, clients, clis, couchdbs, kafkas, orderer_nodes, orderer_orgs, peer_nodes, peer_orgs, \
    zookeepers, IPS_TO_SSH_NAMES, all_hosts, IPS_TO_HOSTS
from config_generation.nodes.CA import CA
from config_generation.nodes.CLI import CLI
from config_generation.nodes.Client import Client
from config_generation.nodes.Host import Host
from config_generation.nodes.Kafka import Kafka
from config_generation.nodes.OrdererOrg import OrdererOrg
from config_generation.nodes.PeerOrg import PeerOrg
from config_generation.nodes.Zookeeper import Zookeeper


def setup_hosts():
    # List of all host objects
    all_hosts.extend([Host(ip, IPS_TO_SSH_NAMES[ip]) for ip in sorted(IPS_TO_SSH_NAMES.keys())])
    all_hosts.append(Host(LOCALHOST_IP, 'localhost'))
    # Mapping from IP to host objects for convenience
    for host in all_hosts:
        IPS_TO_HOSTS[host.ip] = host


def make_default_network_layout(hosts_available,
                                n_orderers_on_pc=2,
                                n_orderers_on_pis=0,
                                n_peers_on_pc=2,
                                n_peers_on_pis=0,
                                n_scc_clients_on_pc=1,
                                n_scc_clients_on_pis=0,
                                n_kafkas_on_pc=3,
                                n_kafkas_on_pis=0,
                                n_zookeepers_on_pc=4,
                                n_zookeepers_on_pis=0):
    """Creates a 'default' network layout.

    Creates a network layout that ensures that every remote host will host an (approximately) equal amount of nodes
    (clients or network nodes), given the numbers of every type of nodes that must run on either a Pi or the
    'Semi-central Machine' (SCM). That is, no remote host in the network will host more than 1 more node than any other
    remote host. This network layout includes 1 Peer- and 1 Orderer organisation.

    Take note how the various nodes are stored: by adding them to the global lists, such as orderer_nodes, kafkas, etc.
    Do NOT assign anythin to these lists (kafkas = [...]), or your additions will not be visible in other files.

    Args:
        hosts_available (:obj:`list` of :obj:`str`): The SSH identifiers of the hosts that are available.
        n_orderers_on_pc (:obj:`int`, optional): Number of orderer nodes to be hosted on the SCM.
        n_orderers_on_pis (:obj:`int`, optional): Number of orderer nodes to be hosted on Pis.
        n_peers_on_pc (:obj:`int`, optional): Number of peer nodes to be hosted on the SCM.
        n_peers_on_pis (:obj:`int`, optional): Number of peer nodes to be hosted on Pis.
        n_scc_clients_on_pc (:obj:`int`, optional): Number of clients to be hosted on the SCM.
        n_scc_clients_on_pis (:obj:`int`, optional): Number of clients to be hosted on Pis.
        n_kafkas_on_pc (:obj:`int`, optional): Number of Kafka nodes to be hosted on the SCM.
        n_kafkas_on_pis (:obj:`int`, optional): Number of Kafka nodes to be hosted on Pis.
        n_zookeepers_on_pc (:obj:`int`, optional): Number of Zookeeper nodes to be hosted on the SCM.
        n_zookeepers_on_pis (:obj:`int`, optional): Number of Zookeeper nodes to be hosted on Pis.
    """
    # The number of Pis that will host at least 1 node
    total_n_pis_in_use = 0

    def get_pi_ips(n_pis):
        """Yields IP addresses of remote hosts that are next in line to be reserved for nodes.

        Args:
            n_pis (int): The number of IP addresses to obtain.

        Returns:
            :obj:`generator` of :obj:`str`: IP addresses of remote hosts that are to be reserved for nodes.
        """
        nonlocal total_n_pis_in_use

        for i in range(total_n_pis_in_use, n_pis + total_n_pis_in_use):
            pi_i = i % len(hosts_available)
            total_n_pis_in_use += 1
            yield SSH_NAMES_TO_IPS[hosts_available[pi_i]]

    # Orderers
    orderer_node_ips = [LOCALHOST_IP] * n_orderers_on_pc + \
                       [pi_ip for pi_ip in get_pi_ips(n_orderers_on_pis)]
    orderer_orgs.extend([OrdererOrg(kafka_enabled=KAFKA_ENABLED, orderer_ips=orderer_node_ips)])
    orderer_nodes.extend(sum([orderer_org.orderers for orderer_org in orderer_orgs], []))
    channel_host_orderers[CHANNEL_NAME] = orderer_orgs[0].orderers[0]
    # Kafka/Zookeeper nodes
    kafkas.extend([Kafka() for _ in range(n_kafkas_on_pc)])
    kafkas.extend([Kafka(ip=pi_ip) for pi_ip in get_pi_ips(n_kafkas_on_pis)])
    zookeepers.extend([Zookeeper() for _ in range(n_zookeepers_on_pc)])
    zookeepers.extend([Zookeeper(ip=pi_ip) for pi_ip in get_pi_ips(n_zookeepers_on_pis)])
    # Peers
    peer_clients = [Client(LOCALHOST_IP, SCC_CLIENT_NAME) for _ in range(n_scc_clients_on_pc)] + \
                   [Client(pi_ip, SCC_CLIENT_NAME) for pi_ip in get_pi_ips(n_scc_clients_on_pis)]
    peer_node_ips = [LOCALHOST_IP] * n_peers_on_pc + [pi_ip for pi_ip in get_pi_ips(n_peers_on_pis)]
    # We use a single organisation, no need to make things more complicated
    peer_orgs.extend([PeerOrg(admin_ips=peer_node_ips, couchdb_deps=[None] * len(peer_node_ips), clients=peer_clients)])
    peer_nodes.extend(sum([peer_org.peers for peer_org in peer_orgs], []))
    # Clients
    clients.extend(sum([peer_org.clients for peer_org in peer_orgs], []))
    # CouchDBs (if used)
    couchdbs.extend([peer_node.couchdb for peer_node in peer_nodes])
    # Certificate Authorities
    cas.extend([CA(peer_orgs[0])])
    # CLI nodes
    clis.extend([CLI()])


def make_test_network_layout():
    pi_ips = [SSH_NAMES_TO_IPS[piname] for piname in ['pi0', 'pi1', 'pi2', 'pi4', 'pi3']]

    # Orderers
    orderer_orgs.extend([OrdererOrg(kafka_enabled=KAFKA_ENABLED, orderer_ips=[pi_ips[0], pi_ips[0]])])
    orderer_nodes.extend(sum([orderer_org.orderers for orderer_org in orderer_orgs], []))
    channel_host_orderers[CHANNEL_NAME] = orderer_orgs[0].orderers[0]
    # Kafka/Zookeeper nodes
    kafkas.extend([Kafka(ip=pi_ips[1]) for _ in range(3)])
    zookeepers.extend([Zookeeper(ip=pi_ips[2]) for _ in range(4)])
    # Peers
    peer_clients = [Client(LOCALHOST_IP, SCC_CLIENT_NAME)]
    peer_node_ips = [pi_ips[1], pi_ips[3]]
    # We use a single organisation, no need to make things more complicated
    peer_orgs.extend([PeerOrg(admin_ips=peer_node_ips, couchdb_deps=[None] * len(peer_node_ips), clients=peer_clients)])
    peer_nodes.extend(sum([peer_org.peers for peer_org in peer_orgs], []))
    # Clients
    clients.extend(sum([peer_org.clients for peer_org in peer_orgs], []))
    # CouchDBs (if used)
    couchdbs.extend([peer_node.couchdb for peer_node in peer_nodes])
    # Certificate Authorities, no more than 1 per organisation
    cas.extend([CA(peer_orgs[0], ip=pi_ips[4])])
    clis.extend([CLI()])


def make_network_layout():
    # Pass parameters to change the numbers of the different types of nodes, or create a different network layout
    # entirely.

    # To only use the railway asset Pis
    # pis_in_use = [name for name in sorted(PIS_TO_IPS.keys()) if not name.startswith('pi')]
    # For the small test board with 5 pis:
    # pis_in_use = ['pi0', 'pi1', 'pi2', 'pi3', 'pi4']
    make_test_network_layout()
